type Item = {
  id: number;
  name: string;
  stock: number;
  satuan: string;
};

type Pacakge = {
  name: string;
  items: Item[];
};

export type DeliveryData = {
  name: string;
  package?: Pacakge[];
  source: string;
  via: string;
  destination: string;
  status: string;
  user: string;
  noResi?: string;
  dateCreated?: any;
  dateArrived?: any;
};

export const deliveryData: DeliveryData[] = [
  {
    name: 'Kapal 171',
    via: 'JNE',
    package: [
      {
        name: 'Paket 1',
        items: [
          { id: 1, name: 'barang 1', stock: 10, satuan: 'Kg' },
          { id: 2, name: 'barang 2', stock: 10, satuan: 'Kg' },
        ],
      },
      {
        name: 'Paket 2',
        items: [
          { id: 3, name: 'barang 3', stock: 10, satuan: 'Kg' },
          { id: 4, name: 'barang 4', stock: 10, satuan: 'Kg' },
        ],
      },
    ],
    source: 'Gudang A',
    destination: 'Malang',
    status: 'On-Going',
    user: 'Wisnu (Staff)',
    noResi: '123123123123',
    dateCreated: '10 January 2024',
    dateArrived: null,
  },
];
