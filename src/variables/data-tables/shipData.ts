export const shipData = [
  {
    uuid: '123123',
    name: '123123123',
    ship_code: 'U-1751',
    desc: 'Ini Deskripsi',
    image: 'Ini Image',
    location: 'Gudang A',
  },
  {
    uuid: '12344',
    name: '12938109238234',
    ship_code: 'U-172',
    desc: 'Ini Deskripsi',
    image: 'Ini Image',
    location: 'Gudang B',
  },
];
