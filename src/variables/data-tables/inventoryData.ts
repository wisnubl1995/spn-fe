type RowObj = {
  uuid: string;
  name: string;
  stock: number;
  satuan: string;
  desc: string;
  image: any;
  location: string;
  threshold: number;
};

const inventoryData: RowObj[] = [
  {
    uuid: '123123123',
    name: 'Barang 1',
    stock: 90,
    satuan: 'Kg',
    desc: 'ini description',
    image: 'Ini image',
    location: 'Gudang A',
    threshold: 10,
  },
  {
    uuid: '123123123',
    name: 'Barang 2',
    stock: 90,
    satuan: 'Kg',
    desc: 'ini description',
    image: 'Ini image',
    location: 'Gudang A',
    threshold: 10,
  },
  {
    uuid: '123123123',
    name: 'Barang 3',
    stock: 90,
    satuan: 'Kg',
    desc: 'ini description',
    image: 'Ini image',
    location: 'Gudang A',
    threshold: 10,
  },
  {
    uuid: '123123123',
    name: 'Barang 4',
    stock: 90,
    satuan: 'Kg',
    desc: 'ini description',
    image: 'Ini image',
    location: 'Gudang A',
    threshold: 10,
  },
  {
    uuid: '123123123',
    name: 'Barang 5',
    stock: 90,
    satuan: 'Kg',
    desc: 'ini description',
    image: 'Ini image',
    location: 'Gudang A',
    threshold: 10,
  },
];

export default inventoryData;
