type RowObj = {
  name: string;
  progress: string;
  inventory: number;
  date: string;
};

const tableDataColumns: RowObj[] = [
  {
    name: 'Kapal 570',
    inventory: 2458,
    progress: '17.5%',
    date: '12 Jan 2021',
  },
  {
    name: 'Kapal 588',
    inventory: 1485,
    progress: '10.8%',
    date: '21 Feb 2021',
  },
  {
    name: 'Kapal 100',
    inventory: 1024,
    progress: '21.3%',
    date: '13 Mar 2021',
  },
  {
    name: 'Kapal 530',
    inventory: 858,
    progress: '31.5%',
    date: '24 Jan 2021',
  },
];

export default tableDataColumns;
