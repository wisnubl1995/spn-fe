export const purchaseRequest = [
  {
    uuid: '11122223222',
    ship_name: 'Berlian',
    department: 'Operational',
    category: 'Besi',
    purchase_request_no: 'PR-10002-10001',
    requested_by: 'Wisnu',
    location: 'Jakarta',
    created_at: '21 March 2024',
    purchaseItems: [
      {
        uuid: '112232442',
        purchaseRequestId: '11122223222',
        name: 'Besi Baja',
        unit: 'Kg',
        quantityAsked: '10',
        quantityLeft: '0',
        quantityApproved: '0',
        sapCode: 'sapCode',
        description: 'Deskkripsi',
        additionalInfo: 'Additional Info',
        image: 'Image',
        status: 'Pending',
      },
      {
        uuid: '112232442',
        purchaseRequestId: '11122223222',
        name: 'Besi Baja',
        unit: 'Kg',
        quantityAsked: '10',
        quantityLeft: '0',
        quantityApproved: '0',
        sapCode: 'sapCode',
        description: 'Deskkripsi',
        additionalInfo: 'Additional Info',
        image: 'Image',
        status: 'Pending',
      },
    ],
  },
];
