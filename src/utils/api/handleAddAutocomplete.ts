const onAddAutocomplete = async (urlAdd: string, payload: any, header?: any) => {
    const url = `${process.env.NEXT_PUBLIC_API}${urlAdd}`;
	try {
        let headerReq = {
            "Content-Type": "application/json"
        }
        
        if (header) {
            headerReq = { ...headerReq, ...header}
        }

        const response = await fetch(url, {
            method: 'POST',
            headers: headerReq,
            body: JSON.stringify(payload)
        });
    
        if (!response.ok) {
            console.log('onAddAutocomplete >> Network response was not ok');
            return false
        } else {
            console.log('onAddAutocomplete >> Success add data!');
            return true
        }
    } catch (error) {
        console.error('onAddAutocomplete >> There was a problem with the save operation:', error);
        return false
    }
}

export default onAddAutocomplete