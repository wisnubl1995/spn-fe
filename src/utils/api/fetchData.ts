const baseUrl = process.env.NEXT_PUBLIC_API

export const sendRequestApi = async (url: string, method: string, body?: any, headers?: any) => {
    const options: any = {
        method: method ? method : 'GET',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    }

    if (body) {
        options.body = body
    }

    if (headers) {
        options.headers = { ...options.headers, ...headers }
    }
    
    // Extract timeout value from headers (if present)
    const timeout = headers?.timeout || 10000; // Default timeout to 10 seconds

    // Create a Promise with timeout functionality
    const controller = new AbortController();
    const signal = controller.signal;

    const timeoutPromise = new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Request timed out')
            controller.abort();
            reject(new Error('Request timed out'));
        }, timeout);
    });

    if (options.headers?.timeout) {
        delete options.headers.timeout
    }
    
    const fetchPromise = fetch(`${baseUrl}${url}`, { ...options, signal });

    // Use Promise.race to handle both success and timeout
    return Promise.race([timeoutPromise, fetchPromise])
        .then((response) => {
            if (response instanceof Response) { // Handle successful fetch response
                if (response.status === 204) {
                    return { data: [] };
                }
                return response.json();
            } else { // Handle timeout case
                console.log('Request timed out')
                throw new Error('Request timed out');
            }
        })
        .catch((error) => {
            throw error; // Re-throw the error for handling in the calling code
        });

    // const response = await fetch(`${baseUrl}${url}`, options)

    // if (response.status == 204) {
    //     return { data: [] }
    // }
    
    // return response.json()
}
