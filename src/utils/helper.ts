export function buildQuery(baseUrl: string, userQuery: any){
    //store query parameters in a temporary variable
    var query = [];
    //loop through user query object
    for (var key in userQuery) {
        //encode the keys and values this is most necessary for search inputs 
        if (userQuery[key] !== null && userQuery[key] !== '' && userQuery[key] !== undefined) {
          query.push(encodeURIComponent(key) + '=' + encodeURIComponent(userQuery[key]));
        }
    }
    //construct new URL
    let new_url = baseUrl + (query.length ? '?' + query.join('&') : '');
    return(new_url);
}

export const getBase64 = async (file: File): Promise<string | null> => {
    if (!file) {
      return null; // Handle case where no file is provided
    }
  
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result as string);
      reader.onerror = (error) => reject(error);
    });
};

type FormValues = { [key: string]: string }; // Define type for form values
interface OptionalFields {
  [key: string]: boolean; // Interface to specify optional fields
}
export const validateForm = (values: FormValues, optionalFields?: OptionalFields): boolean => {
  // Iterate through all form values using Object.entries
  for (const [key, value] of Object.entries(values)) {
    if (!optionalFields || !optionalFields[key]) { // Skip check if field is optional
      if (!value || `${value}`?.trim() === '') {
        return false; // Return false if non-optional value is missing or empty
      }
    }
  }
  return true; // Return true if all values are present and not empty (considering optional)
};

export const formattedDate = (date: Date, format: string): string => {
  const options:any = {
    year: 'numeric', // Year format (e.g., 2024)
    month: '2-digit', // Month format (e.g., 05)
    day: '2-digit', // Day format (e.g., 23)
  };

  // Add additional options based on the formatting string
  if (format.includes('h')) {
    options.hour = 'numeric'; // Hour (1-12 format)
    options.hour12 = true; // Use 12-hour clock
  } else if (format.includes('H')) {
    options.hour = 'numeric'; // Hour (24-hour format)
  }
  if (format.includes('m')) {
    options.minute = '2-digit'; // Minute (e.g., 09)
  }
  if (format.includes('s')) {
    options.second = '2-digit'; // Second (e.g., 31)
  }

  // Extend the options for specific formats (e.g., weekday, month name)
  const extendedOptions = { ...options };
  if (format.includes('EEEE')) {
    extendedOptions.weekday = 'long'; // Full weekday name (e.g., Thursday)
  } else if (format.includes('E')) {
    extendedOptions.weekday = 'short'; // Abbreviated weekday name (e.g., Thu)
  }
  if (format.includes('MMMM')) {
    extendedOptions.month = 'long'; // Full month name (e.g., May)
  } else if (format.includes('MMM')) {
    extendedOptions.month = 'short'; // Abbreviated month name (e.g., May)
  }

  return new Intl.DateTimeFormat('id-ID', extendedOptions).format(date);
};

export const formatNumber = (number: number): string => {
  if (isNaN(number)) {
    return '0';
  }
  
  return new Intl.NumberFormat('id-ID', {
    minimumFractionDigits: 0, // No decimals by default
    maximumFractionDigits: 2, // Allow up to 2 decimal places (optional)
  }).format(number);
};

export function getAndStoreDataWithExpiry<T>(key: string, data: T, expiryTimeInHours: number): T | null {
  const localStorageData = localStorage.getItem(key);

  // Parse stored data (if any)
  let storedData: { data: T; expiredAt: number, isExpired: boolean } | any = null;
  try {
    storedData = JSON.parse(localStorageData || 'null');
  } catch (error) {
    console.error(`Error parsing localStorage data for key "${key}":`, error);
  }

  // Check if data exists and is not expired
  if (storedData) {
    const now = Date.now();
    if (storedData.expiredAt > now) {
      storedData = { data: storedData.data, expiredAt: storedData.expiredAt, isExpired: false }
      return storedData; // Return data if not expired
    } else {
      // Data is expired, remove from localStorage
      localStorage.removeItem(key);
      storedData = { data: null, expiredAt: storedData.expiredAt, isExpired: true }
      return storedData
    }
  }

  // Data is either new or expired, store it with expiry time
  const expiryTimestamp = Date.now() + (expiryTimeInHours * 60 * 60 * 1000);
  const dataToSave: any = { data, expiredAt: expiryTimestamp, isExpired: false };
  localStorage.setItem(key, JSON.stringify(dataToSave));

  return dataToSave; // Data is newly stored, not retrieved
}

export function slugify(str:string) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim leading/trailing white space
  str = str.toLowerCase(); // convert string to lowercase
  str = str.replace(/[^a-z0-9 -]/g, '') // remove any non-alphanumeric characters
           .replace(/\s+/g, '-') // replace spaces with hyphens
           .replace(/-+/g, '-')
           .replace(/-/g, '') // remove consecutive hyphens
  return str;
}

export function formatSequenceNumber(seqNumber: number, digits: number = 6) {
  // Ensure positive integer input
  if (seqNumber <= 0 || !Number.isInteger(seqNumber)) {
    throw new Error('formatSequenceNumber : Invalid page number: must be a positive integer.');
  }

  // Convert to string and pad with leading zeros
  return seqNumber.toString().padStart(digits, '0');
}