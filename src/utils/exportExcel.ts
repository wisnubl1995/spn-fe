import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

function formatDate(date: any, format = "YYYY-MM-DD") {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Add leading 0 for single-digit months
    const day = String(date.getDate()).padStart(2, '0'); // Add leading 0 for single-digit days
  
    // Replace placeholders in the format string
    return format.replace(/YYYY/g, year)
                  .replace(/MM/g, month)
                  .replace(/DD/g, day);
  }

const exportToExcel = (data: any, filename: string, sheetName: string = "Sheet1") => {
    const worksheet = XLSX.utils.json_to_sheet(data);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, sheetName);

    // Buffer to store the generated Excel file
    const excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const blob = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });

    filename = `${filename}_${formatDate(new Date(), "YYYYMMDD")}.xlsx`
    if (!filename) {
        filename = `data_${formatDate(new Date(), "YYYYMMDD")}.xlsx`
    }

    saveAs(blob, filename);
};

export default exportToExcel