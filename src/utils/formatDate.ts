export const formatDate = (dateString) => {
  // Parse the date string
  const date = new Date(dateString);

  // Define options for date formatting
  const options: any = { day: '2-digit', month: 'long', year: 'numeric', weekday: 'long' };

  // Format the date
  return date.toLocaleDateString('id-ID', options);
};
