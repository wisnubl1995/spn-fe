'use client'

import { useState } from "react";
import { sendRequestApi } from "utils/api/fetchData";
import AuthContext from "./AuthContext";

const AuthContextProvider = ({ children }: any) => {
    const checkLastSession = () => {
        try {
            const jsonValue = localStorage.getItem('@session_user')
            const sessionDetail = jsonValue != null ? JSON.parse(jsonValue) : null;
            return sessionDetail
        } catch(error) {
            console.log('checkLastSession error:', error?.message)
            return null
        }
    }
    
    const [isSignIn, setIsSignIn] = useState(false)
    const [detailUser, setDetailUser] = useState(checkLastSession)

    const setIsSignInHandler = (isLogin: boolean) => {
        setIsSignIn(isLogin)
    }

    const setDetailUserHandler = (data: any) => {
        setDetailUser(data)
    }

    const doLoginHandler = async ({ username, password }) => {
        try {
            const resp = await sendRequestApi('account/login', 'POST', JSON.stringify({ username, password }))
            if (resp.data) {
                const userData = JSON.stringify(resp.data)
                if (userData) {
                    localStorage.setItem('@session_user', userData)
                    setDetailUser(resp.data)
                } else {
                    localStorage.removeItem('@session_user');
                }
                return true
            }
            return false
        } catch (error) {
            console.log('doLoginHandler error:', error?.message)
            return false
        }
    }

    const value: any = {
        isLogin: isSignIn,
        sessionUser: detailUser,
        userLevel: detailUser?.ltree ? detailUser?.ltree : "",
        setIsLogin: setIsSignInHandler,
        setSessionUser: setDetailUserHandler,
        checkLogin: doLoginHandler,
    }

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export default AuthContextProvider