import { createContext } from "react";

interface AuthContextType {
    isLogin: boolean,
    sessionUser: any,
    userLevel: string,
    setIsLogin: (isLogin: boolean) => void,
    setSessionUser: (data: any) => void,
    checkLogin: ({ username, password }: any) => Promise<boolean>,
  }

const AuthContext = createContext<AuthContextType>({
    isLogin: false,
    sessionUser: null,
    userLevel: "",
    setIsLogin: (isLogin: boolean) => {},
    setSessionUser: (data: any) => {},
    checkLogin: ({ username, password }: any) => Promise.resolve(false),
})

export default AuthContext