import React from 'react';

const CustomTooltip = ({ children, text }) => {
  return (
    <div className="relative items-center group">
      {children}
      <span className="absolute bottom-full mb-2 w-max hidden p-2 text-center text-white bg-gray-800 rounded opacity-0 transition-opacity duration-300 group-hover:block group-hover:opacity-100">
        {text}
      </span>
    </div>
  );
};

export default CustomTooltip;
