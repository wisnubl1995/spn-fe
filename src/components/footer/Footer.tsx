import Link from 'next/link';

const Footer = () => {
  return (
    <div className="flex w-full flex-col items-center justify-between px-1 lg:px-8 xl:flex-row-reverse">
      <p className=" text-center text-sm font-medium text-gray-600 sm:!mb-0 md:text-lg">
        <span className="text-center text-sm text-gray-600 sm:!mb-0 md:text-base">
          ©{new Date().getFullYear()} Solusi Pelayaran Nasional. All Rights
          Reserved. Powered By{' '}
          <Link href={'https://metageniuslab.com'} target="_blank">
            MetaGenius Lab
          </Link>
        </span>
      </p>
      <div>
        <ul className="flex flex-wrap items-center gap-3 sm:flex-nowrap md:gap-10">
          <li>
            <a
              target="blank"
              href="https://spnindo.co.id/"
              className="text-base font-medium text-gray-600 hover:text-gray-600"
            >
              Website
            </a>
          </li>
          <li>
            <a
              target="blank"
              href="#"
              className="text-base font-medium text-gray-600 hover:text-gray-600"
            >
              Handbook
            </a>
          </li>
          <li>
            <a
              target="blank"
              href="#"
              className="text-base font-medium text-gray-600 hover:text-gray-600"
            >
              Terms of Use
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Footer;
