import Link from 'next/link';

/*eslint-disable*/
export default function Footer() {
  return (
    <div className="z-[5] mx-auto flex w-full max-w-screen-sm flex-col items-center justify-between px-[20px] lg:mb-6 lg:max-w-[100%] lg:flex-row xl:mb-2 xl:w-[1310px] ">
      <p className=" text-center text-sm font-medium text-gray-600 sm:!mb-0 md:text-lg">
        <span className="text-center text-sm text-gray-600 sm:!mb-0 md:text-base">
          ©{new Date().getFullYear()} Solusi Pelayaran Nasional. All Rights
          Reserved. Powered By{' '}
          <Link href={'https://metageniuslab.com'} target="_blank">
            MetaGenius Lab
          </Link>
        </span>
      </p>
      <ul className="flex flex-wrap items-center sm:flex-nowrap">
        <li className="mr-12">
          <a
            target="blank"
            href="mailto:wisnubl1995@gmail.com"
            className="text-sm text-gray-600 hover:text-gray-600 md:text-base lg:text-white lg:hover:text-white"
          >
            Support
          </a>
        </li>
        <li className="mr-12">
          <a
            target="blank"
            href="#"
            className="text-sm text-gray-600 hover:text-gray-600 md:text-base lg:text-white lg:hover:text-white"
          >
            Handbook
          </a>
        </li>

        <li>
          <a
            target="blank"
            href="https://spnindo.co.id/"
            className="text-sm text-gray-600 hover:text-gray-600 md:text-base lg:text-white lg:hover:text-white"
          >
            Website
          </a>
        </li>
      </ul>
    </div>
  );
}
