import React, { useEffect } from 'react';
import { FaInfoCircle } from "react-icons/fa";

interface NotificationOverlayProps {
  title: string;
  message: string;
  handleClose: () => void;
  color?: string;
  isShow?: boolean;
  showBtnClose?: boolean;
}

const NotificationOverlay: React.FC<NotificationOverlayProps> = ({ title, message, color, isShow, handleClose, showBtnClose }) => {
    useEffect(() => {
        if (isShow) {
            const timer = setTimeout(() => {
                handleClose();
            }, 3000);
        
            return () => clearTimeout(timer);
        }
    }, [isShow]);

    return (
        <div
            className={`fixed top-0 left-0 w-full h-full z-10 bg-opacity-75 bg-gray-800 flex justify-center items-center ${
            !isShow && 'hidden'
        }`}>
            <div className="bg-white rounded-lg shadow-md p-4 text-center">
                <p className="font-bold">{title ? title : 'Info'}</p>
                <div className='flex flex-row items-center justify-center'>
                    <FaInfoCircle className='h-6 w-6' />
                    <p className='ml-2'>{message}</p>
                </div>
                {!showBtnClose &&
                <button
                    className={`mt-2 px-2 py-1 bg-${color ? color : 'brand'}-500 text-white rounded-md hover:bg-${color ? color : 'brand'}-700`}
                    onClick={handleClose}> Close
                </button>}
            </div>
        </div>
    );
};

export default NotificationOverlay;