import React, { useState } from 'react';
import Dropdown from 'components/dropdown';
import { AiOutlinePlus, AiOutlineUser } from 'react-icons/ai';
import { BsThreeDots } from 'react-icons/bs';
import { FiSettings } from 'react-icons/fi';
import { AiOutlineShop } from 'react-icons/ai';
import { TiLightbulb } from 'react-icons/ti';
import { BsThreeDotsVertical } from 'react-icons/bs';
import { FaFileCsv } from 'react-icons/fa6';

function CardMenu(props: {
  transparent?: boolean;
  vertical?: boolean;
  handlePopUp?: any;
  handleExport?: any;
  showExportBtn?: boolean;
  hideAddBtn?: boolean;
}) {
  const { transparent, vertical, handlePopUp, handleExport, showExportBtn, hideAddBtn } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <Dropdown
      button={
        <button
          onClick={() => setOpen(!open)}
          className={`flex items-center text-xl hover:cursor-pointer ${
            transparent
              ? 'bg-none text-white hover:bg-none active:bg-none'
              : vertical
              ? 'bg-none text-navy-700 dark:text-white'
              : 'bg-lightPrimary p-2 text-brand-500 hover:bg-gray-100 dark:bg-navy-700 dark:text-white dark:hover:bg-white/20 dark:active:bg-white/10'
          } linear justify-center rounded-lg font-bold transition duration-200`}
        >
          {vertical ? (
            <p className="text-[24px] hover:cursor-pointer">
              <BsThreeDotsVertical />
            </p>
          ) : (
            <BsThreeDots className="h-6 w-6" />
          )}
        </button>
      }
      animation={'origin-top-right transition-all duration-300 ease-in-out'}
      classNames={`${transparent ? 'top-8' : 'top-11'} right-0 w-max`}
    >
      <>
      {!hideAddBtn &&
      <div className="z-50 w-max rounded-xl bg-white px-4 py-3 text-sm shadow-xl shadow-shadow-500 dark:!bg-navy-700 dark:shadow-none">
        <p
          onClick={() => {
            handlePopUp();
            setOpen(false);
          }}
          className="hover:text-black flex cursor-pointer items-center gap-2 text-gray-600 hover:font-medium"
        >
          <span>
            <AiOutlinePlus />
          </span>
          Add New
        </p>
      </div>}
      {showExportBtn &&
      <div className="z-50 w-max rounded-xl bg-white px-4 py-3 text-sm shadow-xl shadow-shadow-500 dark:!bg-navy-700 dark:shadow-none">
        <p
          onClick={() => {
            setOpen(false);
            handleExport();
          }}
          className="hover:text-black flex cursor-pointer items-center gap-2 text-gray-600 hover:font-medium"
        >
          <span>
            <FaFileCsv />
          </span>
          Export Excel
        </p>
      </div>}
      </>
    </Dropdown>
  );
}

export default CardMenu;
