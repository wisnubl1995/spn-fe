'use client';
import Card from 'components/card';
import Image from 'next/image';
import Source from '/public/img/source.png';
import Ship from '/public/img/ship.gif';
import Destination from '/public/img/destination.gif';
import { FaArrowRightLong } from 'react-icons/fa6';
import { useState } from 'react';
import { useRouter } from 'next/navigation';

export type DeliveryCard = {
  uuid: string;
  status: string;
  source: string;
  ship: string;
  via: string;
  destination: string;
  date: any;
  arrived: any;
  popUp?: any;
  packages?: any;
  noResi?: string;
};

const DeliveryCard = ({
  uuid,
  status,
  destination,
  ship,
  source,
  date,
  arrived,
  popUp,
  via,
  packages,
  noResi,
}: DeliveryCard) => {
  let statusColor = 'bg-red-500'; // Default color for unknown status
  const [poppedUp, setPoppedUp] = useState(false);
  const router = useRouter()
  // Check the status and set the appropriate color
  if (status === 'On-Going') {
    statusColor = 'bg-yellow-400';
  } else if (status === 'Done') {
    statusColor = 'bg-green-400';
  }

  const handlePopUp = () => {
    setPoppedUp(true);
  };

  const handleClosePopUp = () => {
    setPoppedUp(false);
  };

  return (
    <Card extra="p-3 mt-5 pl-10">
      <div className="flex items-center justify-between gap-3">
        <div className="flex flex-col gap-2 pb-3">
          <div className="flex items-center gap-3">
            <div>Status:</div>
            <div
              className={`rounded-xl px-3 py-[1px] text-[13px] ${statusColor}`}
            >
              {status}
            </div>
          </div>
          <div className="flex items-center gap-3">
            <div>Tanggal:</div>
            <div
              className={`rounded-xl px-3 py-[1px] text-[13px] ${statusColor}`}
            >
              {date}
            </div>
          </div>
        </div>
        <div className="pr-10">
          <h3
            className="cursor-pointer text-xs font-bold text-blueSecondary hover:underline"
            onClick={() => {
              router.push(`outbound/details/${uuid}`)
            }}
          >
            Detail
          </h3>
        </div>
        {poppedUp && (
          <div className="fixed left-0 top-0 z-50 flex h-full w-full items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="h-[500px] w-[500px] overflow-y-scroll rounded-md bg-white p-5 shadow-md">
              {/* Popup content */}
              <div className="flex flex-col gap-3">
                <div className="flex gap-2">
                  <h3>Nama Kapal:</h3>
                  <h3>{ship}</h3>
                </div>
                <div className="flex gap-2">
                  <h3>Via:</h3>
                  <h3>{via}</h3>
                </div>
                <div className="flex gap-2">
                  <h3>No Resi:</h3>
                  <h3>{noResi}</h3>
                </div>
                <div className="flex gap-2">
                  <h3>Lokasi:</h3>
                  <h3>{source}</h3>
                </div>
                <div className="flex gap-2">
                  <h3>Tujuan:</h3>
                  <h3>{destination}</h3>
                </div>
                <div className="flex gap-2">
                  <h3>Tanggal Keberangkatan:</h3>
                  <h3>{date}</h3>
                </div>
                <div className="flex gap-2">
                  <h3>Tanggal Tiba:</h3>
                  <h3>{arrived || 'Belum Tiba'}</h3>
                </div>
                <div className="flex flex-col gap-2">
                  <h3>Inventory:</h3>
                  {packages.map((e: any, i: number) => {
                    return (
                      <div key={i}>
                        <div className="flex gap-2">
                          <h3>Nama Paket:</h3>
                          <h3>{e.productName}</h3>
                        </div>
                        <div className="flex gap-2">
                          <h3>Quantity:</h3>
                          <h3>{e.count} box</h3>
                        </div>
                        {/* <div className="flex flex-col">
                          <h3>Detail Paket:</h3>
                          {e.items.map((e: any, i: number) => {
                            return (
                              <div key={i}>
                                <h3>
                                  {e.name} - {e.stock} {e.satuan}
                                </h3>
                              </div>
                            );
                          })}
                        </div> */}
                      </div>
                    );
                  })}
                </div>
              </div>
              <button
                className="mt-3 rounded-md bg-blue-500 px-4 py-2 text-white"
                onClick={handleClosePopUp}
              >
                Close
              </button>
            </div>
          </div>
        )}
      </div>
      <div className="grid grid-cols-10 items-center">
        <div className="col-span-1 ">
          <div className="flex flex-col items-center gap-3">
            <div>
              <Image className="w-20" src={Source} alt="source" />
            </div>
            <div>
              <h3 className="text-xs font-thin">{source}</h3>
            </div>
          </div>
        </div>
        <div className="col-span-3 mx-auto">
          <FaArrowRightLong className="" />
        </div>
        <div className="col-span-1">
          <div className="flex h-full flex-col items-center gap-3 ">
            <div>
              <Image className="" src={Ship} alt="source" />
            </div>
            <div>
              <h3 className="mt-4 text-xs font-thin">{ship}</h3>
            </div>
          </div>
        </div>
        <div className="col-span-3 mx-auto">
          <FaArrowRightLong className="" />
        </div>
        <div className="">
          <div className="flex h-full flex-col items-center gap-3 ">
            <div>
              <Image src={Destination} alt="source" />
            </div>
            <div>
              <h3 className="text-xs font-thin">{destination}</h3>
            </div>
          </div>
        </div>
      </div>
    </Card>
  );
};

export default DeliveryCard;
