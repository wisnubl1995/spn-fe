import React, { useState } from 'react';
import CardMenu from 'components/card/CardMenu';
import Card from 'components/card';
import { MdCreate, MdOutlineDeleteForever } from 'react-icons/md';

import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import Image from 'next/image';
import { purchaseRequest } from 'variables/data-tables/purchaseRequest';
import { useRouter } from 'next/navigation';
import { formatDate } from 'utils/formatDate';

type RowObj = {
  uuid: any;
  shipName: any;
  department: any;
  category: any;
  purchaseRequestNo: any;
  urgency: any;
  status: any;
  requestedBy: any;
  location: any;
  createdAt: any;
  updatedAt: any;
  mocItems: any;
  edit: any;
};

function MocTable(props: {
  tableData: any;
  updateTableData?: (updatedData: any[]) => void;
  tableName: string;
}) {
  const { tableData, updateTableData } = props;
  const [detail, setDetail] = React.useState(false);
  const [sorting, setSorting] = React.useState<SortingState>([]);
  const [selectedItem, setSelectedItem] = React.useState(null);
  const [showDetail, setShowDetail] = React.useState(false);
  const [selectedRow, setSelectedRow] = React.useState(null);
  const [addNew, setAddNew] = React.useState(false);

  let isMounted = true;
  const [purchaseData, setPurchaseData] = useState<any>();
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = async () => {
    const url = `${process.env.NEXT_PUBLIC_API}purchaserequest?page=1&pageSize=10`;

    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();
      if (isMounted) {
        setPurchaseData(data.data);
        setIsLoading(false); // Update isLoading state to false
      }
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  let defaultData = tableData;
  const columns = [
    columnHelper.accessor('purchaseRequestNo', {
      id: 'purchaseRequestNo',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">PR No</p>
      ),
      cell: (info: any) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),

    columnHelper.accessor('department', {
      id: 'department',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          Deparment
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('category', {
      id: 'category',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          Category
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('shipName', {
      id: 'shipName',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">Ship</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),

    columnHelper.accessor('location', {
      id: 'location',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          Location
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('createdAt', {
      id: 'createdAt',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          Date Requested
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {formatDate(info.getValue())}
        </p>
      ),
    }),
    columnHelper.accessor('mocItems', {
      id: 'mocItems',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          Total Items
        </p>
      ),
      cell: (info) => (
        <p
          onClick={() => {
            setSelectedItem(info.getValue());
            setDetail(true);
          }}
          className="cursor-pointer text-sm font-bold text-navy-700 underline dark:text-white"
        >
          {info.getValue().length}
        </p>
      ),
    }),
    columnHelper.accessor('edit', {
      id: 'edit',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          Action
        </p>
      ),
      cell: (info) => (
        <div className="flex cursor-pointer gap-3">
          <p
            onClick={() => {
              setSelectedRow(info.row.original); // Set selected row
              setShowDetail(true); // Show edit popup
            }}
            className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
          >
            <MdCreate />
          </p>
          <p className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white">
            <MdOutlineDeleteForever />
          </p>
        </div>
      ),
    }),
  ];
  const [data, setData] = React.useState(() => [...defaultData]);
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    debugTable: true,
  });
  return (
    <Card extra={'w-full pb-10 p-4 h-full'}>
      <header className="relative flex items-center justify-between">
        <div className="text-xl font-bold text-navy-700 dark:text-white">
          {props.tableName}
        </div>
        <CardMenu
          handlePopUp={() => {
            setAddNew(true);
          }}
        />
      </header>

      <div className="mt-8 overflow-x-scroll ">
        <table className="w-full">
          <thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id} className="!border-px !border-gray-400">
                {headerGroup.headers.map((header) => {
                  return (
                    <th
                      key={header.id}
                      colSpan={header.colSpan}
                      onClick={header.column.getToggleSortingHandler()}
                      className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30"
                    >
                      <div className="items-center justify-between text-xs text-gray-200">
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                        {{
                          asc: '',
                          desc: '',
                        }[header.column.getIsSorted() as string] ?? null}
                      </div>
                    </th>
                  );
                })}
              </tr>
            ))}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row) => {
              return (
                <tr key={row.id}>
                  {row.getVisibleCells().map((cell) => {
                    return (
                      <td
                        key={cell.id}
                        className="min-w-[150px] border-white/0 py-3  pr-4"
                      >
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      {
        //show detail modal
        showDetail && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[400px] w-[1000px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">Edit Data</h2>
                <button
                  onClick={() => {
                    setShowDetail(false);
                    setSelectedRow({});
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>
              <div className="w-full pt-5">
                <div className="flex w-full flex-wrap items-center gap-3">
                  <div className="flex flex-col">
                    <label className="ml-2">PR No</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.purchaseRequestNo}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          purchase_request_no: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Department</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.department}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          department: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Category</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.category}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          category: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Ship</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.shipName}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          ship_name: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Requested By</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1 placeholder:font-bold placeholder:text-gray-900"
                      type="text"
                      disabled
                      placeholder={selectedRow.requestedBy}
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Location</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.location}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          location: e.target.value,
                        })
                      }
                    />
                  </div>
                  <button
                    type="submit"
                    onClick={() => {}}
                    className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                  >
                    Save Info
                  </button>
                </div>
              </div>
              <div className="pt-10">
                <table className="w-full">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Unit</td>
                      <td>Quantity</td>
                      <td>Sap Code</td>

                      <td>Additional Info</td>
                      <td>Image</td>
                      <td>Quantity Approved</td>
                      <td>
                        <button className="rounded bg-blue-500 px-1 py-2 text-white hover:bg-blue-600 focus:outline-none">
                          Check All
                        </button>
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedRow.mocItems.map(
                      (item, index) =>
                        // Conditionally render the row only if the status is "Open"
                        item.status === 'Open' && (
                          <tr className="border-b-2" key={index}>
                            <td>{item.description}</td>
                            <td>{item.unit}</td>
                            <td>{item.quantityAsked}</td>
                            <td>{item.sapCode}</td>
                            <td>{item.additionalInfo}</td>
                            <td>
                              <Image
                                className="h-[100px] w-[100px] object-cover"
                                src={
                                  'https://nimsteel.co.id/wp-content/uploads/2023/05/Cara-Memilih-Besi-Beton-Berkualitas-300x267.jpg'
                                }
                                alt="image"
                                width={100}
                                height={100}
                              />
                            </td>
                            <td>
                              <div className="flex items-center">
                                <input
                                  className="w-[120px] rounded-xl border-2 px-1 py-1"
                                  type="text"
                                  placeholder={item.quantityApproved}
                                  value={item.quantityApproved || ''}
                                  onChange={(e) => {
                                    const newValue = e.target.value;
                                    const updatedItems = [
                                      ...selectedRow.purchaseItems,
                                    ];
                                    updatedItems[index].quantityApproved =
                                      newValue; // Update quantityApproved for the specific item
                                    setSelectedRow((prevSelectedRow) => ({
                                      ...prevSelectedRow,
                                      purchaseItems: updatedItems, // Update purchaseItems with the modified item
                                    }));
                                  }}
                                />
                                <button
                                  className={`ml-2 rounded bg-blue-500 px-1 py-2 text-white hover:bg-blue-600 focus:outline-none ${
                                    !item.quantityApproved
                                      ? 'cursor-not-allowed opacity-50'
                                      : ''
                                  }`}
                                  disabled={!item.quantityApproved}
                                  onClick={() => {
                                    setShowDetail(true);
                                  }}
                                >
                                  Change
                                </button>
                              </div>
                            </td>

                            <td>
                              <input
                                type="checkbox"
                                checked={item.isChecked || false}
                                onChange={(e) => {
                                  const isChecked = e.target.checked;
                                  const updatedItems = [
                                    ...selectedRow.purchaseItems,
                                  ];
                                  updatedItems[index].isChecked = isChecked;
                                  setSelectedRow((prevSelectedRow) => ({
                                    ...prevSelectedRow,
                                    purchaseItems: updatedItems,
                                  }));
                                }}
                              />
                            </td>
                          </tr>
                        ),
                    )}
                  </tbody>
                </table>

                <button>Add To MOC</button>
              </div>
            </div>
          </div>
        )
      }

      {addNew && (
        <div className="bg-black fixed inset-0 flex items-center justify-center bg-opacity-50">
          <div className="h-[400px] w-[1000px] overflow-auto rounded bg-white p-4 shadow-md">
            <div className="flex items-center justify-between">
              <h2 className="mb-4 text-lg font-bold">New Purchase Request</h2>
              <button
                onClick={() => {
                  setAddNew(false);
                }}
                className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
              >
                Close
              </button>
            </div>

            <button className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none">
              Add New
            </button>
          </div>
        </div>
      )}
    </Card>
  );
}

export default MocTable;
const columnHelper = createColumnHelper<RowObj>();
