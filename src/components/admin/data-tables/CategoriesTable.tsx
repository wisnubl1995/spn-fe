import React from 'react';
import CardMenu from 'components/card/CardMenu';
import Card from 'components/card';

import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { formatDate } from 'utils/formatDate';
import { MdCreate } from 'react-icons/md';
import CustomTooltip from 'components/tooltip/CustomTooltip';

type RowObj = {
  uuid: string;
  name: [string, boolean];
  desc: string;
  createdAt: any;
  updatedAt: string;
  action: any;
};

type Props = {
  onHandleAdd: () => void;
  onHandleEdit: (info: any) => void;
  tableData: any[];
  tableName: string;
  children?: JSX.Element | any[];
  [x: string]: any;
};

function CategoriesTable({
  tableData,
  tableName,
  onHandleAdd,
  onHandleEdit,
}: Props) {
  const [sorting, setSorting] = React.useState<SortingState>([]);
  const [pageIndex, setPageIndex] = React.useState(0);
  const [pageSize, setPageSize] = React.useState(10);
  let defaultData = tableData || [];
  const columns = [
    columnHelper.accessor('name', {
      id: 'name',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">NAME</p>
      ),
      cell: (info: any) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('desc', {
      id: 'desc',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">DESC</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('createdAt', {
      id: 'createdAt',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          CREATED AT
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {formatDate(info.getValue())}
        </p>
      ),
    }),
    columnHelper.accessor('updatedAt', {
      id: 'updatedAt',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          UPDATED AT
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {formatDate(info.getValue())}
        </p>
      ),
    }),
    columnHelper.accessor('action', {
      id: 'action',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          ACTION
        </p>
      ),
      cell: (info: any) => (
        <div className="flex cursor-pointer gap-3">
          <CustomTooltip text="Edit Data">
            <p
              onClick={() => onHandleEdit(info)}
              className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
            >
              <MdCreate />
            </p>
          </CustomTooltip>
        </div>
      ),
    }),
  ]; // eslint-disable-next-line
  const [data, setData] = React.useState(() => [...defaultData]);
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
      pagination: {
        pageIndex,
        pageSize,
      },
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    debugTable: true,
  });

  // Update the data state based on the current page index and page size
  React.useEffect(() => {
    const startIndex = pageIndex * pageSize;
    const endIndex = Math.min(startIndex + pageSize, defaultData.length);
    setData(defaultData.slice(startIndex, endIndex));
  }, [pageIndex, pageSize, defaultData]);

  // Event handler for going to the previous page
  const goToPreviousPage = () => {
    setPageIndex((old) => Math.max(old - 1, 0));
  };

  // Event handler for going to the next page
  const goToNextPage = () => {
    setPageIndex((old) =>
      Math.min(old + 1, Math.ceil(defaultData.length / pageSize) - 1),
    );
  };

  // Disable previous and next buttons based on current page
  const canGoPrevious = pageIndex > 0;
  const canGoNext = pageIndex < Math.ceil(defaultData.length / pageSize) - 1;

  return (
    <Card extra={'w-full pb-10 p-4 h-full'}>
      <header className="relative flex items-center justify-between">
        <div className="text-xl font-bold text-navy-700 dark:text-white">
          {tableName}
        </div>
        <CardMenu handlePopUp={onHandleAdd} />
      </header>

      <div className="mt-8 overflow-x-scroll xl:overflow-x-hidden">
        <table className="w-full">
          <thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id} className="!border-px !border-gray-400">
                {headerGroup.headers.map((header) => {
                  return (
                    <th
                      key={header.id}
                      colSpan={header.colSpan}
                      onClick={header.column.getToggleSortingHandler()}
                      className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30"
                    >
                      <div className="items-center justify-between text-xs text-gray-200">
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                        {{
                          asc: '',
                          desc: '',
                        }[header.column.getIsSorted() as string] ?? null}
                      </div>
                    </th>
                  );
                })}
              </tr>
            ))}
          </thead>
          <tbody>
            {table
              .getRowModel()
              .rows.slice(0, 10)
              .map((row) => {
                return (
                  <tr key={row.id}>
                    {row.getVisibleCells().map((cell) => {
                      return (
                        <td
                          key={cell.id}
                          className="min-w-[150px] border-white/0 py-3  pr-4"
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext(),
                          )}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>

      <div className="mt-4 flex items-center justify-between">
        <button
          onClick={goToPreviousPage}
          disabled={!canGoPrevious}
          className="rounded bg-gray-300 px-4 py-2"
        >
          Previous
        </button>
        <span>
          Page <strong>{pageIndex + 1}</strong> of{' '}
          <strong>{Math.ceil(defaultData.length / pageSize)}</strong>
        </span>
        <button
          onClick={goToNextPage}
          disabled={!canGoNext}
          className="rounded bg-gray-300 px-4 py-2"
        >
          Next
        </button>
      </div>
    </Card>
  );
}

export default CategoriesTable;
const columnHelper = createColumnHelper<RowObj>();
