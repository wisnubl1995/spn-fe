import React from 'react';
import CardMenu from 'components/card/CardMenu';
import Card from 'components/card';

import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { formatDate } from 'utils/formatDate';
import { MdCreate } from 'react-icons/md';

type RowObj = {
  uuid: string;
  noPr: string;
  shipName: string;
  deliverTo: string;
  via: string;
  noResi: string;
  location: string;
  createdAt: any;
  updatedAt: string;
  action: any;
};

type Props = {
  onHandleAdd?: () => void,
  onHandleExport?: () => void,
  onSearchLocation?: (event: any) => void,
  onHandleEdit?: (info: any) => void,
  tableData: any[],
  tableName: string,
  valueSearch?: string,
  children?: JSX.Element | any[],
  currentPage: number,
  canGoNext: boolean,
  canGoPrev: boolean,
  prevHandler?: () => void,
  nextHandler?: () => void,
  [x: string]: any;
};

function DeliveryTable({ tableData, tableName, onHandleAdd, onHandleEdit, valueSearch, onSearchLocation, onHandleExport, currentPage, prevHandler, nextHandler, canGoNext, canGoPrev } : Props) {
  const [sorting, setSorting] = React.useState<SortingState>([]);
  let defaultData = tableData || [];
  const columns = [
    columnHelper.accessor('noPr', {
      id: 'noPr',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">PR NO.</p>
      ),
      cell: (info: any) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue() ? info.getValue() : '-'}
        </p>
      ),
    }),
    columnHelper.accessor('shipName', {
      id: 'shipName',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">SHIP</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('deliverTo', {
      id: 'deliverTo',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">DELIVER TO</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('via', {
      id: 'via',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">VIA</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('noResi', {
      id: 'noResi',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">NO. RESI</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('location', {
      id: 'location',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">LOCATION</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {info.getValue()}
        </p>
      ),
    }),
    columnHelper.accessor('createdAt', {
      id: 'createdAt',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">CREATED AT</p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {formatDate(info.getValue())}
        </p>
      ),
    }),
    columnHelper.accessor('updatedAt', {
      id: 'updatedAt',
      header: () => (
        <p className="text-sm font-bold text-gray-600 dark:text-white">
          UPDATED AT
        </p>
      ),
      cell: (info) => (
        <p className="text-sm font-bold text-navy-700 dark:text-white">
          {formatDate(info.getValue())}
        </p>
      ),
    }),
  ]; // eslint-disable-next-line
  const [data, setData] = React.useState(() => [...defaultData]);
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    debugTable: true,
  });
  return (
    <Card extra={'w-full pb-10 p-4 h-full'}>
      <header className="relative flex items-center justify-between">
        <div className="text-xl font-bold text-navy-700 dark:text-white">
          {tableName}
        </div>
        <div className="flex">
          <div className="flex mr-2">
            <label className="text-sm font-bold text-navy-700">Search Location : </label>
            <input id="location" name="location" placeholder="Search by Location" value={valueSearch} onChange={onSearchLocation} className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none" />
          </div>
          <CardMenu
            hideAddBtn={true}
            showExportBtn={true}
            handleExport={onHandleExport}
          />
        </div>
      </header>

      <div className="mt-8 overflow-x-scroll xl:overflow-x-hidden">
        <table className="w-full">
          <thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id} className="!border-px !border-gray-400">
                {headerGroup.headers.map((header) => {
                  return (
                    <th
                      key={header.id}
                      colSpan={header.colSpan}
                      onClick={header.column.getToggleSortingHandler()}
                      className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30"
                    >
                      <div className="items-center justify-between text-xs text-gray-200">
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                        {{
                          asc: '',
                          desc: '',
                        }[header.column.getIsSorted() as string] ?? null}
                      </div>
                    </th>
                  );
                })}
              </tr>
            ))}
          </thead>
          <tbody>
            {table
              .getRowModel()
              .rows.slice(0, 5)
              .map((row) => {
                return (
                  <tr key={row.id}>
                    {row.getVisibleCells().map((cell) => {
                      return (
                        <td
                          key={cell.id}
                          className="min-w-[150px] border-white/0 py-3  pr-4"
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext(),
                          )}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>

      <div className="mt-6 flex items-center justify-between">
        <button
          onClick={prevHandler}
          disabled={!canGoPrev}
          className="rounded bg-gray-300 px-4 py-2">
          Previous
        </button>
        <span>
          Page <strong>{currentPage}</strong>
        </span>
        <button
          onClick={nextHandler}
          disabled={!canGoNext}
          className="rounded bg-gray-300 px-4 py-2"
        >
          Next
        </button>
      </div>
    </Card>
  );
}

export default DeliveryTable;
const columnHelper = createColumnHelper<RowObj>();
