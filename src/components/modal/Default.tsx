import React from 'react'

type Props = {
    onClose: () => void,
    children?: JSX.Element | any[],
    title: string,
    [x: string]: any;
};

const DefaultModal = ({ onClose, children, title }: Props) => {
    return (
        <div className="bg-opacity-75 bg-gray-800 border-black fixed inset-0 flex items-center justify-center border-2 shadow-2xl z-40">
            <div className="max-h-[800px] max-w-full min-w-[700px] overflow-auto rounded bg-white p-4 shadow-md">
                <div className="flex items-center justify-between">
                    <h2 className="mb-4 text-lg font-bold">{title}</h2>
                    <button onClick={onClose} className="mt-4 rounded bg-red-500 px-4 py-2 text-white hover:bg-red-600 focus:outline-none"> Close </button>
                </div>
                {children}
            </div>
        </div>
    )
}

export default DefaultModal