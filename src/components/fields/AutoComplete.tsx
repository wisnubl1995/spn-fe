import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import React, { useState, useEffect, useRef, useContext } from 'react';
import { FaPlus } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import { buildQuery } from 'utils/helper';

interface Tag {
  label?: string;
  value?: string;
  isNew?: boolean;
}

interface AutocompleteInputProps {
  placeholder?: string; // Optional placeholder text
  apiUrl: string; // API endpoint for fetching suggestions
  searchQuery: string; // API endpoint for fetching suggestions
  keyLabel: string;
  keyValue: string;
  minTreshold?: number;
  disabled?: boolean;
  defaultValue?: string;
  onSelect?: (suggestion: Tag) => void; // Callback for handling selected suggestion (optional)
}

const AutocompleteInput: React.FC<AutocompleteInputProps> = ({
  placeholder = 'Enter tag',
  apiUrl,
  searchQuery,
  keyLabel,
  keyValue,
  minTreshold,
  disabled,
  defaultValue,
  onSelect,
}) => {
  const { sessionUser } = useContext(AuthContext)
  const headersReq = {
		"account-access": sessionUser?.uuid,
    timeout: 15000
	}
  const [searchTerm, setSearchTerm] = useState(defaultValue);
  const [suggestions, setSuggestions] = useState<Tag[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const previousSearchTermRef = useRef(searchTerm); // Use useRef to store previous search term
  const [showAddButton, setShowAddButton] = useState(false);
  const [showNotif, setShowNotif] = useState(false)
  const [notifMessage, setNotifMessage] = useState(null)
  let timeoutId: number | any;

  useEffect(() => {
      
      const fetchData = async () => {
        if (searchTerm?.length >= (minTreshold ? minTreshold : 3) && searchTerm !== previousSearchTermRef.current) {
            setIsLoading(true);
            try {
                const searchParam = apiUrl.split("?")[1]
                const urlSearchParams = new URLSearchParams(searchParam);
                const params = Object.fromEntries(urlSearchParams.entries());
                const newUrl = buildQuery(apiUrl.split('?')[0], {...params, [searchQuery]: searchTerm})
                const response = await sendRequestApi(newUrl, 'GET', null, headersReq);
                if (response?.data) {
                  setSuggestions(response.data);
                  setShowAddButton(response.data.length === 0);
                }else{
                  setSuggestions([]);
                  setShowAddButton(true);
                }

            } catch (error) {
              setShowNotif(true)
              setNotifMessage(`Failed! Error fetching suggestion : ${error?.message.includes('signal is aborted') ? 'Request timeout!' : error?.message}`)
                console.error('Error fetching suggestions:', error);
            } finally {
                setIsLoading(false);
            }

        } else {
            setSuggestions([]);
        }

        previousSearchTermRef.current = searchTerm; 
    };
    
    const debouncedFetch = debounce(fetchData, 500); // Debounce to avoid excessive requests
    debouncedFetch();

    // Cleanup function to clear timeout on unmount
    return () => clearTimeout(timeoutId);;
  }, [searchTerm, apiUrl]);

  const handleAddCustomItem = () => {
    setSearchTerm(searchTerm); // Set search term to itself to trigger selection
    setSuggestions([]);
    setShowAddButton(false); // Hide button after adding custom item
    if (onSelect) 
      onSelect({ [keyLabel]: searchTerm, [keyValue]: searchTerm, isNew: true }); // Call callback with custom item
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const handleSelectSuggestion = (suggestion: Tag) => {
    previousSearchTermRef.current = suggestion[keyLabel]; 
    setSearchTerm(suggestion[keyLabel]);
    setSuggestions([]);
    if (onSelect) onSelect(suggestion); // Call optional callback on selection
  };

  // Debounce function (replace with your preferred debounce implementation)
  const debounce = (func, delay) => {
    return (...args) => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => func.apply(this, args), delay);
    };
  };

	return (
		<div className="relative my-1 w-full">
			<input
				type="text"
        disabled={disabled && disabled === true ? true : false}
				className="w-full px-3 py-2 rounded-md border border-gray-300 focus:outline-none focus:ring-1 focus:ring-blue-500"
				placeholder={placeholder}
				value={searchTerm}
				onChange={handleInputChange}
			/>
			{isLoading && <div className="absolute top-0 left-0 w-full h-full bg-white opacity-75 z-10"></div>}
			{showAddButton && ( // Conditionally render button if needed
				<button
					className="absolute top-right right-0 px-3 py-2 text-sm font-medium rounded-md bg-green-500 text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
					onClick={handleAddCustomItem}
				>
					<FaPlus className='h-6 w-6' />
				</button>
			)}
			{suggestions?.length > 0 && (
				<ul className="absolute mt-1 w-full rounded-md shadow-md bg-white z-10 overflow-y-auto max-h-60">
					{suggestions.map((suggestion) => (
					<li
						key={suggestion[keyLabel]}
						className="px-3 py-2 hover:bg-gray-100 cursor-pointer"
						onClick={() => handleSelectSuggestion(suggestion)}
					>
						{suggestion[keyValue]}
					</li>
					))}
				</ul>
			)}
      {showNotif && 
        <NotificationOverlay 
            isShow={showNotif}
            title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
            message={notifMessage}
            handleClose={() => setShowNotif(false)}
            color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
		</div>
	);
};

export default AutocompleteInput;
