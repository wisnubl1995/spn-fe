import React, { useEffect, useState } from 'react';
import { FaFileUpload } from 'react-icons/fa'; // Assuming you have Font Awesome icons installed

interface FileUploadProps {
  onUpload: (file: File) => void; // Callback function to handle uploaded file
  accept?: string; // Optional MIME type filter for accepted file types (e.g., '.csv, .txt')
  title?: string;
}

const FileUpload: React.FC<FileUploadProps> = ({ onUpload, accept, title }) => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    setSelectedFile(file); // Update state with selected file
  };

  const handleUploadClick = () => {
    if (selectedFile) {
      onUpload(selectedFile); // Call the callback function with the selected file
      setSelectedFile(null); // Reset state after upload
    } else {
      console.error('No file selected for upload.');
    }
  };

  useEffect(() => {
    if (selectedFile) {
        onUpload(selectedFile);
        setSelectedFile(null)
    }
  }, [selectedFile])

  return (
    <div className="relative">
      <input
        type="file"
        accept={accept} // Apply MIME type filter if provided
        className="absolute top-0 left-0 w-full h-full opacity-0 cursor-pointer"
        onChange={handleInputChange}
      />
      <button
        type="button"
        onClick={handleUploadClick}
        className="rounded-md bg-cyan-600 px-4 py-2 text-md cursor-pointer text-white shadow-sm hover:bg-cyan-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-cyan-600"
      >
        <FaFileUpload className="inline-block h-5 w-6" /> {title ? title : `Upload`}
      </button>
    </div>
  );
};

export default FileUpload;