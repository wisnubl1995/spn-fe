import React, { ReactNode } from 'react';
import AppWrappers from './AppWrappers';
import AuthContextProvider from 'contexts/AuthContextProvider';
import RootHead from './head';
// import '@asseinfo/react-kanban/dist/styles.css';
// import '/public/styles/Plugins.css';

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="en">
      <RootHead />
      <body id={'root'}>
      <AuthContextProvider>
          <AppWrappers>
            {children}
          </AppWrappers>
      </AuthContextProvider>
      </body>
    </html>
  );
}
