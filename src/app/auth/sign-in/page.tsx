'use client';
import InputField from 'components/fields/InputField';
import Default from 'components/auth/variants/DefaultAuthLayout';
import { FcGoogle } from 'react-icons/fc';
import Checkbox from 'components/checkbox';
import { NavLink } from 'react-router-dom';
import Link from 'next/link';
import { useContext, useEffect, useState } from 'react';
import AuthContext from 'contexts/AuthContext';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import { redirect, useSearchParams } from 'next/navigation';

function SignInDefault() {
	const query = useSearchParams()
	const isLogout = query.get('logout')
	const [isLoading, setIsLoading] = useState(false);
	const { checkLogin, setIsLogin, sessionUser, setSessionUser } = useContext(AuthContext);
	const [notifSuccess, setNotifSuccess] = useState(false);
	const [notifFailed, setNotifFailed] = useState(false);
	const [formData, setFormData] = useState<any>({
		username: '',
		password: ''
	})

	const loginHandler = async () => {
		setIsLoading(true)
		try {
			const success = await checkLogin(formData)
			if (success) {
				setIsLogin(success)
				setNotifSuccess(true)
			}else{
				setNotifFailed(true)
			}
			setIsLoading(false)
		} catch (error) {
			setIsLoading(false)
			setNotifFailed(true)
			console.log('Login failed!')
		}
	}

	const enterPressed = async (event: any) => {
		var code = event.keyCode || event.which;
		if(code === 13) { //13 is the enter keycode
			await loginHandler()
		}
	}

	const logoutHandler = () => {
		console.log('logouted')
		setIsLoading(true)
		setSessionUser(null)
		localStorage.removeItem('@session_user');
		setIsLoading(false)
		redirect('/auth/sign-in')
	}

	useEffect(() => {
		if (isLogout) {
			logoutHandler()
		}else if (sessionUser?.name) {
			redirect('/admin/home')
		}
	}, [isLogout, sessionUser])

  	if (isLoading)
		return <OverlayLoading />

  return (
	<>
    <Default
      maincard={
        <div className="mb-16 mt-16 flex h-full w-full items-center justify-center px-2 md:mx-0 md:px-0 lg:mb-10 lg:items-center lg:justify-start">
          {/* Sign in section */}
          <div className="mt-[10vh] w-full max-w-full flex-col items-center md:pl-4 lg:pl-0 xl:max-w-[420px]">
		  	<div className="flex justify-between items-center">
				<h3 className="mb-2.5 text-4xl font-bold text-navy-700 dark:text-white">
					Sign In
				</h3>
				<div className="w-4/12">
					<img src="/img/logo/spn.png" alt="Logo SPN" width={300} height={300}/>
				</div>
			</div>
            <p className="mb-7 ml-1 text-base text-gray-600">
              Enter your username and password to sign in!
            </p>
            {/* Email */}
            <InputField
				variant="auth"
				extra="mb-3"
				label="Username*"
				placeholder="username"
				id="username"
				type="text"
				onKeyPress={enterPressed}
				onChange={(e: any) =>
					setFormData({
						...formData,
						username: e.target.value,
					})
				}
            />

            {/* Password */}
            <InputField
				variant="auth"
				extra="mb-3"
				label="Password*"
				placeholder="Min. 8 characters"
				id="password"
				type="password"
				onKeyPress={enterPressed}
				onChange={(e: any) =>
					setFormData({
						...formData,
						password: e.target.value,
					})
				}
            />
            {/* Checkbox */}
            {/* <div className="mb-4 flex items-center justify-between px-2">
              <div className="mt-2 flex items-center">
                <Checkbox />
                <p className="ml-2 text-sm font-medium text-navy-700 dark:text-white">
                  Keep me logged In
                </p>
              </div>
              <a
                className="text-sm font-medium text-brand-500 hover:text-brand-600 dark:text-white"
                href=" "
              >
                Forgot Password?
              </a>
            </div> */}
            <div className="mt-0 lg:pt-10">
              <button onClick={loginHandler} className="linear w-full rounded-xl bg-brand-500 py-3 text-base font-medium text-white transition duration-200 hover:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:text-white dark:hover:bg-brand-300 dark:active:bg-brand-200">
                Sign In
              </button>
            </div>
          </div>
        </div>
      }
    />

		{notifSuccess && <NotificationOverlay isShow={notifSuccess} handleClose={() => setNotifSuccess(false)} title='Success' message='Action completed successfully!' color='green' />}
		{notifFailed && <NotificationOverlay isShow={notifFailed} handleClose={() => setNotifFailed(false)} title='Failed' message='Something went wrong. Please try again!' color='red'  />}
	</>
  );
}

export default SignInDefault;
