'use client';
import React, {
  ReactNode,
  ReactElement,
  JSXElementConstructor,
  lazy,
  Suspense,
} from 'react';

import 'styles/App.css';
import 'styles/Contact.css';
import 'styles/MiniCalendar.css';
import 'styles/index.css';

import dynamic from 'next/dynamic';

const NoSSR = lazy(() => import(/* webpackChunkName: "NoSSR" */ './Nossr'));

export default function AppWrappers({ children }: { children: ReactNode }) {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      {typeof window !== 'undefined' && <NoSSR>{children}</NoSSR>}
    </Suspense>
  );
}
