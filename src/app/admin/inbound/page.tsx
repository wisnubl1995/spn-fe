'use client';

import Card from 'components/card';
import CardMenu from 'components/card/CardMenu';
import OverlayLoading from 'components/loading/OverlayLoading';
import DefaultModal from 'components/modal/Default';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';

import React, { useContext, useEffect, useState } from 'react';
import { FaCheck } from 'react-icons/fa6';
import { TiArrowForward } from 'react-icons/ti';
import { sendRequestApi } from 'utils/api/fetchData';
import exportToExcel from 'utils/exportExcel';

import { formatDate } from 'utils/formatDate';
import { formattedDate } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
  const { sessionUser } = useContext(AuthContext);
  const headersReq = {
		"account-access": sessionUser?.uuid
	}
  const [activeTab, setActiveTab] = useState('Pending');
  const [inboundData, setInboundData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchLocation, setSearchLocation] = useState('')
  const [showModalForm, setShowModalForm] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [showNotif, setShowNotif] = useState(false)
  const [notifMessage, setNotifMessage] = useState(null)
  const [currentPage, setCurrentPage] = useState(1);
  let timeoutId: number | any;

  const fetchData = async () => {
    setIsLoading(true);
    const url = `${process.env.NEXT_PUBLIC_API}inbound?page=${currentPage}&pageSize=10&location=${searchLocation}`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (data.data) {
          setInboundData(data.data);
        }else{
          setInboundData([])
        }
      } else {
        setInboundData([]);
      }

      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      setIsLoading(false);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const handleApprove = (idPo, location) => {
    const payload = {
      id: idPo,
      location: location,
    };
    setIsLoading(true);

    fetch(`${process.env.NEXT_PUBLIC_API}inbound/movetoinventory`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "account-access": sessionUser?.uuid
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (response.ok) {
          console.log('Data sent successfully');
        } else {
          console.error('Failed to send data');
        }
        fetchData()
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        console.error('Error:', error);
      });
  };

  useEffect(() => {
    const debouncedFetch = debounce(fetchData, 1000); // Debounce to avoid excessive requests
    debouncedFetch();

    // Cleanup function to clear timeout on unmount
    return () => clearTimeout(timeoutId);;
  }, [searchLocation])

  const debounce = (func, delay) => {
    return (...args) => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => func.apply(this, args), delay);
    };
  };

  const handleSubmit = async (savedData: any) => {
    setIsLoading(true)
    
    const payload = {
      id: selectedData.uuid,
      ...savedData
    }
    
    try {
        const resp = await sendRequestApi(`inbound/directtoclient`, 'POST', JSON.stringify(payload), headersReq)
        
        setNotifMessage(resp?.message)
        setShowNotif(true)
        setIsLoading(false)
        await fetchData()
    } catch (error) {
        setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
        setShowNotif(true)
        setIsLoading(false)
        console.log(`Failed submit purchase order!`, error?.message)
    }
  }

  useEffect(() => {
    fetchData()
  }, [currentPage])

  const handleExport = () => {
    setIsLoading(true);
    const tmpData = [];
    for (let i = 0; i < inboundData.length; i++) {
      const header = inboundData[i];
      tmpData.push({
        PURCHASE_REQUEST_NO: header.purchaseRequestNo,
        PRODUCT_NAME: header.productName,
        SATUAN: header.unit,
        QUANTITY: header.count,
        ORDER_NUMBER: header.orderNumber,
        NAMA_KAPAL: header.shipName,
        KATEGORI_KAPAL: header.categoryShip,
        LOKASI: header.location,
        CREATED_BY: header.createdBy ? header.createdBy : '-',
        CREATED_AT: header.createdAt
          ? formattedDate(new Date(header.createdAt), 'YYYY-MM-DD')
          : header.createdAt,
        STATUS_INBOUND: header.status === "Close" ? "COMPLETED" : "PENDING"
      });
    }

    exportToExcel(tmpData, 'INBOUND', 'DATA');
    setIsLoading(false);
  };

  if (isLoading) {
    return <OverlayLoading />
  }

  const pendingData = inboundData
    ? inboundData.filter((order) => order.status === 'Pending')
    : 'Loading';
  const completeData = inboundData
    ? inboundData.filter((order) => order.status === 'Close')
    : 'Loading';

  const canGoPrev = currentPage < 1
  const canGoNext = inboundData.length >= 10

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="text-xl font-bold text-navy-700 dark:text-white">
            Inbound
          </div>
          <div className="flex items-center mr-2">
            <div className="flex items-center mr-2">
              <label className="text-sm font-bold text-navy-700">Search Location : </label>
              <input 
                id="location" 
                name="location" 
                placeholder="Search by Location" value={searchLocation} 
                onChange={(e) => {
                  setSearchLocation(e.target.value)
                }} 
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none" />
            </div>
              <CardMenu
                hideAddBtn={true}
                showExportBtn={true}
                handleExport={handleExport}
              />
          </div>
        </header>

        <div className=" flex">
          <button
            className={`mr-2 rounded-md px-4 py-2 focus:outline-none ${
              activeTab === 'Pending' ? 'bg-blue-500 text-white' : 'bg-gray-200'
            }`}
            onClick={() => setActiveTab('Pending')}
          >
            Pending
          </button>
          <button
            className={`rounded-md px-4 py-2 focus:outline-none ${
              activeTab === 'Complete'
                ? 'bg-blue-500 text-white'
                : 'bg-gray-200'
            }`}
            onClick={() => setActiveTab('Complete')}
          >
            Complete
          </button>
        </div>

        <div className=" overflow-x-scroll ">
          <table className="w-full">
            <thead>
              <tr className="!border-px !border-gray-400">
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      PR Number
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Product Name
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Quantity
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Status
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Ship
                    </p>
                  </div>
                </th>

                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Location
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Date Requested
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Action
                    </p>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {activeTab === 'Pending' &&
                pendingData?.map((v: any, i: any) => (
                  <tr key={i}>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v?.purchaseRequestNo ? v.purchaseRequestNo : '-'}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.productName}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.count}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.status}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.shipName}
                      </p>
                    </td>

                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.location}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {formatDate(v.createdAt)}
                      </p>
                    </td>

                    <td className="min-w-[150px] border-white/0 py-3 pr-4 flex flex-col items-stretch">
                        <button
                          onClick={() => handleApprove(v.uuid, v.location)}
                          disabled={v.status == 'Close' ? true : false}
                          className={`my-1 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none
                          ${
                            v.status == 'Close'
                              ? 'bg-gray-500 hover:bg-gray-600'
                              : ''
                          }
                          `}
                        >
                          <FaCheck className='inline-block' /> Approve
                        </button>
                        <button
                          onClick={() => {
                            setSelectedData(v)
                            setShowModalForm(true)
                          }}
                          disabled={v.status == 'Close' ? true : false}
                          className={`my-1 rounded bg-cyan-500 px-4 py-2 text-white hover:bg-cyan-600 focus:outline-none
                          ${
                            v.status == 'Close'
                              ? 'bg-gray-500 hover:bg-gray-600'
                              : ''
                          }
                          `}
                        >
                          <TiArrowForward className="inline-block" /> Direct to Client
                        </button>
                    </td>
                  </tr>
                ))}

              {activeTab === 'Complete' &&
                completeData.map((v: any, i: any) => (
                  <tr key={i}>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v?.purchaseRequestNo ? v.purchaseRequestNo : '-'}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.productName}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.count}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.status}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.shipName}
                      </p>
                    </td>

                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.location}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {formatDate(v.createdAt)}
                      </p>
                    </td>

                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      {/* <button
                        onClick={() => handleApprove(v.uuid)}
                        disabled={v.status == 'Close' ? true : false}
                        className={`mt-4  rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none
                        ${
                          v.status == 'Close'
                            ? 'bg-gray-500 hover:bg-gray-600'
                            : ''
                        }
                        `}
                      >
                        <FaCheck className='inline-block' /> Approve
                      </button> */}
                      -
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>

        <div className="mt-6 flex items-center justify-between">
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage - 1
              setCurrentPage(page)
            }}
            disabled={!canGoPrev}
            className="rounded bg-gray-300 px-4 py-2">
            Previous
          </button>
          <span>
            Page <strong>{currentPage}</strong>
          </span>
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage + 1
              setCurrentPage(page)
            }}
            disabled={!canGoNext}
            className="rounded bg-gray-300 px-4 py-2"
          >
            Next
          </button>
        </div>

      </Card>

      {showModalForm && <ModalDirectToClient 
				onClose={() => { 
          setShowModalForm(false)
          setSelectedData(null)
        }}
				onSubmit={handleSubmit} />}

      {showNotif && 
        <NotificationOverlay 
            isShow={showNotif}
            title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
            message={notifMessage}
            handleClose={() => setShowNotif(false)}
            color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
    </div>
  );
};

const ModalDirectToClient = ({ onClose, onSubmit, defaultValue }: any) => {
	const [formData, setFormData] = useState<any>(defaultValue ? defaultValue : {
		deliverTo: '',
		via: '',
		noResi: '',
	})

	return (
		<DefaultModal title="Direct to Client" onClose={onClose}>
			<div className="mt-6 border-b border-t border-gray-900/10 pb-12">
				<div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
					<div className="col-span-full">
						<label htmlFor="deliverTo" className="block text-sm font-medium leading-6 text-gray-900">
							Deliver To
						</label>
						<div className="mt-2">
							<input
								id="deliverTo"
								name="deliverTo"
								type="text"
								value={formData?.deliverTo}
								onChange={(e) =>
									setFormData({
										...formData,
										deliverTo: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
					<div className="col-span-full">
						<label htmlFor="via" className="block text-sm font-medium leading-6 text-gray-900">
							Via
						</label>
						<div className="mt-2">
							<input
								id="via"
								name="via"
								type="text"
								value={formData?.via}
								onChange={(e) =>
									setFormData({
										...formData,
										via: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
					<div className="col-span-full">
						<label htmlFor="noResi" className="block text-sm font-medium leading-6 text-gray-900">
							No. Resi
						</label>
						<div className="mt-2">
							<input
								id="noResi"
								name="noResi"
								type="text"
								value={formData?.noResi}
								onChange={(e) =>
									setFormData({
										...formData,
										noResi: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
				</div>
			</div>
			<div className="mt-6 flex items-center justify-end gap-x-6">
				<button
					type="button"
					onClick={() => {
						setFormData({
							deliverTo: '',
              via: '',
              noResi: '',
						})
						onSubmit(formData)
						onClose()
					}}
					className="rounded-md bg-green-600 px-4 py-2 text-md text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"> Submit
				</button>
			</div>
		</DefaultModal>
	)
}

export default Page;
