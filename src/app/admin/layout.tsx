'use client';

import { usePathname } from 'next/navigation';
import { useContext, useEffect, useState } from 'react';
import routes from 'routes';
import {
  getActiveNavbar,
  getActiveRoute,
  isWindowAvailable,
} from 'utils/navigation';
import React from 'react';
import Navbar from 'components/navbar';
import Sidebar from 'components/sidebar';
import Footer from 'components/footer/Footer';
import AuthContext from 'contexts/AuthContext';

export default function Admin({ children }: { children: React.ReactNode }) {
  const { sessionUser } = useContext(AuthContext)
  const [open, setOpen] = useState(false);
  const pathname = usePathname();

  const checkModulesRoutes = () => {
    const currentModuleAccess = sessionUser?.modules || []
    const tmpRoutes = []
    for (let i = 0; i < routes.length; i++) {
      const route = routes[i];
      const path = `${route.layout}/${route.path}`
      const idExists = currentModuleAccess.findIndex(mod => mod.moduleRouter === path)
      if (idExists !== -1 || path.includes('/auth/sign-in')) {
        tmpRoutes.push(route)
      }
    }
    return tmpRoutes
  }

  useEffect(() => {
    checkModulesRoutes()
  }, [pathname])

  if (isWindowAvailable()) document.documentElement.dir = 'ltr';
  return (
    <div className="flex h-full w-full bg-background-100 dark:bg-background-900">
      <Sidebar routes={checkModulesRoutes()} open={open} setOpen={setOpen} variant="admin" />
      {/* Navbar & Main Content */}
      <div className="h-full w-full font-dm dark:bg-navy-900">
        {/* Main Content */}
        <main
          className={`mx-2.5  flex-none transition-all dark:bg-navy-900 
              md:pr-2 xl:ml-[323px]`}
        >
          {/* Routes */}
          <div>
            <Navbar
              onOpenSidenav={() => setOpen(!open)}
              brandText={getActiveRoute(checkModulesRoutes(), pathname)}
              secondary={getActiveNavbar(checkModulesRoutes(), pathname)}
            />
            <div className="mx-auto min-h-screen p-2 !pt-[10px] md:p-2">
              {children}
            </div>
            <div className="p-3">
              <Footer />
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
