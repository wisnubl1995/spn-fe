'use client';

import Card from 'components/card';
import CardMenu from 'components/card/CardMenu';
import AutocompleteInput from 'components/fields/AutoComplete';
import OverlayLoading from 'components/loading/OverlayLoading';
import DefaultModal from 'components/modal/Default';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useRouter } from 'next/navigation';

import React, { useContext, useEffect, useState } from 'react';
import { FaFileArchive, FaSearch } from 'react-icons/fa';
import { FaCheck, FaFileExport, FaPrint } from 'react-icons/fa6';
import { MdSearchOff } from 'react-icons/md';
import exportToExcel from 'utils/exportExcel';
import { formatDate } from 'utils/formatDate';
import { buildQuery, formattedDate, getBase64 } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
  const { sessionUser } = useContext(AuthContext)
  const [activeTab, setActiveTab] = useState('Pending');
  const router = useRouter()
  const [inventoryData, setInventoryData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchLocation, setSearchLocation] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [showCloseModal, setShowCloseModal] = useState(false);
  const [currentId, setCurrentId] = useState('');
  const [showNotif, setShowNotif] = useState(false)
  const [notifMessage, setNotifMessage] = useState(null)
  const [showFilter, setShowFilter] = useState(false);
  const [filteredQuery, setFilteredQuery] = useState(null);

  let isMounted = true;
  let timeoutId: number | any;

  const handleSelectUUID = (uuid) => {
    // setSelectedUUID(uuid);
    // // Mengambil objek yang sesuai dengan UUID yang dipilih
    // const selectedData = purchaseData.find((data) => data.uuid === uuid);
    // console.log(selectedData);
    // setSelectedRow(selectedData);
    // // Tampilkan pop-up atau lakukan operasi lain sesuai kebutuhan
    // setShowDetail(true);
  };

  const fetchData = async () => {
    setIsLoading(true);
    const url = `${process.env.NEXT_PUBLIC_API}outbound?page=${currentPage}&pageSize=10&location=${searchLocation}`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }
      
      if (response.status === 200) {
        const data = await response.json();
        if (isMounted) {
          setInventoryData(data.data);
        }else{
          setInventoryData([])
        }
      }else{
        setInventoryData([])
      }
      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      setIsLoading(false);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    const debouncedFetch = debounce(fetchData, 1000); // Debounce to avoid excessive requests
    debouncedFetch();

    // Cleanup function to clear timeout on unmount
    return () => clearTimeout(timeoutId);
  }, [searchLocation]);

  const debounce = (func, delay) => {
    return (...args) => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => func.apply(this, args), delay);
    };
  };

  const onHandleCloseData = async (file: any) => {
    try {
      setIsLoading(true); 
      const payload = {
        id: currentId,
        file: file
      }

      const url = `${process.env.NEXT_PUBLIC_API}outbound/delivered`;
      const response = await fetch(url, {
        body: JSON.stringify(payload),
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }
      
      if (response.status === 200) {
        fetchData()
      }
      setIsLoading(false); 
    } catch (error) {
      setIsLoading(false); 
      console.log('Failed set status close!', error)
    }
  }

  useEffect(() => {
    isMounted = true
    fetchData()
    return () => { isMounted = false }
  }, [currentPage])

  const handleExport = () => {
    setIsLoading(true);
    const tmpData = [];
    for (let i = 0; i < inventoryData.length; i++) {
      const header = inventoryData[i];
      for (let j = 0; j < header.outboundItems.length; j++) {
        const item = header.outboundItems[j];
        tmpData.push({
          PURCHASE_REQUEST_NO: header.noPr,
          NAMA_KAPAL: header.shipName,
          KATEGORI_KAPAL: header.categoryShip,
          DEPARTEMEN: header.department,
          LOKASI: header.location,
          DELIVER_TO: header.deliverTo,
          VIA: header.via,
          NO_RESI: header.noResi,
          PENERIMA: header.recepient,
          TANGGAL_TERIMA: header.receiptDate
            ? formattedDate(new Date(header.receiptDate), 'YYYY-MM-DD')
            : header.receiptDate,
          PRODUCT_NAME: item.productName,
          SATUAN: item.unit,
          QUANTITY: item.count,
          CREATED_BY: header.createdBy ? header.createdBy : '-',
          CREATED_AT: header.createdAt
            ? formattedDate(new Date(header.createdAt), 'YYYY-MM-DD')
            : header.createdAt,
          STATUS_OUTBOUND: header.status === "Close" ? "COMPLETED" : "PENDING"
        });
      }
    }

    exportToExcel(tmpData, 'OUTBOUND', 'DATA');
    setIsLoading(false);
  };

  const onSubmitModal = async (formData: any) => {
    setFilteredQuery(formData)
    fetchFiltered(formData)
  }

  const fetchFiltered = async (queries: any) => {
    const url = buildQuery(`${process.env.NEXT_PUBLIC_API}outbound/filter`, queries);
    if (url.includes('?')) {
      setIsLoading(true);
      try {
        const response = await fetch(url, {
          headers: {
            "account-access": sessionUser?.uuid
          }
        });
  
        if (!response.ok) {
          setIsLoading(false);
          throw new Error('Network response was not ok');
        }
  
        if (response.status === 200) {
          const data = await response.json();
          if (isMounted) {
            setInventoryData(data.data);
          }else{
            setInventoryData([])
            setNotifMessage(`Failed! ${data?.message ? data?.message : 'Internal Server Error'}`)
            setShowNotif(true)
          }
        } else {
          setInventoryData([])
        }
  
        setIsLoading(false); // Update isLoading state to false
      } catch (error) {
        setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
        setShowNotif(true)
        setIsLoading(false);
        console.error('There was a problem with the fetch operation:', error);
      }
    }
  }

  const canGoPrev = currentPage < 1
  const canGoNext = inventoryData.length >= 10

  if (isLoading) {
    return <OverlayLoading />;
  }

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="text-xl font-bold text-navy-700 dark:text-white">
            Outbound
          </div>
          <div className="mr-2 flex">
            <div className="flex mr-2">
              <label className="text-sm font-bold text-navy-700 ">
                Search Location :{' '}
              </label>
              <input
                id="location"
                name="location"
                placeholder="Search by Location"
                value={searchLocation}
                onChange={(e) => {
                  setSearchLocation(e.target.value);
                }}
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
            <CardMenu
              showExportBtn={true}
              handleExport={handleExport}
              handlePopUp={() => {
                router.push('/admin/outbound/create')
              }}
            />
            <div className="flex cursor-pointer ml-2">
              <button
                onClick={() => {
                  setShowFilter(true)
                }}
                className="rounded px-3 py-1 text-white focus:outline-none bg-brand-600 hover:bg-brand-300"
              >
                <FaSearch className="inline-block h-4 w-4" /> Filter
              </button>
            </div>
          </div>
        </header>

        {filteredQuery && <div className="flex items-center my-1">
          <div className="mr-8 font-bold p-2 bg-gray-700 rounded text-white">
            FILTERED BY :
          </div>
          <div className="flex-1 flex justify-between items-center">
            {Object.keys(filteredQuery)?.filter((key) => filteredQuery[key] !== '').map((key) => (
              <div key={key}>
                <p className="font-semibold">{key.toUpperCase()} : </p>
                <p>{filteredQuery[key]}</p>
              </div>
            ))}
            <div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={handleExport}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-cyan-600 hover:bg-cyan-300"
                >
                  <FaFileExport className="inline-block h-4 w-4" /> Export
                </button>
              </div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={() => {
                    setFilteredQuery(null)
                    fetchData()
                  }}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-red-600 hover:bg-red-300"
                >
                  <MdSearchOff className="inline-block h-6 w-6" /> Clear
                </button>
              </div>
            </div>
          </div>
        </div>}

        <div className=" overflow-x-scroll ">
          <table className="w-full">
            <thead>
              <tr className="!border-px !border-gray-400">
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      PR Number
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Ship
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Category Ship
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Location
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Deliver To
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Via
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      No. Resi
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Date Requested
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Status
                    </p>
                  </div>
                </th>

                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Action
                    </p>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {inventoryData
                ? inventoryData.map((v: any, i: any) => (
                    <tr key={i}>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.noPr}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.shipName}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.categoryShip}
                        </p>
                      </td>

                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.location}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.deliverTo}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.via}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.noResi}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {formatDate(v.createdAt)}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          <span className={`p-2 rounded-md text-white ${v.status === 'Open' ? 'bg-green-600' : 'bg-yellow-600'}`}>{v.status}</span>
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3 pr-4 flex flex-col items-stretch">
                        <button
                          onClick={() => {
                            setCurrentId(v.uuid)
                            setShowCloseModal(true)
                          }}
                          disabled={v.status == 'Close' ? true : false}
                          className={`my-1 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none
                          ${
                            v.status == 'Close'
                              ? 'bg-gray-500 hover:bg-gray-600'
                              : ''
                          }
                          `}
                        >
                          <FaCheck className='inline-block' /> Set Close
                        </button>
                        <button
                          onClick={() => {
                            router.push(`/admin/outbound/details/${v.uuid}`)
                          }}
                          className={`my-1 rounded bg-cyan-500 px-4 py-2 text-white hover:bg-cyan-600 focus:outline-none
                          `}
                        >
                          <FaFileArchive className="inline-block" /> Details
                        </button>
                        <button
                          onClick={() => {
                            router.push(`/admin/outbound/preview/${v.uuid}`)
                          }}
                          className={`my-1 rounded bg-orange-500 px-4 py-2 text-white hover:bg-orange-600 focus:outline-none
                          `}
                        >
                          <FaPrint className="inline-block" /> Print
                        </button>
                      </td>
                    </tr>
                  ))
                : 'Loading'}
            </tbody>
          </table>
        </div>

        <div className="mt-6 flex items-center justify-between">
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage - 1
              setCurrentPage(page)
            }}
            disabled={!canGoPrev}
            className="rounded bg-gray-300 px-4 py-2">
            Previous
          </button>
          <span>
            Page <strong>{currentPage}</strong>
          </span>
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage + 1
              setCurrentPage(page)
            }}
            disabled={!canGoNext}
            className="rounded bg-gray-300 px-4 py-2"
          >
            Next
          </button>
        </div>

      </Card>

      {showCloseModal && (
        <ModalCloseStatus
          title={`Set Close Data Outbound`}
          onClose={() => setShowCloseModal(false)}
          onSubmit={onHandleCloseData}
        />
      )}

      {showNotif && 
        <NotificationOverlay 
            isShow={showNotif}
            title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
            message={notifMessage}
            handleClose={() => setShowNotif(false)}
            color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}

      {showFilter && (
        <ModalFilter
          title={`Filter Data Purchase Request`}
          onClose={() => setShowFilter(false)}
          onSubmit={onSubmitModal}/>)}
    </div>
  );
};

const ModalCloseStatus = ({ onClose, onSubmit, title }: any) => {
  const [file, setFile] = useState('')

  return (
    <DefaultModal title={title} onClose={onClose}>
      <div className="mt-6 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 py-2">
        <div className="my-2">
          <p className="text-lg leading-6 font-semibold mb-2">Receipt Letter</p>
          <input
              accept="image/*"
              type="file"
              name="file"
              id="file"
              onChange={async (ev) => {
                  setFile(await getBase64(ev.target.files[0]))
              }}
              placeholder="Input Additional Info"
              className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit(file);
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </DefaultModal>
  );
};

const ModalFilter = ({ onClose, onSubmit, title }: any) => {
  const [formData, setFormData] = useState({
    department: '',
    ship: '',
    startDate: '',
    endDate: '',
  })
  return (
    <DefaultModal title={title} onClose={onClose}>
      <div className="mt-6 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 py-2">
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Start Date</p>
          <input
              type="date"
              name="startDate"
              id="startDate"
              placeholder="Input Start Date"
              value={formData.startDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  startDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">End Date</p>
          <input
              type="date"
              name="endDate"
              id="endDate"
              placeholder="Input End Date"
              value={formData.endDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  endDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Department</p>
          <AutocompleteInput
              placeholder="Search Department"
              apiUrl="department/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  department: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Ship</p>
          <AutocompleteInput
              placeholder="Search Ship"
              apiUrl="ship/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  ship: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit(formData);
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </DefaultModal>
  );
};

export default Page;
