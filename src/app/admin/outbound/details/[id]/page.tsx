'use client'
import Card from 'components/card';
import AutocompleteInput from 'components/fields/AutoComplete';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation';
import React, { useContext, useEffect, useState } from 'react'
import { FaBoxOpen } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import onAddAutocomplete from 'utils/api/handleAddAutocomplete';
import { formattedDate, validateForm } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
	const router = useRouter()
	const { sessionUser } = useContext(AuthContext)
	const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const params = useParams()
    const uuid = params?.id

	const [formData, setFormData] = useState({
		noPr: "",
		shipName: "",
		categoryShip: "",
		deliverTo: "",
		via: "",
		noResi: "",
		location: "",
		department: "",
		recepient: "",
		sentDate: "",
		receiptDate: "",
		sentBy: "",
	})
	const [listItems, setListItems] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`outbound/${uuid}`, 'GET', null, headersReq)
            if (resp.data) {
                setFormData({
                    noPr: resp.data?.noPr,
                    shipName: resp.data?.shipName,
                    categoryShip: resp.data?.categoryShip,
                    deliverTo: resp.data?.deliverTo,
                    via: resp.data?.via,
                    noResi: resp.data?.noResi,
                    location: resp.data?.location,
                    department: resp.data?.department,
                    recepient: resp.data?.recepient,
                    sentDate: resp.data?.createdAt ? formattedDate(new Date(resp.data?.createdAt), "YYYY-MM-DD") : '-',
                    receiptDate: resp.data?.receiptDate ? formattedDate(new Date(resp.data?.receiptDate), "YYYY-MM-DD") : '-',
                    sentBy: resp.data?.createdBy,
                })
                setListItems(resp.data?.outboundItems)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setNotifMessage(`Failed! Fetch details Error: ${error?.message}.`)
            setShowNotif(true)
			return
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [uuid])

	if (isLoading) 
        return <OverlayLoading />

	return (
		<div className="mt-10">
			<Card extra={'w-full pb-10 p-4 h-full'}>
				<header className="relative flex items-center justify-between">
					<div className="py-5 text-xl font-bold text-navy-700 dark:text-white">
						Details Outbound
					</div>
				</header>
				<div className="mt-2 border-b border-t border-gray-900/10 pb-12">
					<div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-1 sm:grid-cols-6">
						<div className="sm:col-span-2">
                            <label htmlFor="noPr" className="block text-base font-medium leading-6 text-gray-900">
                                PR Number
                            </label>
                            <div className="mt-2">
								<input
									id="noPr"
                                    disabled
									name="noPr"
									placeholder="Purchase Request Number"
									type="text"
									value={formData?.noPr}
									onChange={(e) =>
										setFormData({
											...formData,
											noPr: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="deliverTo" className="block text-base font-medium leading-6 text-gray-900">
                                Deliver To <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
								<AutocompleteInput
                                    placeholder="Search Deliver To Location"
                                    apiUrl="location/search"
                                    disabled
                                    searchQuery='name'
									defaultValue={formData.deliverTo}
                                    keyLabel='name'
                                    keyValue='name'
                                    minTreshold={3}
                                    onSelect={async (suggestion: any) => {
                                        setFormData({
											...formData,
											deliverTo: suggestion?.name,
										})
                                        if (suggestion?.isNew) {
                                            await onAddAutocomplete('location', { 
                                                name: suggestion.name,
												email: '',
                                                address: '',
                                                telephone: '',
                                                fax: '',
                                                desc: ''
                                            }, headersReq)
                                        }
                                    }}
                                />
                                <p className="mt-1 text-sm leading-6 text-gray-600">Min 3 character for search.</p>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="via" className="block text-base font-medium leading-6 text-gray-900">
                                Via
                            </label>
                            <div className="mt-2">
								<input
									id="via"
									name="via"
                                    disabled
									placeholder="Deliver To"
									type="text"
									value={formData?.via}
									onChange={(e) =>
										setFormData({
											...formData,
											via: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="noResi" className="block text-base font-medium leading-6 text-gray-900">
                                No. Resi
                            </label>
                            <div className="mt-2">
								<input
									id="noResi"
									name="noResi"
                                    disabled
									placeholder="No. Resi"
									type="text"
									value={formData?.noResi}
									onChange={(e) =>
										setFormData({
											...formData,
											noResi: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="department" className="block text-base font-medium leading-6 text-gray-900">
                                Department
                            </label>
                            <div className="mt-2">
                                <AutocompleteInput
                                    placeholder="Search Department"
                                    apiUrl="department/search"
                                    disabled
                                    searchQuery='name'
									defaultValue={formData.department}
                                    keyLabel='name'
                                    keyValue='name'
                                    minTreshold={3}
                                    onSelect={async (suggestion: any) => {
                                        setFormData({
											...formData,
											department: suggestion?.name,
										})

                                        if (suggestion?.isNew) {
                                            await onAddAutocomplete('department', { 
                                                name: suggestion.name,
                                                desc: ''
                                            }, headersReq)
                                        }
                                    }}
                                />
                                <p className="mt-1 text-sm leading-6 text-gray-600">Min 3 character for search.</p>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="recepient" className="block text-base font-medium leading-6 text-gray-900">
                                Recepient <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
								<input
									id="recepient"
									name="recepient"
									placeholder="Recepient"
                                    disabled
									type="text"
									value={formData?.recepient}
									onChange={(e) =>
										setFormData({
											...formData,
											recepient: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="location" className="block text-base font-medium leading-6 text-gray-900">
                                Location <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
                                <AutocompleteInput
                                    placeholder="Search Location"
                                    apiUrl="location/search"
                                    searchQuery='name'
                                    disabled
									defaultValue={formData.location}
                                    keyLabel='name'
                                    keyValue='name'
                                    minTreshold={3}
                                    onSelect={async (suggestion: any) => {
                                        setFormData({
											...formData,
											location: suggestion?.name,
										})
                                        if (suggestion?.isNew) {
                                            await onAddAutocomplete('location', { 
                                                name: suggestion.name,
												email: '',
                                                address: '',
                                                telephone: '',
                                                fax: '',
                                                desc: ''
                                            }, headersReq)
                                        }
                                    }}
                                />
                                <p className="mt-1 text-sm leading-6 text-gray-600">Min 3 character for search.</p>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="ship" className="block text-base font-medium leading-6 text-gray-900">
                                Ship <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
                                <AutocompleteInput
                                    placeholder="Search Ship Name"
                                    apiUrl="ship/search"
                                    disabled
									defaultValue={formData.shipName}
                                    searchQuery='name'
                                    keyLabel='name'
                                    keyValue='name'
                                    minTreshold={3}
                                    onSelect={async (suggestion: any) => {
                                        setFormData({
											...formData,
											shipName: suggestion?.name,
											categoryShip: suggestion?.category ? suggestion?.category : '',
										})
                                        if (suggestion?.isNew) {
                                            await onAddAutocomplete('ship', { 
                                                name: suggestion.name,
												category: '',
                                                desc: '',
                                                shipCode: '',
                                                location: '',
                                                image: ''
                                            }, headersReq)
                                        }
                                    }}
                                />
                                <p className="mt-1 text-sm leading-6 text-gray-600">Min 3 character for search.</p>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="categoryShip" className="block text-base font-medium leading-6 text-gray-900">
                                Category Ship
                            </label>
                            <div className="mt-2">
								<input
									disabled
									id="categoryShip"
									name="categoryShip"
									placeholder="Category Ship"
									type="text"
									value={formData?.categoryShip}
									onChange={(e) =>
										setFormData({
											...formData,
											categoryShip: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="sentBy" className="block text-base font-medium leading-6 text-gray-900">
                                Sent By
                            </label>
                            <div className="mt-2">
								<input
									disabled
									id="sentBy"
									name="sentBy"
									placeholder="Sent By"
									type="text"
									value={formData?.sentBy}
									onChange={(e) =>
										setFormData({
											...formData,
											sentBy: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="sentDate" className="block text-base font-medium leading-6 text-gray-900">
                                Sent Date
                            </label>
                            <div className="mt-2">
								<input
									disabled
									id="sentDate"
									name="sentDate"
									placeholder="Sent Date"
									type="text"
									value={formData?.sentDate}
									onChange={(e) =>
										setFormData({
											...formData,
											sentDate: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="receiptDate" className="block text-base font-medium leading-6 text-gray-900">
                                Receipt Date
                            </label>
                            <div className="mt-2">
								<input
									disabled
									id="receiptDate"
									name="receiptDate"
									placeholder="Category Ship"
									type="text"
									value={formData?.receiptDate}
									onChange={(e) =>
										setFormData({
											...formData,
											receiptDate: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
					</div>

					<div className="mt-4 border-t">
						<h3 className="font-bold mt-3 ml-1">List Items</h3>
						<div className="my-3 flex items-center justify-between">
							<p className="text-base font-semibold border rounded bg-gray-700 text-white px-2 mx-1 flex-1">
								Product Name
							</p>
							<p className="text-base font-semibold border rounded bg-gray-700 text-white px-2 mx-1 flex-1">
								Unit
							</p>
							<p className="text-base font-semibold border rounded bg-gray-700 text-white px-2 mx-1 flex-1">
								Quantity
							</p>
						</div>
						{listItems?.map((item, idx) => (
							<div className="my-3 flex items-center justify-between" key={idx}>
								<p className="text-base font-medium flex-1 px-2">
									{item.productName}
								</p>
								<p className="text-base font-medium flex-1 px-2">
									{item.unit}
								</p>
								<p className="text-base font-medium flex-1 px-2">
									{item.count}
								</p>
							</div>
						))}
					</div>
				</div>

				<div className="mt-6 flex items-center justify-end gap-x-6">
					<button
						type="button"
						onClick={() => router.back()}
						className="text-md rounded-md bg-gray-600 px-4 py-2 text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600" >
						{' '}
						Back
					</button>
				</div>
			</Card>

			{showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
		</div>
	)
}

export default Page