'use client'
import Card from 'components/card'
import Image from 'next/image';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation'
import React, { useContext, useEffect, useState } from 'react'
import { FaBox, FaPrint } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import { formatNumber, formattedDate } from 'utils/helper';
import logo from '/public/img/logo/spn.png';
import { IoIosArrowRoundBack } from 'react-icons/io';
import { formatDate } from 'utils/formatDate';

type Props = {};

const Page = (props: Props) => {
    const router = useRouter()
    const params = useParams()
    const uuid = params?.id

    const { sessionUser } = useContext(AuthContext);
	const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const printRef = React.createRef<HTMLDivElement>();

    const [isLoading, setIsLoading] = useState(false)
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [details, setDetails] = useState(null)
	const [listItems, setListItems] = useState([])

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`outbound/${uuid}`, 'GET', null, headersReq)
            if (resp.data) {
				setDetails(resp.data)
				setListItems(resp.data.outboundItems)
            }else{
                setNotifMessage(`Failed! ${resp?.message}`)
                setShowNotif(true)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.log(`failed fetching details. error: ${error?.message}`)
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [uuid])

    const handlePrintClick = () => {
        if (!printRef.current) {
          console.error('Printing failed: Ref is not attached to a valid element.');
          return;
        }
    
        const printableContent = printRef.current.cloneNode(true) as HTMLElement;
        const originalContent = document.body.innerHTML;
    
        const otherElements = document.body.querySelectorAll('*:not(.print-container)');
        otherElements.forEach((element) => element.classList.add('hidden'));

		// Add CSS style for landscape orientation
		const printStyle = document.createElement('style');
		printStyle.innerHTML = `@page { size: landscape; }`;
		document.head.appendChild(printStyle);
    
        document.body.innerHTML = printableContent.outerHTML;
        window.print();

		// Remove landscape style after printing
		document.head.removeChild(printStyle);
    
        document.body.innerHTML = originalContent;
        otherElements.forEach((element) => element.classList.remove('hidden'));
        window.location.reload()
    };

    if (isLoading) 
        return <OverlayLoading />

    return (
        <div className="mt-10">
            <Card extra={'w-full pb-10 p-4 h-full'}>
                <header className="relative flex items-center justify-between">
                    <div className="py-2 text-xl font-bold text-navy-700 dark:text-white">
                        Print Delivery Letter
                    </div>
                    <div>
                        <button
                            onClick={() => {
                                router.push('/admin/outbound')
                            }}
                            className="mt-4 rounded bg-gray-500 px-4 py-2 text-white hover:bg-gray-600 focus:outline-none mr-2">
                            <IoIosArrowRoundBack className="w-6 h-6 inline-block" /> Cancel
                        </button>
                        <button
                            onClick={handlePrintClick}
                            className="mt-4 rounded bg-brand-500 px-4 py-2 text-white hover:bg-brand-600 focus:outline-none ">
                            <FaPrint className="inline-block" /> Print
                        </button>
                    </div>
                </header>
                <div className="my-6 border-b border-t border-gray-900/10 py-6">
                    <div className="print-container h-min-[700px] w-min-[1200px] overflow-auto rounded bg-white p-4" ref={printRef}>
                        <div className="flex w-full flex-col gap-1 px-3" id="pdfContent">
							<div className="text-center">
								<h3 className="font-semibold text-sm">DELIVERY LETTER</h3>
							</div>
                            <div className="flex w-full items-center justify-between border-b-2 pb-2">
                                <div className="w-3/12">
                                    <img src="/img/logo/spn.png" alt="Logo SPN" width={200} height={200}/>
                                </div>
                                <div className="flex w-6/12 flex-col">
                                    <p className="text-[#57B5E2] font-bold text-sm">
                                        PT. SOLUSI PELAYARAN NUSANTARA
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        Jl. Kembang Kerep No.4, RT.4/RW.2, Meruya Utara, Kec. Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11620
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        021-38777005
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        solusipelayarannusantara@gmail.com
                                    </p>
                                </div>
                            </div>

							<div className="grid grid-cols-4 gap-1">
                                <div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">LETTER NUMBER:</h3>
									</div>
									<p className="font-bold text-sm">{details?.outbound_no ? details.outbound_no : '-'}</p>
								</div>
								<div className="col-span-2">
									<div>
										<h3 className="text-[#57B5E2] text-sm">PR NO:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.noPr}</p>
									</div>
								</div>
                                <div className="col-span-1 grid grid-cols-2 gap-3">
                                    <div className="col-span-1">
                                        <div>
                                            <h3 className="text-[#57B5E2] text-sm">VESSEL:</h3>
                                        </div>
                                        <div>
                                            <p className="font-bold text-sm">{details?.shipName}</p>
                                        </div>
                                    </div>
                                    <div className="col-span-1">
                                        <div>
                                            <h3 className="text-[#57B5E2] text-sm">CATEGORY VESSEL:</h3>
                                        </div>
                                        <div>
                                            <p className="font-bold text-sm">{details?.categoryShip}</p>
                                        </div>
                                    </div>
                                </div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">DEPARTMENT:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.department ? details?.department : '-'}</p>
									</div>
								</div>
                                <div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">LOCATION:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.location}</p>
									</div>
								</div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">DELIVER TO:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.deliverTo}</p>
									</div>
								</div>
                                <div className="col-span-1 grid grid-cols-2 gap-3">
                                    <div className="col-span-1">
                                        <div>
                                            <h3 className="text-[#57B5E2] text-sm">VIA:</h3>
                                        </div>
                                        <div>
                                            <p className="font-bold text-sm">{details?.via ? details?.via : '...'}</p>
                                        </div>
                                    </div>
                                    <div className="col-span-1">
                                        <div>
                                            <h3 className="text-[#57B5E2] text-sm">NO. RESI:</h3>
                                        </div>
                                        <div>
                                            <p className="font-bold text-sm">{details?.noResi ? details?.noResi : '...'}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">CREATED DATE:</h3>
									</div>
									<p className="font-bold text-sm">{details?.createdAt ? formattedDate(new Date(details.createdAt), 'YYYY-MM-DD') : '-'}</p>
								</div>
                                <div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">RECEIPT DATE:</h3>
									</div>
									<p className="font-bold text-sm">{details?.receiptDate ? formattedDate(new Date(details.receiptDate), 'YYYY-MM-DD') : '-'}</p>
								</div>
                                <div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">RECEPIENT:</h3>
									</div>
									<p className="font-bold text-sm">{details?.recepient ? details.recepient : '-'}</p>
								</div>
							</div>

                            <div className={`mt-4 py-1 grid grid-cols-1 gap-2`}>
                                <div className={`col-span-1`}>
									<div className="text-center py-2 bg-gray-400 rounded font-bold text-sm border border-gray-400">List Items</div>
									<div className={`col-span-1 grid grid-cols-3 py-2 gap-2`}>
										<div className="p-2 bg-gray-400 rounded font-bold text-sm border border-gray-400">Product Name</div>
										<div className="p-2 bg-gray-400 rounded font-bold text-sm border border-gray-400">Unit</div>
										<div className="p-2 bg-gray-400 rounded font-bold text-sm border border-gray-400">Quantity</div>
									</div>
									{listItems?.map(item => (
										<div className={`col-span-1 grid gap-2 grid-cols-3 my-1`} key={item.productName}>
											<div className="text-sm p-2 rounded">{item.productName}</div>
											<div className="text-sm p-2 rounded">{item.unit}</div>
											<div className="text-sm p-2 rounded">{item.count}</div>
										</div>
									))}
								</div>
                            </div>

                            <div className="my-2 flex flex-col">
                                <div>
									<p className="text-base font-semibold">{details?.location}, {details?.createdAt ? formatDate(details.createdAt).replace(',', '') : '-'}</p>
									<p className="mt-1 text-sm">NOTE:</p>
									<p className="mt-1 text-sm italic">MOHON DI TTD + STEMPEL, SCAN DAN KIRIM KE PROCUREMENT</p>
								</div>
                            </div>

                            <div className="mt-3 grid grid-cols-3 gap-2 items-center">
                                <div className="text-center border">
									<p className="text-base font-bold border-b">SENDER</p>
									<p className="mt-20 text-base font-semibold">...</p>
								</div>
                                <div className="text-center border">
									<p className="text-base font-bold border-b">VIA</p>
									<p className="mt-20 text-base font-semibold">{details?.via ? details.via : '-'}</p>
								</div>
                                <div className="text-center border">
									<p className="text-base font-bold border-b">RECEPIENT</p>
									<p className="mt-20 text-base font-semibold">{details?.recepient ? details.recepient : '...'}</p>
								</div>
                            </div>

                        </div>
                    </div>
                </div>
            </Card>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

export default Page