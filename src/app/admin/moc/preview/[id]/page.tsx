'use client'
import Card from 'components/card'
import Image from 'next/image';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation'
import React, { useContext, useEffect, useState } from 'react'
import { FaBox, FaPrint } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import { formatNumber, formattedDate } from 'utils/helper';
import logo from '/public/img/logo/spn.png';
import { IoIosArrowRoundBack } from 'react-icons/io';
import CustomTooltip from 'components/tooltip/CustomTooltip';

type Props = {};

function reformatJson(data) {
	const stores = {};
	for (const item of data) {
	  const itemId = item.itemId;
	  for (const store of item.stores) {
		const shopName = store.shopName;
		if (!stores[shopName]) {
		  stores[shopName] = { items: [] };
		}
		const storeItem = { ...item, ...store };
		stores[shopName].items.push(storeItem);
	  }
	}
	return stores;
}

function orderByNumberAsc(array, prop) {
	return array.sort((a, b) => a[prop] - b[prop]);
}

const Page = (props: Props) => {
    const router = useRouter()
    const params = useParams()
    const uuid = params?.id

    const { sessionUser } = useContext(AuthContext);
	const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const printRef = React.createRef<HTMLDivElement>();

    const [isLoading, setIsLoading] = useState(false)
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [details, setDetails] = useState(null)
    const [subTotal, setSubtotal] = useState(0)
    const [totalTax, setTotalTax] = useState<number>(0)
    const [totalAmount, setTotalAmount] = useState<number>(0)
	const [listItems, setListItems] = useState([])
	const [listStores, setListStores] = useState([])
	const [preparedBy, setPreparedBy] = useState('')
	const [approvedBy, setApprovedBy] = useState('')

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`matrixofcomparison/${uuid}`, 'GET', null, headersReq)
            if (resp.data) {
				setDetails(resp.data)
				const items = []
				const stores = []
				if (resp.data.mocItems[0]) {
					setPreparedBy(resp.data.mocItems[0]?.createdBy)
					setApprovedBy(resp.data.mocItems[0]?.approvedL2By)
				}
				for (let i = 0; i < resp.data.mocItems.length; i++) {
					const item = resp.data.mocItems[i];
					const selectedStores = item.storeDetails.filter(str => str.isSelected)
					for (let j = 0; j < selectedStores.length; j++) {
						const store = selectedStores[j];
						const storeIdx = stores.findIndex(str => str.shopName === store.shopName)
						if (storeIdx === -1) {
							stores.push({
								shopName: store.shopName,
							})	
						}
					}

					items.push({
						itemId: item.uuid,
						description: item.description,
						unit: item.unit,
						quantity: item.quantity,
						stores: selectedStores
					})
				}
				setListItems(orderByNumberAsc(items, 'itemId'))
				setListStores(stores)

                let sub = 0
                let tax = 0
				if (resp.data?.approvedPoItems) {
					for (let i = 0; i < resp.data.approvedPoItems.length; i++) {
						const item = resp.data.approvedPoItems[i];
						sub += parseInt(item.amount)
						tax += item.tax ? parseInt(item.tax) : 0
					}
				}

                setSubtotal(sub)
                setTotalTax(tax)
                setTotalAmount(sub + tax)
            }else{
                setNotifMessage(`Failed! ${resp?.message}`)
                setShowNotif(true)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.log(`failed fetching details. error: ${error?.message}`)
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [uuid])

	const getItemsByStores = (shopName: string) => {
		const formattedData = reformatJson(listItems);
		for (let i = 0; i < listStores.length; i++) {
			const store = listStores[i];
			const storesItem = formattedData[store.shopName].items
			for (let j = 0; j < listItems.length; j++) {
				const item = listItems[j];
				const idxNoItem = storesItem.findIndex(it => it.itemId === item.itemId)
				if (idxNoItem === -1) {
					storesItem.push({itemId: item.itemId, unit: '-', description: '-', quantity: '-'})
				}
			}
			formattedData[store.shopName].items = orderByNumberAsc(storesItem, 'itemId')
		}

		return formattedData[shopName]
	}

    const handlePrintClick = () => {
        if (!printRef.current) {
          console.error('Printing failed: Ref is not attached to a valid element.');
          return;
        }
    
        const printableContent = printRef.current.cloneNode(true) as HTMLElement;
        const originalContent = document.body.innerHTML;
    
        const otherElements = document.body.querySelectorAll('*:not(.print-container)');
        otherElements.forEach((element) => element.classList.add('hidden'));

		// Add CSS style for landscape orientation
		const printStyle = document.createElement('style');
		printStyle.innerHTML = `@page { size: landscape; }`;
		document.head.appendChild(printStyle);
    
        document.body.innerHTML = printableContent.outerHTML;
        window.print();

		// Remove landscape style after printing
		document.head.removeChild(printStyle);
    
        document.body.innerHTML = originalContent;
        otherElements.forEach((element) => element.classList.remove('hidden'));
        window.location.reload()
    };

    if (isLoading) 
        return <OverlayLoading />

    return (
        <div className="mt-10">
            <Card extra={'w-full pb-10 p-4 h-full'}>
                <header className="relative flex items-center justify-between">
                    <div className="py-2 text-xl font-bold text-navy-700 dark:text-white">
                        Print Matrix of Comparison
                    </div>
                    <div>
                        <button
                            onClick={() => {
                                router.push('/admin/moc')
                            }}
                            className="mt-4 rounded bg-gray-500 px-4 py-2 text-white hover:bg-gray-600 focus:outline-none mr-2">
                            <IoIosArrowRoundBack className="w-6 h-6 inline-block" /> Cancel
                        </button>
                        <button
                            onClick={handlePrintClick}
                            className="mt-4 rounded bg-brand-500 px-4 py-2 text-white hover:bg-brand-600 focus:outline-none ">
                            <FaPrint className="inline-block" /> Print
                        </button>
                    </div>
                </header>
                <div className="my-6 border-b border-t border-gray-900/10 py-6">
                    <div className="print-container h-min-[700px] w-min-[1200px] overflow-auto rounded bg-white p-4" ref={printRef}>
                        <div className="flex w-full flex-col gap-1 px-3" id="pdfContent">
							<div className="text-center">
								<h3 className="font-semibold text-sm">Matrix Of Comparison For Material and Services</h3>
							</div>
                            <div className="flex w-full items-center justify-between border-b-2 pb-2">
                                <div className="w-3/12">
                                    <img src="/img/logo/spn.png" alt="Logo SPN" width={200} height={200}/>
                                </div>
                                <div className="flex w-6/12 flex-col">
                                    <p className="text-[#57B5E2] font-bold text-sm">
                                        PT. SOLUSI PELAYARAN NUSANTARA
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        Jl. Kembang Kerep No.4, RT.4/RW.2, Meruya Utara, Kec. Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11620
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        021-38777005
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        solusipelayarannusantara@gmail.com
                                    </p>
                                </div>
                            </div>

							<div className="grid grid-cols-4 gap-3">
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">DATE:</h3>
									</div>
									<p className="font-bold text-sm">{details?.createdAt ? formattedDate(new Date(details.createdAt), 'YYYY-MM-DD') : '-'}</p>
								</div>
								<div className="col-span-2">
									<div>
										<h3 className="text-[#57B5E2] text-sm">PR NO:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.purchaseRequestNo}</p>
									</div>
								</div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">VESSEL:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.shipName}</p>
									</div>
								</div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">DEPARTMENT:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.department}</p>
									</div>
								</div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">CATEGORY:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.category}</p>
									</div>
								</div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">LOCATION:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.location}</p>
									</div>
								</div>
								<div className="col-span-1">
									<div>
										<h3 className="text-[#57B5E2] text-sm">URGENCY:</h3>
									</div>
									<div>
										<p className="font-bold text-sm">{details?.urgency ? details?.urgency : '...'}</p>
									</div>
								</div>
							</div>

                            <div className={`mt-4 border-t-2 py-2 grid grid-cols-${listStores.length + 1} gap-2`}>
                                <div className={`col-span-1`}>
									<div className="text-center py-2 bg-gray-200 rounded border font-bold text-sm">List Items</div>
									<div className={`col-span-1 grid grid-cols-3 py-2 gap-2`}>
										<div className="p-2 bg-gray-200 rounded border font-bold text-sm overflow-hidden text-ellipsis whitespace-nowrap">Description</div>
										<div className="p-2 bg-gray-200 rounded border font-bold text-sm">Unit</div>
										<div className="p-2 bg-gray-200 rounded border font-bold text-sm overflow-hidden text-ellipsis whitespace-nowrap">Qty. Asked</div>
									</div>
									{listItems?.map(item => (
										<div className={`col-span-1 grid grid-cols-3 my-1`} key={item.description}>
											<CustomTooltip text={item.description}>
												<div className="text-sm p-2">{item.description}</div>
											</CustomTooltip>
											<div className="text-sm p-2">{item.unit}</div>
											<div className="text-sm p-2">{item.quantity}</div>
										</div>
									))}
								</div>
								{listStores?.map(store => (
									<div className="col-span-1" key={store.shopName}>
										<div className="text-center p-2 bg-gray-200 border rounded font-bold text-sm">{store.shopName}</div>
										<div className="grid grid-cols-3 py-2 gap-2">
											<div className="text-sm p-2 bg-gray-200 border rounded font-bold">Qty</div>
											<div className="text-sm p-2 bg-gray-200 border rounded font-bold">Price</div>
											<div className="text-sm p-2 bg-gray-200 border rounded font-bold">Total</div>
										</div>

										{getItemsByStores(store.shopName).items?.map(item => (
											<div className="grid grid-cols-3 my-1" key={item.description}>
												<div className="text-sm p-2">{item.description === '-' ? '-' : item.quantity}</div>
												<div className="text-sm p-2">{item.description === '-' ? '-' : `Rp${formatNumber(parseInt(item.unitPrice))}`}</div>
												<div className="text-sm p-2">{item.description === '-' ? '-' : `Rp${formatNumber(parseInt(item.totalPrice))}`}</div>
											</div>
										))}
									</div>
								))}
                            </div>

                            <div className="my-2 flex justify-around items-center">
                                <div className="text-center">
									<p className="text-base font-bold text-gray-600">Prepared By:</p>
									<p className="mt-20 text-base font-semibold">{details?.createdBy ? details?.createdBy : preparedBy}</p>
								</div>
                                <div className="text-center">
									<p className="text-base font-bold text-gray-600">Approved By:</p>
									<p className="mt-20 text-base font-semibold">{details?.approvedBy ? details?.approvedBy : approvedBy}</p>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </Card>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

export default Page