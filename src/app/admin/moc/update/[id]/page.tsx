'use client'
import Card from 'components/card'
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import CustomTooltip from 'components/tooltip/CustomTooltip';
import AuthContext from 'contexts/AuthContext';
import { useRouter, useParams } from 'next/navigation'
import React, { useContext, useEffect, useState } from 'react'
import { sendRequestApi } from 'utils/api/fetchData';
import { formatDate } from 'utils/formatDate';

type Props = {};

const dataToko = [
    {
        label: 'Toko 1',
        keyTokoName: 'shopOneName',
        keyUnitName: 'shopOneUnitPrice',
        keyTotalPrice: 'shopOneTotalPrice',
        keyQty: 'shopOneQuantity',
        keyTax: 'shopOneTax',
        keyRemarks: 'shopOneRemarks',
      },
      {
        label: 'Toko 2',
        keyTokoName: 'shopTwoName',
        keyUnitName: 'shopTwoUnitPrice',
        keyTotalPrice: 'shopTwoTotalPrice',
        keyQty: 'shopTwoQuantity',
        keyTax: 'shopTwoTax',
        keyRemarks: 'shopTwoRemarks',
      },
      {
        label: 'Toko 3',
        keyTokoName: 'shopThreeName',
        keyUnitName: 'shopThreeUnitPrice',
        keyTotalPrice: 'shopThreeTotalPrice',
        keyQty: 'shopThreeQuantity',
        keyTax: 'shopThreeTax',
        keyRemarks: 'shopThreeRemarks',
      },
    {
        label: 'Toko 4',
        keyTokoName: 'shopFourName',
        keyUnitName: 'shopFourUnitPrice',
        keyTotalPrice: 'shopFourTotalPrice',
        keyQty: 'shopFourQuantity',
        keyTax: 'shopFourTax',
        keyRemarks: 'shopFourRemarks',
      },
      {
        label: 'Toko 5',
        keyTokoName: 'shopFiveName',
        keyUnitName: 'shopFiveUnitPrice',
        keyTotalPrice: 'shopFiveTotalPrice',
        keyQty: 'shopFiveQuantity',
        keyTax: 'shopFiveTax',
        keyRemarks: 'shopFiveRemarks',
      },
]

const Page = (props: Props) => {
    const router = useRouter()
    const params = useParams()
    const { sessionUser } = useContext(AuthContext);
    const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const [isLoading, setIsLoading] = useState(false)
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [dataDetails, setDataDetails] = useState(null)
    const [choosedItems, setChoosedItems] = useState([]);

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`matrixofcomparison/${params.id}`, 'GET', null, headersReq)
            if (resp.data) {
                setDataDetails(resp.data)
                setChoosedItems(resp.data?.mocItems.map(item => ({
                    id: item.uuid,
                    storeDetails: item.storeDetails.filter(store => store.shopName).map(store => ({
                        idStore: store.id,
                        name: store.shopName,
                        unitPrice: store.unitPrice,
                        quantity: store.quantity,
                        totalPrice: store.totalPrice,
                        tax: store.tax,
                        remarks: store.remarks,
                    }))
                })))
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setNotifMessage(`Failed fetching details data!`)
            setShowNotif(true)
            console.log(`failed fetching details`)
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [params.id])

    const handleChangeInput = (event: any, itemId: any, indexStore: any) => {
        const newData = [...choosedItems]; // Create a copy to avoid mutation
        const index = newData.findIndex((item) => item.id === itemId);
        if (index !== -1) {
          newData[index].storeDetails[indexStore][event.target.name] =
            event.target.value;
    
          const oriItem = dataDetails?.mocItems[index];
          if (event.target.name.includes('quantity')) {
            const totalPrice =
              parseInt(newData[index].storeDetails[indexStore].unitPrice) *
              parseInt(event.target.value);
    
            const keyTotalPrice = event.target.getAttribute('data-keytotal');
            newData[index].storeDetails[indexStore][keyTotalPrice] = `${totalPrice}`;
          }
          
          if (event.target.name.includes('unitPrice')) {
            const totalPrice =
              parseInt(newData[index].storeDetails[indexStore].quantity) *
              parseInt(event.target.value);
    
            const keyTotalPrice = event.target.getAttribute('data-keytotal');
            newData[index].storeDetails[indexStore][keyTotalPrice] = `${totalPrice}`;
          }
    
          if (event.target.name.includes('quantity')) {
            if (
              parseInt(event.target.value) > parseInt(oriItem.quantity) ||
              parseInt(event.target.value) < 0
            ) {
                newData[index].storeDetails[indexStore][event.target.name] = parseInt(oriItem.quantity);

                const totalPrice =
                parseInt(newData[index].storeDetails[indexStore].unitPrice) *
                parseInt(oriItem.quantity);

                const keyTotalPrice = event.target.getAttribute('data-keytotal');
                newData[index].storeDetails[indexStore][keyTotalPrice] = `${totalPrice}`;
            }
          }
    
          setChoosedItems(newData);
          setNotifMessage('');
        }
    };

    const handleSubmit = async () => {
        setIsLoading(true)
        try {
            const payload = { dataMocItems: choosedItems }
            const resp = await sendRequestApi(`mocitems/update`, 'PATCH', JSON.stringify(payload), headersReq)
            setNotifMessage(`${resp?.message}. Will be redirect to MOC page in 3 seconds`)
            setShowNotif(true)
            setIsLoading(false)
            setTimeout(() => {
                router.back()
            }, 3000);
        } catch (error) {
            setIsLoading(false)
            setNotifMessage(`Failed submit data!`)
            setShowNotif(true)
            console.log(`failed submit details`)
        }
    }

    if (isLoading) 
        return <OverlayLoading />

    return (
        <div className="mt-10">
            <Card extra={'w-full pb-10 p-4 h-full'}>
                <header className="relative flex items-center justify-between">
                    <div className="pt-5 text-xl font-bold text-navy-700 dark:text-white">
                        MOC - Update Items Stores
                    </div>
                </header>
                <div className="mt-6 border-b border-t border-gray-900/10 pb-12">
                    <div className='border rounded my-6 p-6'>
                        <div className="px-4 sm:px-0">
                            <h3 className="text-base font-semibold leading-7 text-gray-900">
                                Purchase Request Information {`[No. ${dataDetails ? dataDetails.purchaseRequestNo : '-'}]`}
                            </h3>
                            <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Details Purchase Request.</p>
                        </div>
                        <div className="mt-6 border-t border-gray-100">
                            <dl className="grid grid-cols-1 gap-x-6 gap-y-5 sm:grid-cols-3 sm:gap-y-6 lg:gap-x-8">
                                <div className="border-t border-gray-200 pt-4">
                                    <dt className="font-medium text-gray-900">Department</dt>
                                    <dd className="mt-2 text-sm text-gray-700">{dataDetails ? dataDetails.department : '-'}</dd>
                                </div>
                                <div className="border-t border-gray-200 pt-4">
                                    <dt className="font-medium text-gray-900">Category</dt>
                                    <dd className="mt-2 text-sm text-gray-700">{dataDetails ? dataDetails.category : '-'}</dd>
                                </div>
                                <div className="border-t border-gray-200 pt-4">
                                    <dt className="font-medium text-gray-900">Location</dt>
                                    <dd className="mt-2 text-sm text-gray-700">{dataDetails ? dataDetails.location : '-'}</dd>
                                </div>
                                <div className="border-t border-gray-200 pt-4">
                                    <dt className="font-medium text-gray-900">Ship Name</dt>
                                    <dd className="mt-2 text-sm text-gray-700">{dataDetails ? dataDetails.shipName : '-'}</dd>
                                </div>
                                <div className="border-t border-gray-200 pt-4">
                                    <dt className="font-medium text-gray-900">Total Items</dt>
                                    <dd className="mt-2 text-sm text-gray-700">{dataDetails ? dataDetails.mocItems?.length : '-'}</dd>
                                </div>
                                <div className="border-t border-gray-200 pt-4">
                                    <dt className="font-medium text-gray-900">Created At</dt>
                                    <dd className="mt-2 text-sm text-gray-700">{dataDetails ? formatDate(dataDetails.createdAt) : '-'}</dd>
                                </div>
                            </dl>
                        </div>
                    </div>
                    <div className="mt-4 flex flex-row">
                        <div className='overflow-auto border rounded pl-4 min-w-[500px]'>
                            <div className='min-w-[600px]'>
                                <label className="block text-base font-semibold leading-6 text-gray-900 mt-4 bg-gray-400 p-1 rounded mr-1">
                                    List Items
                                </label>
                                <div className="grid grid-cols-3 my-2 mr-1">
                                    <label className='font-semibold bg-gray-400 mr-2 p-1 rounded'>Description</label>
                                    <label className='font-semibold bg-gray-400 mr-2 p-1 rounded'>Unit</label>
                                    <label className='font-semibold bg-gray-400 p-1 rounded'>Qty</label>
                                </div>
                                {dataDetails?.mocItems.map((item, index) => (
                                    <div key={`ori-item-${index}`} className="grid grid-cols-3 my-2 mr-1">
                                        <div className='mx-1'>
                                            <CustomTooltip text={item.description}>
                                                <p className=' py-1 overflow-hidden whitespace-nowrap text-ellipsis'>{item.description}</p>
                                            </CustomTooltip>
                                        </div>
                                        <div className='mx-1'>
                                            <p className=' py-1'>{item.unit}</p>
                                        </div>
                                        <div className='mx-1'>
                                            <p className=' py-1'>{item.quantity}</p>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className='overflow-auto flex flex-row items-center ml-4'>
                            {dataToko.filter((_, idx) => (dataDetails?.mocItems?.[0].storeDetails?.[idx]?.shopName)).map((toko, index) => (
                                <div key={`toko-${index}`} className='text-center min-w-[760px] mr-4 border-x px-2'>
                                    <div className="flex flex-row items-center justify-center">
                                        <label className='font-semibold border rounded mx-1 bg-gray-400 flex-1'>
                                            {dataDetails?.mocItems?.[0].storeDetails?.[index].shopName}
                                        </label>
                                    </div>
                                    <div className="grid grid-cols-5 my-3">
                                        <label className='font-semibold border rounded mx-1 bg-gray-400'>Unit Price</label>
                                        <label className='font-semibold border rounded mx-1 bg-gray-400'>Qty</label>
                                        <label className='font-semibold border rounded mx-1 bg-gray-400'>Total Price</label>
                                        <label className='font-semibold border rounded mx-1 bg-gray-400'>Tax (%)</label>
                                        <label className='font-semibold border rounded mx-1 bg-gray-400'>Remarks</label>
                                    </div>
                                    {choosedItems?.map((item:any, i:number) => (
                                        <div key={`toko-${index}-item-${i}`} className="grid grid-cols-5 my-2">
                                            <div className='mx-1'>
                                                <input
                                                    placeholder='Unit' 
                                                    data-keytotal={'totalPrice'}
                                                    id={`toko-${index}-item-unit-${i}`}
                                                    name={'unitPrice'}
                                                    value={item.storeDetails[index].unitPrice}
                                                    onChange={(e) => {
                                                        handleChangeInput(e, item.id, index);
                                                    }}
                                                    className='block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none' type="text" />
                                            </div>
                                            <div className='mx-1'>
                                                <input
                                                    placeholder='Quantity'
                                                    id={`toko-${index}-item-total-${i}`}
                                                    data-keytotal={'totalPrice'}
                                                    name={'quantity'}
                                                    value={item.storeDetails[index].quantity}
                                                    onChange={(e) => {
                                                        handleChangeInput(e, item.id, index);
                                                    }}
                                                    className='block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none' type="text" />
                                            </div>
                                            <div className='mx-1'>
                                                <input
                                                    placeholder='Total'
                                                    disabled
                                                    id={`toko-${index}-item-total-${i}`}
                                                    name={'totalPrice'}
                                                    value={item.storeDetails[index].totalPrice ? item.storeDetails[index].totalPrice : 0}
                                                    onChange={(e) => {
                                                        handleChangeInput(e, item.id, index);
                                                    }}
                                                    className='block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none' type="text" />
                                            </div>
                                            <div className='mx-1'>
                                                <input
                                                    placeholder='Tax (%)'
                                                    id={`toko-${index}-item-total-${i}`}
                                                    name={'tax'}
                                                    value={item.storeDetails[index].tax}
                                                    onChange={(e) => {
                                                        handleChangeInput(e, item.id, index);
                                                    }}
                                                    className='block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none' type="text" />
                                            </div>
                                            <div className='mx-1 flex flex-row items-center'>
                                                <input
                                                    placeholder='Remarks' 
                                                    id={`toko-${index}-item-remarks-${i}`}
                                                    name={'remarks'}
                                                    value={item.storeDetails[index].remarks}
                                                    onChange={(e) => {
                                                        handleChangeInput(e, item.id, index);
                                                    }}
                                                    className='block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none' type="text" />
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <div className="mt-6 flex items-center justify-end gap-x-6">
                    <button
                        type="button"
                        onClick={() => router.back()}
                        className="rounded-md bg-gray-600 px-4 py-2 text-md text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"> Cancel
                    </button>
                    <button
                        type="button"
                        onClick={handleSubmit}
                        className="rounded-md bg-green-600 px-4 py-2 text-md text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"> Submit
                    </button>
                </div>
            </Card>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

export default Page