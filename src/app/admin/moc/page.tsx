'use client';

import DeliveryTable from 'components/admin/data-tables/DeliveryTable';
import PurchaseRequestTable from 'components/admin/data-tables/PurchaseRequestTable';
import MocTable from 'components/admin/data-tables/MocTable';
import React, { useContext, useEffect, useState } from 'react';
import { deliveryData } from 'variables/data-tables/deliveryData';
import Card from 'components/card';
import CardMenu from 'components/card/CardMenu';
import { formatDate } from 'utils/formatDate';
import { MdCreate, MdOutlineDeleteForever, MdSearchOff } from 'react-icons/md';
import Image from 'next/image';
import OverlayLoading from 'components/loading/OverlayLoading';
import DefaultModal from 'components/modal/Default';
import { useRouter } from 'next/navigation';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import { FaBox, FaFileExport, FaListCheck, FaPrint } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import { buildQuery, formatNumber, formattedDate } from 'utils/helper';
import Papa from 'papaparse';
import exportToExcel from 'utils/exportExcel';
import AuthContext from 'contexts/AuthContext';
import { FaSearch } from 'react-icons/fa';
import AutocompleteInput from 'components/fields/AutoComplete';
import CustomTooltip from 'components/tooltip/CustomTooltip';

type Props = {};

const Page = (props: Props) => {
  const router = useRouter();
  const { userLevel, sessionUser } = useContext(AuthContext)
  const headersReq = {
		"account-access": sessionUser?.uuid
	}
  const [purchaseRequest, setPurchaseRequest] = useState(() => {
    const savedData = localStorage.getItem('purchaseRequest');
    return savedData
      ? JSON.parse(savedData)
      : [
          {
            uuid: '11122223222',
            ship_name: 'Berlian',
            department: 'Operational',
            category: 'Besi',
            purchase_request_no: 'PR-10002-10001',
            requested_by: 'Wisnu',
            location: 'Jakarta',
            created_at: '21/3/2024',
            mocItems: [
              {
                uuid: '112232442',
                purchaseRequestId: '11122223222',
                name: 'Besi Baja',
                unit: 'Kg',
                quantityAsked: '10',
                quantityLeft: '0',
                quantityApproved: '0',
                sapCode: 'sapCode',
                description: 'Deskkripsi',
                additionalInfo: 'Additional Info',
                image: 'Image',
                status: 'Pending',
              },
              {
                uuid: '112232443',
                purchaseRequestId: '11122223222',
                name: 'Besi Baja',
                unit: 'Kg',
                quantityAsked: '10',
                quantityLeft: '0',
                quantityApproved: '0',
                sapCode: 'sapCode',
                description: 'Deskkripsi',
                additionalInfo: 'Additional Info',
                image: 'Image',
                status: 'Pending',
              },
            ],
          },
        ];
  });
  const [items, setItems] = useState([{ id: 1 }]);
  const [activeTab, setActiveTab] = useState('Pending');
  const [addNew, setAddNew] = useState(false);
  const [selectedUUID, setSelectedUUID] = useState('');
  const [mocData, setMocData] = useState<any>();
  const [isLoading, setIsLoading] = useState(true);
  const [selectedRow, setSelectedRow] = useState(null);
  const [isChecked, setIsChecked] = useState(false);
  const [showDetail, setShowDetail] = useState(false);
  const [showComparisonPopup, setShowComparisonPopup] = useState(false);
  const [showModalMovePO, setShowModalMovePO] = useState(false);
  const [showTokoPopup, setShowTokoPopup] = useState(false);
  const [selectedMocRow, setSelectedMocRow] = useState(null);
  const [detailsData, setDetailsData] = useState(null);
  const [notifFailed, setNotifFailed] = useState(false);
  const [notifSuccess, setNotifSuccess] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [showFilter, setShowFilter] = useState(false);
  const [filteredQuery, setFilteredQuery] = useState(null);
  
  let isMounted = true;

  const handleSaveClick = async () => {
    try {
      const url = `${process.env.NEXT_PUBLIC_API}mocitems/update`;

      const payload = {
        dataMocItems: [
          {
            id: selectedMocRow.uuid,
            shopOneName: selectedMocRow.shopOneName,
            shopOneUnitPrice: selectedMocRow.shopOneUnitPrice,
            shopOneTotalPrice:
              selectedMocRow.quantity * selectedMocRow.shopOneUnitPrice,
            shopOneRemarks: selectedMocRow.shopOneRemarks,
            shopTwoName: selectedMocRow.shopTwoName,
            shopTwoUnitPrice: selectedMocRow.shopTwoUnitPrice,
            shopTwoTotalPrice:
              selectedMocRow.shopTwoUnitPrice * selectedMocRow.quantity,
            shopTwoRemarks: selectedMocRow.shopTwoRemarks,
            shopThreeRemarks: selectedMocRow.shopThreeRemarks,
            shopThreeName: selectedMocRow.shopThreeName,
            shopThreeUnitPrice: selectedMocRow.shopThreeUnitPrice,
            shopThreeTotalPrice:
              null ||
              selectedMocRow.shopThreeUnitPrice * selectedMocRow.quantity,
            l1Status: true,
          },
        ],
      };

      const response = await fetch(url, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          "account-access": sessionUser?.uuid
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error('Failed to save data');
      }

      // Logika tambahan jika pengiriman berhasil
      console.log('Data saved successfully!');
      window.location.reload();
    } catch (error) {
      console.error('Error saving data:', error);
      // Logika penanganan error jika diperlukan
    }
  };

  const handleApproveClick = async () => {
    try {
      const url = `${process.env.NEXT_PUBLIC_API}mocitems/update`;

      const payload = {
        dataMocItems: [
          {
            id: selectedMocRow.uuid,
            shopOneName: selectedMocRow.shopOneName,
            shopOneUnitPrice: selectedMocRow.shopOneUnitPrice,
            shopOneTotalPrice:
              selectedMocRow.quantity * selectedMocRow.shopOneUnitPrice,
            shopOneRemarks: selectedMocRow.shopOneRemarks,
            shopTwoName: selectedMocRow.shopTwoName,
            shopTwoUnitPrice: selectedMocRow.shopTwoUnitPrice,
            shopTwoTotalPrice:
              selectedMocRow.shopTwoUnitPrice * selectedMocRow.quantity,
            shopTwoRemarks: selectedMocRow.shopTwoRemarks,
            shopThreeRemarks: selectedMocRow.shopThreeRemarks,
            shopThreeName: selectedMocRow.shopThreeName,
            shopThreeUnitPrice: selectedMocRow.shopThreeUnitPrice,
            shopThreeTotalPrice:
              null ||
              selectedMocRow.shopThreeUnitPrice * selectedMocRow.quantity,
            l1Status: true,
            l2Status: true,
          },
        ],
      };

      const response = await fetch(url, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          "account-access": sessionUser?.uuid
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error('Failed to save data');
      }

      // Logika tambahan jika pengiriman berhasil
      console.log('Data saved successfully!');
      window.location.reload();
    } catch (error) {
      console.error('Error saving data:', error);
      // Logika penanganan error jika diperlukan
    }
  };

  const moveToPO = async (idMoc, detailMocItems) => {
    const url = `${process.env.NEXT_PUBLIC_API}mocitems/checktomove`;

    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "account-access": sessionUser?.uuid
        },
        body: JSON.stringify({
          idMoc: idMoc,
          detailMocItems: detailMocItems.map((v, index) => ({
            id: v.uuid,
          })),
        }),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      console.log('Move to PO response:', data);
      // Lakukan operasi lain sesuai kebutuhan, seperti menangani respons dari server
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
      // Tangani kesalahan jika terjadi saat melakukan fetch
    }
  };

  const handleAddComparisonClick = (row) => {
    setSelectedMocRow(row);
    setShowComparisonPopup(true);
  };

  const handleCheckComparisonClick = (row) => {
    setSelectedMocRow(row);
    setShowTokoPopup(true);
  };

  const handleSelectUUID = (uuid) => {
    setSelectedUUID(uuid);
    // Mengambil objek yang sesuai dengan UUID yang dipilih
    const selectedData = mocData.find((data) => data.uuid === uuid);
    console.log(selectedData);
    setSelectedRow(selectedData);
    // Tampilkan pop-up atau lakukan operasi lain sesuai kebutuhan
    setShowDetail(true);
  };

  const addItem = () => {
    const newItem = { id: items.length + 1 }; // Create a new item object
    setItems([...items, newItem]); // Add the new item to the items array
  };

  const removeItem = (idToRemove) => {
    const updatedItems = items.filter((item) => item.id !== idToRemove);
    setItems(updatedItems);
  };

  const fetchData = async () => {
    const url = `${process.env.NEXT_PUBLIC_API}matrixofcomparison?page=${currentPage}&pageSize=10`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });
      if (!response.ok) {
        setNotifFailed(true);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        setMocData(data.data);
      }else{
        setMocData([])
      }

      setNotifFailed(false);
      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      setIsLoading(false);
      setNotifFailed(true);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [currentPage]);

  const onSubmitToPO = async () => {
    setIsLoading(true);
    const tmpItems = [];
    const bodyPayload = {
      idMoc: detailsData?.uuid,
      detailMocItems: [],
    };
    for (let i = 0; i < detailsData?.mocItems.length; i++) {
      const element = detailsData?.mocItems[i];
      tmpItems.push({ id: element.uuid });
    }
    bodyPayload.detailMocItems = tmpItems;

    try {
      const resp = await sendRequestApi(
        `mocitems/checktomove`,
        'POST',
        JSON.stringify(bodyPayload),
        headersReq
      );
      setNotifSuccess(true);
      setIsLoading(false);
      fetchData();
      setShowModalMovePO(false);
    } catch (error) {
      setNotifFailed(true);
      setIsLoading(false);
      setShowModalMovePO(false);
    }
  };

  const handleExportCsv = () => {
    setIsLoading(true);
    const tmpData = [];
    for (let i = 0; i < mocData.length; i++) {
      const header = mocData[i];
      for (let j = 0; j < header.mocItems.length; j++) {
        const item = header.mocItems[j];
        const unitPrice = formatNumber(parseInt(item.selectedUnitPrice));
        const unitAmount = formatNumber(parseInt(item.selectedTotalPrice));
        tmpData.push({
          PURCHASE_REQUEST_NO: header.purchaseRequestNo,
          DEPARTEMEN: header.department,
          KATEGORI: header.category,
          NAMA_KAPAL: header.shipName,
          REQUEST_BY: item.createdBy,
          LOKASI: header.location,
          TANGGAL_REQUEST: formattedDate(
            new Date(header.createdAt),
            'YYYY-MM-DD',
          ),
          STATUS_MOC: header.status,
          NAMA_BARANG: item.description,
          SATUAN: item.unit,
          QTY_REQUEST: item.quantity,
          SAP_CODE: item.sapCode,
          REMARKS: item.additionalInfo,
          STATUS_BARANG: item.status,
          NAMA_TOKO: item.selectedShop,
          HARGA_SATUAN: `Rp${isNaN(parseInt(unitPrice)) ? '-' : unitPrice}`,
          TOTAL_HARGA: `Rp${isNaN(parseInt(unitAmount)) ? '-' : unitAmount}`,
          QTY_ORDER: item?.selectedQuantity ? item?.selectedQuantity : '-',
        });
      }
    }

    exportToExcel(tmpData, 'MOC', 'DATA');
    setIsLoading(false);
  };

  const onSubmitModal = async (formData: any) => {
    setFilteredQuery(formData)
    fetchFiltered(formData)
  }

  const fetchFiltered = async (queries: any) => {
    const url = buildQuery(`${process.env.NEXT_PUBLIC_API}matrixofcomparison/filter`, queries);
    if (url.includes('?')) {
      setIsLoading(true);
      try {
        const response = await fetch(url, {
          headers: {
            "account-access": sessionUser?.uuid
          }
        });
  
        if (!response.ok) {
          setIsLoading(false);
          throw new Error('Network response was not ok');
        }
  
        if (response.status === 200) {
          const data = await response.json();
          if (isMounted) {
            setMocData(data.data);
          }else{
            setMocData([])
            setNotifFailed(true)
          }
        } else {
          setMocData([])
        }
  
        setIsLoading(false); // Update isLoading state to false
      } catch (error) {
        setNotifFailed(true)
        setIsLoading(false);
        console.error('There was a problem with the fetch operation:', error);
      }
    }
  }

  if (isLoading) {
    return <OverlayLoading />;
  }

  const pendingData = mocData
    ? mocData.filter((entry) => entry.status == 'Open')
    : [];
  const completeData = mocData
    ? mocData.filter((entry) => entry.status == 'Close')
    : [];

  const canGoPrev = currentPage < 1
  const canGoNext = mocData.length >= 10

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="py-5 text-xl font-bold text-navy-700 dark:text-white">
            MOC
          </div>
          <CardMenu
            handlePopUp={() => {
              router.push('/admin/moc/create');
            }}
            showExportBtn={true}
            handleExport={handleExportCsv}
          />
        </header>

        <div className="flex justify-between items-center my-2">
          <div>
            <button
              className={`mr-2 rounded-md px-4 py-2 focus:outline-none ${
                activeTab === 'Pending' ? 'bg-blue-500 text-white' : 'bg-gray-200'
              }`}
              onClick={() => setActiveTab('Pending')}
            >
              Pending
            </button>
            <button
              className={`rounded-md px-4 py-2 focus:outline-none ${
                activeTab === 'Complete'
                  ? 'bg-blue-500 text-white'
                  : 'bg-gray-200'
              }`}
              onClick={() => setActiveTab('Complete')}
            >
              Complete
            </button>
          </div>
          <div className="flex items-center gap-2">
            <div className="flex cursor-pointer">
              <button
                onClick={() => {
                  setShowFilter(true)
                }}
                className="rounded px-3 py-1 text-white focus:outline-none bg-brand-600 hover:bg-brand-300"
              >
                <FaSearch className="inline-block" /> Filter
              </button>
            </div>
          </div>
        </div>

        {filteredQuery && <div className="flex items-center my-1">
          <div className="mr-8 font-bold p-2 bg-gray-700 rounded text-white">
            FILTERED BY :
          </div>
          <div className="flex-1 flex justify-between items-center">
            {Object.keys(filteredQuery)?.filter((key) => filteredQuery[key] !== '').map((key) => (
              <div key={key}>
                <p className="font-semibold">{key.toUpperCase()} : </p>
                <p>{filteredQuery[key]}</p>
              </div>
            ))}
            <div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={handleExportCsv}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-cyan-600 hover:bg-cyan-300"
                >
                  <FaFileExport className="inline-block h-4 w-4" /> Export
                </button>
              </div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={() => {
                    setFilteredQuery(null)
                    fetchData()
                  }}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-red-600 hover:bg-red-300"
                >
                  <MdSearchOff className="inline-block h-6 w-6" /> Clear
                </button>
              </div>
            </div>
          </div>
        </div>}

        <div className=" overflow-x-scroll ">
          <table className="w-full">
            <thead>
              <tr className="!border-px !border-gray-400">
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      PR No
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Deparment
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Category
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Ship
                    </p>
                  </div>
                </th>

                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Location
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Date Requested
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Total Items
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Action
                    </p>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {activeTab === 'Pending' &&
                pendingData.map((v: any, i: any) => (
                  <tr key={i}>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.purchaseRequestNo}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.department}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.category}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.shipName}
                      </p>
                    </td>

                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.location}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {formatDate(v.createdAt)}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.mocItems.length}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <div className="flex cursor-pointer gap-3">
                        {(v.mocItems?.filter((item: any) => item.l1Status === true).length > 0) && (
                          <CustomTooltip text="Update Items Stores">
                            <p
                              onClick={() => {
                                router.push(`/admin/moc/update/${v.uuid}`);
                              }}
                              className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                            >
                              <MdCreate className="h-5 w-5" />
                            </p>
                          </CustomTooltip>
                        )}
                        {userLevel !== "" && (userLevel === "2" || userLevel === "3") ? (
                          <>
                          {(v.mocItems?.filter((item: any) => item.l1Status === true)
                            .length > 0) && (
                            <CustomTooltip text="Choose Stores">
                              <p
                                onClick={() => {
                                  router.push(`/admin/moc/edit/${v.uuid}`);
                                }}
                                className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                              >
                                <FaListCheck className="h-5 w-5" />
                              </p>
                            </CustomTooltip>
                          )}
                          {v.mocItems?.filter((item: any) => item.l2Status === true)
                            .length > 0 && (
                            <CustomTooltip text="Move to PO">
                              <p
                                onClick={() => {
                                  setDetailsData(v);
                                  setShowModalMovePO(true);
                                }}
                                className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                              >
                                <FaFileExport className="h-5 w-5" />
                              </p>
                            </CustomTooltip>
                          )}
                          </>
                        ) : null}
                      </div>
                    </td>
                  </tr>
                ))}
              {activeTab === 'Complete' &&
                completeData.map((v: any, i: any) => (
                  <tr key={i}>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.purchaseRequestNo}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.department}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.category}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.shipName}
                      </p>
                    </td>

                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.location}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {formatDate(v.createdAt)}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.mocItems.length}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <div className="flex cursor-pointer gap-3">
                        <button
                          onClick={() => {
                            router.push(`moc/preview/${v.uuid}`); // Show edit popup
                          }}
                          className="rounded px-3 py-1 text-white focus:outline-none bg-brand-600 hover:bg-brand-300"
                        >
                          <FaPrint className="inline-block" /> Print MOC
                        </button>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>

        <div className="mt-6 flex items-center justify-between">
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage - 1
              setCurrentPage(page)
            }}
            disabled={!canGoPrev}
            className="rounded bg-gray-300 px-4 py-2">
            Previous
          </button>
          <span>
            Page <strong>{currentPage}</strong>
          </span>
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage + 1
              setCurrentPage(page)
            }}
            disabled={!canGoNext}
            className="rounded bg-gray-300 px-4 py-2"
          >
            Next
          </button>
        </div>

        {showDetail && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[700px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">Edit Data</h2>
                <button
                  onClick={() => {
                    setShowDetail(false);
                    setSelectedRow({});
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>
              <div className="w-full pt-5"></div>
              <div className="pt-10">
                <table className="w-full">
                  <thead>
                    <tr>
                      <td>No</td>
                      <td>Description</td>
                      <td>PN</td>
                      <td>Qty</td>
                      <td>Comparison</td>
                      <td>Action</td>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedRow.mocItems.map(
                      (item, index) =>
                        // Conditionally render the row only if the status is "Open"
                        item.status === 'Open' && (
                          <tr className="border-b-2" key={index}>
                            <td>{index + 1}</td>
                            <td>{item.description}</td>
                            <td>{item.quantityAsked}</td>
                            <td>
                              {item.quantity} {item.unit}
                            </td>
                            <td>
                              <button
                                onClick={() => {
                                  setShowComparisonPopup(true),
                                    handleAddComparisonClick(item);
                                }} // Ketika tombol diklik, atur state untuk menampilkan pop-up
                                className=" mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                              >
                                Add Comparison
                              </button>
                            </td>
                            <td>
                              {' '}
                              <button
                                onClick={() => {
                                  handleCheckComparisonClick(item);
                                }}
                                disabled={
                                  item.shopOneName == null ? true : false
                                }
                                className={`mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none
                                ${
                                  item.shopOneName == null
                                    ? 'bg-gray-500 hover:bg-gray-600'
                                    : ''
                                }
                                `}
                              >
                                Show Toko
                              </button>
                            </td>
                            {/* Tambahkan input field untuk matriks perbandingan */}
                          </tr>
                        ),
                    )}
                  </tbody>
                </table>
              </div>
              <button
                onClick={() => {
                  moveToPO(selectedRow.uuid, selectedRow.mocItems);
                }}
                className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
              >
                Move to PO
              </button>
            </div>
          </div>
        )}

        {showComparisonPopup && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[700px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              {/* Isi dari pop-up comparison */}
              <div className="flex items-center justify-between">
                <h2>Comparison</h2>
                <button
                  onClick={() => {
                    setShowComparisonPopup(false), setSelectedMocRow({});
                  }} // Tombol untuk menutup pop-up
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>
              {selectedMocRow && (
                <>
                  <div>
                    <p>Description: {selectedMocRow.description}</p>
                    <p>PN: {selectedMocRow.quantityAsked}</p>
                    <p>
                      Qty: {selectedMocRow.quantity} {selectedMocRow.unit}
                    </p>
                  </div>

                  <div className="flex items-center gap-1">
                    <div className="flex flex-col">
                      <label className="ml-2">Nama Toko Pertama</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopOneName: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Unit Price</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopOneUnitPrice: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Total Price</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        disabled
                        type="text"
                        placeholder={String(
                          selectedMocRow.quantity *
                            selectedMocRow.shopOneUnitPrice,
                        )}
                        value={
                          selectedMocRow.quantity *
                          selectedMocRow.shopOneUnitPrice
                        }
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopOneTotalPrice: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Remark</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopOneRemarks: newName,
                          });
                        }}
                      />
                    </div>
                  </div>

                  <div
                    className="flex items-center gap-1"
                    key={selectedMocRow.uuid}
                  >
                    <div className="flex flex-col">
                      <label className="ml-2">Nama Toko Kedua</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopTwoName: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Unit Price</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopTwoUnitPrice: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Total Price</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        disabled
                        type="text"
                        placeholder={String(
                          selectedMocRow.quantity *
                            selectedMocRow.shopTwoUnitPrice,
                        )}
                        value={
                          selectedMocRow.quantity *
                          selectedMocRow.shopTwoUnitPrice
                        }
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopTwoTotalPrice: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Remark</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopTwoRemarks: newName,
                          });
                        }}
                      />
                    </div>
                  </div>

                  <div
                    className="flex items-center gap-1"
                    key={selectedMocRow.uuid}
                  >
                    <div className="flex flex-col">
                      <label className="ml-2">Nama Toko Ketiga</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopThreeName: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Unit Price</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopThreeUnitPrice: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Total Price</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        disabled
                        type="text"
                        placeholder={String(
                          selectedMocRow.quantity *
                            selectedMocRow.shopthreeUnitPrice,
                        )}
                        value={
                          selectedMocRow.quantity *
                          selectedMocRow.shopThreeUnitPrice
                        }
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopThreeTotalPrice: newName,
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Remark</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          setSelectedMocRow({
                            ...selectedMocRow,
                            shopThreeRemarks: newName,
                          });
                        }}
                      />
                    </div>
                  </div>
                </>
              )}
              <button
                onClick={handleSaveClick}
                className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
              >
                Save
              </button>
            </div>
          </div>
        )}

        {showTokoPopup && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[700px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              {/* Isi dari pop-up comparison */}
              <div className="flex items-center justify-between">
                <h2>Comparison</h2>
                <button
                  onClick={() => {
                    setShowTokoPopup(false), setSelectedMocRow({});
                  }} // Tombol untuk menutup pop-up
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>
              <table className="w-full">
                <thead>
                  <tr>
                    <td>Nama Toko</td>
                    <td>Unit Price</td>
                    <td>Total Price</td>
                    <td>Remarks</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{selectedMocRow.shopOneName}</td>
                    <td>{selectedMocRow.shopOneUnitPrice}</td>
                    <td>{selectedMocRow.shopOneTotalPrice}</td>
                    <td>{selectedMocRow.shopOneRemarks}</td>
                  </tr>
                  <tr>
                    <td>{selectedMocRow.shopTwoName}</td>
                    <td>{selectedMocRow.shopTwoUnitPrice}</td>
                    <td>{selectedMocRow.shopTwoTotalPrice}</td>
                    <td>{selectedMocRow.shopTwoRemarks}</td>
                  </tr>
                  <tr>
                    <td>{selectedMocRow.shopThreeName}</td>
                    <td>{selectedMocRow.shopThreeUnitPrice}</td>
                    <td>
                      {selectedMocRow.shopThreeTotalPrice == 0
                        ? null
                        : selectedMocRow.shopThreeTotalPrice}
                    </td>
                    <td>{selectedMocRow.shopThreeRemarks}</td>
                  </tr>
                </tbody>
              </table>
              <button
                onClick={handleApproveClick}
                disabled={selectedMocRow.l2Status == true ? true : false}
                className={`mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none
                ${
                  selectedMocRow.l2Status == true
                    ? 'bg-gray-500 hover:bg-gray-600'
                    : ''
                }
                `}
              >
                {selectedMocRow.l2Status == true ? 'Approved' : 'Approve'}
              </button>
            </div>
          </div>
        )}
      </Card>

      {notifSuccess && (
        <NotificationOverlay
          title="Info"
          message="Successfully submit data!"
          color="green"
          isShow={notifSuccess}
          handleClose={() => setNotifSuccess(false)}
        />
      )}

      {notifFailed && (
        <NotificationOverlay
          title="Failed"
          message="Something went wrong. Please try again!"
          color="red"
          isShow={notifFailed}
          handleClose={() => setNotifFailed(false)}
        />
      )}

      {showModalMovePO && (
        <ModalMoveToPO
          detailsData={detailsData}
          onClose={() => setShowModalMovePO(false)}
          onSubmit={onSubmitToPO}
        />
      )}

      {showFilter && (
        <ModalFilter
          title={`Filter Data Matrix of Comparison`}
          onClose={() => setShowFilter(false)}
          onSubmit={onSubmitModal}
        />
      )}
    </div>
  );
};

const ModalMoveToPO = ({ onClose, detailsData, onSubmit }: any) => {
  const listItems = detailsData?.mocItems || [];
  return (
    <DefaultModal title="Move Data To Purchase Order" onClose={onClose}>
      <div className="mt-6 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 pb-2">
        <ul role="list" className="divide-y-2 divide-gray-100">
          {listItems.map((item: any, index: number) => (
            <li
              key={`item-${index}`}
              className="flex justify-between gap-x-6 py-5"
            >
              <div className="flex min-w-0 gap-x-4">
                <FaBox className="h-12 w-12 flex-none text-gray-700" />
                <div className="min-w-0 flex-auto">
                  <p className="text-xl font-semibold leading-6">
                    {item.description}
                  </p>
                  <p className="mt-1 truncate text-base leading-6">
                    Asked Qty : <strong>{item.quantity}</strong> {item.unit}
                  </p>
                </div>
              </div>
              <div className="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
                {item.storeDetails
                  .filter((store) => store.isSelected)
                  .map((store) => (
                    <div className="m-1 grid min-w-full grid-cols-2 gap-x-2 self-end rounded-md border p-3" key={store.shopName}>
                      <p className="col-span-2 text-base leading-6">
                        Store Name : <strong>{store.shopName}</strong>
                      </p>
                      <p className="col-span-2 mt-1 text-base leading-5">
                        Unit Price :{' '}
                        <strong>
                          Rp{formatNumber(parseInt(store.unitPrice))}
                        </strong>
                      </p>
                      <p className="col-span-2 mt-1 text-base leading-5">
                        Qty : <strong>{store.quantity}</strong>
                      </p>
                      <p className="col-span-2 mt-1 text-base leading-5">
                        Total Price :{' '}
                        <strong>
                          Rp{formatNumber(parseInt(store.totalPrice))}
                        </strong>
                      </p>
                      <p className="col-span-2 mt-1 text-base leading-5">
                        Tax (%) : <strong>{store.tax ? store.tax : 0}%</strong>
                      </p>
                    </div>
                  ))}
              </div>
            </li>
          ))}
        </ul>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit();
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </DefaultModal>
  );
};

const ModalFilter = ({ onClose, onSubmit, title }: any) => {
  const [formData, setFormData] = useState({
    category: '',
    department: '',
    ship: '',
    startDate: '',
    endDate: '',
  })
  return (
    <DefaultModal title={title} onClose={onClose}>
      <div className="mt-6 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 py-2">
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Start Date</p>
          <input
              type="date"
              name="startDate"
              id="startDate"
              placeholder="Input Start Date"
              value={formData.startDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  startDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">End Date</p>
          <input
              type="date"
              name="endDate"
              id="endDate"
              placeholder="Input End Date"
              value={formData.endDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  endDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Category</p>
          <AutocompleteInput
              placeholder="Search Category"
              apiUrl="category/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  category: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Department</p>
          <AutocompleteInput
              placeholder="Search Department"
              apiUrl="department/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  department: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Ship</p>
          <AutocompleteInput
              placeholder="Search Ship"
              apiUrl="ship/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  ship: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit(formData);
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </DefaultModal>
  );
};

export default Page;
