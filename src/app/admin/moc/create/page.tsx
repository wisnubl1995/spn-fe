'use client';
import Card from 'components/card';
import AutocompleteInput from 'components/fields/AutoComplete';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import CustomTooltip from 'components/tooltip/CustomTooltip';
import AuthContext from 'contexts/AuthContext';
import { useRouter } from 'next/navigation';
import React, { useContext, useState } from 'react';
import { sendRequestApi } from 'utils/api/fetchData';
import onAddAutocomplete from 'utils/api/handleAddAutocomplete';

type Props = {};

const listToko = [
  {
    label: 'Toko 1',
    keyTokoName: 'shopOneName',
    keyUnitName: 'shopOneUnitPrice',
    keyTotalPrice: 'shopOneTotalPrice',
    keyQty: 'shopOneQuantity',
    keyTax: 'shopOneTax',
    keyRemarks: 'shopOneRemarks',
    value: '',
  },
  {
    label: 'Toko 2',
    keyTokoName: 'shopTwoName',
    keyUnitName: 'shopTwoUnitPrice',
    keyTotalPrice: 'shopTwoTotalPrice',
    keyQty: 'shopTwoQuantity',
    keyTax: 'shopTwoTax',
    keyRemarks: 'shopTwoRemarks',
    value: '',
  },
  {
    label: 'Toko 3',
    keyTokoName: 'shopThreeName',
    keyUnitName: 'shopThreeUnitPrice',
    keyTotalPrice: 'shopThreeTotalPrice',
    keyQty: 'shopThreeQuantity',
    keyTax: 'shopThreeTax',
    keyRemarks: 'shopThreeRemarks',
    value: '',
  },
  {
    label: 'Toko 4',
    keyTokoName: 'shopFourName',
    keyUnitName: 'shopFourUnitPrice',
    keyTotalPrice: 'shopFourTotalPrice',
    keyQty: 'shopFourQuantity',
    keyTax: 'shopFourTax',
    keyRemarks: 'shopFourRemarks',
    value: '',
  },
  {
    label: 'Toko 5',
    keyTokoName: 'shopFiveName',
    keyUnitName: 'shopFiveUnitPrice',
    keyTotalPrice: 'shopFiveTotalPrice',
    keyQty: 'shopFiveQuantity',
    keyTax: 'shopFiveTax',
    keyRemarks: 'shopFiveRemarks',
    value: '',
  },
];

const Page = (props: Props) => {
  const router = useRouter();
  const { sessionUser } = useContext(AuthContext);
  const headersReq = {
		"account-access": sessionUser?.uuid
	}
  const [selectedDataPR, setSelectedDataPR] = useState(null);
  const [dataToko, setDataToko] = useState(listToko);
  const [dataItems, setDataItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showNotif, setShowNotif] = useState(false);
  const [notifMessage, setNotifMessage] = useState(null);

  const handleSelect = (suggestion: any) => {
    setSelectedDataPR(suggestion);
    setDataItems(
      suggestion?.mocItems.map((obj: any, i: number) => ({
        id: obj.uuid,
        storeDetails: [
          {
            name: '',
            unitPrice: '',
            quantity: '',
            totalPrice: '',
            tax: '',
            remarks: '',
          },
          {
            name: '',
            unitPrice: '',
            quantity: '',
            totalPrice: '',
            tax: '',
            remarks: '',
          },
          {
            name: '',
            unitPrice: '',
            quantity: '',
            totalPrice: '',
            tax: '',
            remarks: '',
          },
          {
            name: '',
            unitPrice: '',
            quantity: '',
            totalPrice: '',
            tax: '',
            remarks: '',
          },
          {
            name: '',
            unitPrice: '',
            quantity: '',
            totalPrice: '',
            tax: '',
            remarks: '',
          },
        ],
        createdBy: sessionUser?.name,
        l1Status: true,
        l2Status: false,
      })),
    );
    setNotifMessage(null);
  };

  const handleSubmit = async () => {
    setNotifMessage(null);
    setIsLoading(true);
    const minStore = 2;
    let totalStore = 0;
    
    for (let i = 0; i < dataItems.length; i++) {
      const element = dataItems[i];
      for (let j = 0; j < element.storeDetails.length; j++) {
        const store = element.storeDetails[j];
        if (totalStore >= 2) {
          break;
        }
        if (store.name != '') {
          totalStore += 1;
        }
      }
    }

    if (totalStore < minStore) {
      setNotifMessage('Failed! Minimun Store must have more than 1.');
      setShowNotif(true);
      setIsLoading(false);
      return;
    }

    const payload = {
      dataMocItems: dataItems,
    };

    try {
      const resp = await sendRequestApi(
        `mocitems/createstore`,
        'POST',
        JSON.stringify(payload),
        headersReq
      );
      setNotifMessage(
        `${resp?.message}. Will be redirect to MOC page in 3 seconds`,
      );
      setShowNotif(true);
      setIsLoading(false);

      setTimeout(() => {
        router.back();
      }, 3000);
    } catch (error) {
      setNotifMessage('Failed! Internal Server Error!');
      setShowNotif(true);
      setIsLoading(false);
      console.log(`Failed submit moc!`, error?.message);
    }
  };

  const handleChangeInput = (event: any, itemId: any, indexStore: any) => {
    const newData = [...dataItems]; // Create a copy to avoid mutation
    const index = newData.findIndex((item) => item.id === itemId);

    if (index !== -1) {
      newData[index].storeDetails[indexStore][event.target.name] =
        event.target.value;

      const oriItem = selectedDataPR?.mocItems[index];
      if (event.target.name.includes('quantity')) {
        const totalPrice =
          parseInt(newData[index].storeDetails[indexStore].unitPrice) *
          parseInt(event.target.value);

        const keyTotalPrice = event.target.getAttribute('data-keytotal');
        newData[index].storeDetails[indexStore][
          keyTotalPrice
        ] = `${totalPrice}`;
      }

      if (event.target.name.includes('quantity')) {
        if (
          parseInt(event.target.value) > parseInt(oriItem.quantity) ||
          parseInt(event.target.value) < 0
        ) {
          newData[index].storeDetails[indexStore][event.target.name] = parseInt(
            oriItem.quantity,
          );
        }
      }

      setDataItems(newData);
      setNotifMessage('');
    }
  };

  if (isLoading) return <OverlayLoading />;

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="py-5 text-xl font-bold text-navy-700 dark:text-white">
            Form Matrix Of Comparisson
          </div>
        </header>
        <div className="mt-4 border-b border-t border-gray-900/10 pb-12">
          <div className="mr-8">
            <div className="mt-6 grid grid-cols-4 gap-x-6 gap-y-8">
              <div className="col-span-1">
                <label
                  htmlFor="name"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Search Purchase Request Number
                </label>
                <div className="mt-2">
                  <AutocompleteInput
                    placeholder="Search PR Number"
                    apiUrl="matrixofcomparison?page=1&pageSize=10&status=Open"
                    searchQuery="prNo"
                    keyLabel="purchaseRequestNo"
                    keyValue="purchaseRequestNo"
                    minTreshold={5}
                    onSelect={handleSelect}
                  />
                  <p className="mt-3 text-sm leading-6 text-gray-600">
                    Min 5 character for search.
                  </p>
                </div>
              </div>
              <div className="col-span-3 my-6 rounded border p-6">
                <div className="px-4 sm:px-0">
                  <h3 className="text-base font-semibold leading-7 text-gray-900">
                    PR Information{' '}
                    {`[${
                      selectedDataPR ? selectedDataPR.purchaseRequestNo : '-'
                    }]`}
                  </h3>
                  <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">
                    Details Purchase Request.
                  </p>
                </div>
                <div className="mt-6 border-t border-gray-100">
                  <dl className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 sm:gap-y-6 lg:gap-x-6">
                    <div className="border-t border-gray-200 pt-4">
                      <dt className="font-medium text-gray-900">Department</dt>
                      <dd className="mt-2 text-sm text-gray-700">
                        {selectedDataPR ? selectedDataPR.department : '-'}
                      </dd>
                    </div>
                    <div className="border-t border-gray-200 pt-4">
                      <dt className="font-medium text-gray-900">Category</dt>
                      <dd className="mt-2 text-sm text-gray-700">
                        {selectedDataPR ? selectedDataPR.category : '-'}
                      </dd>
                    </div>
                    <div className="border-t border-gray-200 pt-4">
                      <dt className="font-medium text-gray-900">Location</dt>
                      <dd className="mt-2 text-sm text-gray-700">
                        {selectedDataPR ? selectedDataPR.location : '-'}
                      </dd>
                    </div>
                    <div className="border-t border-gray-200 pt-4">
                      <dt className="font-medium text-gray-900">Ship Name</dt>
                      <dd className="mt-2 text-sm text-gray-700">
                        {selectedDataPR ? selectedDataPR.shipName : '-'}
                      </dd>
                    </div>
                  </dl>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-6 flex flex-row">
            <div className="min-w-[500px] overflow-auto rounded border pl-4">
              <div className="min-w-[600px]">
                <label className="mt-4 block text-base font-semibold leading-6 text-gray-900">
                  List Items
                </label>
                <div className="my-3 grid grid-cols-3">
                  <label className="font-semibold">Description</label>
                  <label className="font-semibold">Unit</label>
                  <label className="font-semibold">Qty</label>
                </div>
                {selectedDataPR?.mocItems.map((item, index) => (
                  <div
                    key={`ori-item-${index}`}
                    className="my-2 grid grid-cols-3"
                  >
                    <div className="mx-1">
                      <CustomTooltip text={item.description}>
                        <p className=" overflow-hidden text-ellipsis whitespace-nowrap py-1">
                          {item.description}
                        </p>
                      </CustomTooltip>
                    </div>
                    <div className="mx-1">
                      <p className=" py-1">{item.unit}</p>
                    </div>
                    <div className="mx-1">
                      <p className=" py-1">{item.quantity}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="ml-4 flex flex-row items-center overflow-auto">
              {dataToko.map((toko, index) => (
                <div
                  key={`toko-${index}`}
                  className="mr-4 min-w-[760px] border-x px-2 text-center"
                >
                  <div className="flex flex-row items-center justify-center">
                    <p className="mx-1 py-2 px-4 rounded font-semibold bg-gray-400">{index+1}</p>
                    <AutocompleteInput
                      placeholder="Search Store Name"
                      apiUrl="store/search"
                      searchQuery="name"
                      keyLabel="name"
                      keyValue="name"
                      minTreshold={3}
                      onSelect={async (suggestion: any) => {
                        dataToko[index].value = suggestion?.name;
                        setDataToko(dataToko);
                        setDataItems(
                          dataItems.map((it: any) => ({
                            ...it,
                            storeDetails: [
                              ...it.storeDetails.slice(0, index),
                              {
                                ...it.storeDetails[index],
                                name: suggestion?.name,
                              },
                              ...it.storeDetails.slice(index + 1),
                            ],
                          })),
                        );

                        if (suggestion?.isNew) {
                          await onAddAutocomplete('store', {
                            name: suggestion.name,
                            desc: '',
                            email: '',
                            address: '',
                            telephone: '',
                            fax: '',
                          }, headersReq);
                        }
                      }}
                    />
                  </div>
                  <div className="my-2 grid grid-cols-5">
                    <label className="mx-1 rounded border bg-gray-400 font-semibold">
                      Unit Price
                    </label>
                    <label className="mx-1 rounded border bg-gray-400 font-semibold">
                      Qty
                    </label>
                    <label className="mx-1 rounded border bg-gray-400 font-semibold">
                      Tax (%)
                    </label>
                    <label className="mx-1 rounded border bg-gray-400 font-semibold">
                      Total Price
                    </label>
                    <label className="mx-1 rounded border bg-gray-400 font-semibold">
                      Remarks
                    </label>
                  </div>
                  {dataItems?.map((item: any, i: number) => (
                    <div
                      key={`toko-${index}-item-${i}`}
                      className="my-2 grid grid-cols-5"
                    >
                      <div className="mx-1">
                        <input
                          placeholder="Unit"
                          id={`toko-${index}-item-unit-${i}`}
                          name={'unitPrice'}
                          value={item.storeDetails[index].unitPrice}
                          onChange={(e) => {
                            handleChangeInput(e, item.id, index);
                          }}
                          className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                          type="text"
                        />
                      </div>
                      <div className="mx-1">
                        <input
                          placeholder="Quantity"
                          data-keytotal={'totalPrice'}
                          id={`toko-${index}-item-quantity-${i}`}
                          name={'quantity'}
                          value={item.storeDetails[index].quantity}
                          onChange={(e) => {
                            handleChangeInput(e, item.id, index);
                          }}
                          className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                          type="number"
                        />
                      </div>
                      <div className="mx-1">
                        <input
                          placeholder="Tax (%)"
                          min={0}
                          id={`toko-${index}-item-tax-${i}`}
                          name={'tax'}
                          defaultValue={0}
                          value={
                            item.storeDetails[index].tax
                              ? item.storeDetails[index].tax
                              : 0
                          }
                          onChange={(e) => {
                            handleChangeInput(e, item.id, index);
                          }}
                          className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                          type="number"
                        />
                      </div>
                      <div className="mx-1">
                        <input
                          placeholder="Total"
                          disabled
                          id={`toko-${index}-item-total-${i}`}
                          name={'totalPrice'}
                          value={
                            item.storeDetails[index].totalPrice
                              ? item.storeDetails[index].totalPrice
                              : 0
                          }
                          onChange={(e) => {
                            handleChangeInput(e, item.id, index);
                          }}
                          className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                          type="text"
                        />
                      </div>
                      <div className="mx-1">
                        <input
                          placeholder="Remarks"
                          id={`toko-${index}-item-remarks-${i}`}
                          name={'remarks'}
                          value={item.storeDetails[index].remarks}
                          onChange={(e) => {
                            handleChangeInput(e, item.id, index);
                          }}
                          className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                          type="text"
                        />
                      </div>
                    </div>
                  ))}
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="mt-6 flex items-center justify-end gap-x-6">
          <button
            type="button"
            onClick={() => router.back()}
            className="text-md rounded-md bg-gray-600 px-4 py-2 text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
          >
            {' '}
            Cancel
          </button>
          <button
            type="button"
            onClick={handleSubmit}
            className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
          >
            {' '}
            Submit
          </button>
        </div>
      </Card>

      {showNotif && (
        <NotificationOverlay
          isShow={showNotif}
          title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
          message={notifMessage}
          handleClose={() => setShowNotif(false)}
          color={notifMessage?.includes('Failed') ? 'red' : 'green'}
        />
      )}
    </div>
  );
};

export default Page;
