'use client'
import Card from 'components/card';
import AutocompleteInput from 'components/fields/AutoComplete';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation';
import React, { useContext, useEffect, useState } from 'react'
import { FaBoxOpen } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import onAddAutocomplete from 'utils/api/handleAddAutocomplete';
import { validateForm } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
	const router = useRouter()
    const params = useParams()
    const uuid = params?.id
	const { sessionUser } = useContext(AuthContext)
	const headersReq = {
		"account-access": sessionUser?.uuid
	}
	const [formData, setFormData] = useState({
		categoryShip: "",
		name: "",
		email: "",
		username: "",
		password: "",
		idEmployee: "",
		division: "",
		location: "",
		ltree: "",
		status: "",
	})
	const [listModules, setListModules] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [detailsAccount, setDetailsAccount] = useState(null)

	const fetchModules = async (currentModules: any) => {		
		setIsLoading(true)
		try {
			const resp = await sendRequestApi(`module`, 'GET', null, headersReq)

			if (resp.data?.length > 0) {
                const list = resp.data.map(item => ({
                    ...item,
                    isSelected: checkAccountHasModules(currentModules, item.uuid)
                }))
				setListModules(list)
			}else{
				setListModules([])
			}
			setIsLoading(false)
		} catch (error) {
			setIsLoading(false)
			setNotifMessage(`Failed! Fetching items. Error: ${error?.message}`)
            setShowNotif(true)
			console.log('error fetching modules!')
		}
	}

    const fetchDetailsModules = async () => {
        setIsLoading(true)
		try {
			const resp = await sendRequestApi(`account/module/${uuid}`, 'GET', null, headersReq)
			if (resp?.data) {
                await fetchModules(resp.data?.modules)
			}else{
                await fetchModules([])
			}
			setIsLoading(false)
		} catch (error) {
			setIsLoading(false)
			setNotifMessage(`Failed! Fetching details modules. Error: ${error?.message}`)
            setShowNotif(true)
			console.log('error fetching details modules!')
		}
    }

    const fetchDetailsAccount = async () => {		
		setIsLoading(true)
		try {
			const resp = await sendRequestApi(`account/${uuid}`, 'GET', null, headersReq)

			if (resp.data) {
				setDetailsAccount(resp.data)
                setFormData({
                    categoryShip: resp.data.categoryShip,
                    name: resp.data.name,
                    email: resp.data.email,
                    username: resp.data.username,
                    password: resp.data.password,
                    idEmployee: resp.data.idEmployee,
                    division: resp.data.division,
                    location: resp.data.location,
                    ltree: resp.data.ltree,
                    status: resp.data.status,
                })
			}else{
				setDetailsAccount(null)
			}
			setIsLoading(false)
		} catch (error) {
			setIsLoading(false)
			setNotifMessage(`Failed! Fetching details account. Error: ${error?.message}`)
            setShowNotif(true)
			console.log('error fetching details account!')
		}
	}

    const checkAccountHasModules = (currentModules, moduleId) => {
        const idxAccModules = currentModules.findIndex(mod => mod.uuid === moduleId)
        return idxAccModules !== -1
    }

    useEffect(() => {
        fetchDetailsAccount()
        fetchDetailsModules()
    }, [])

	const handleOnChange = (e, item) => {
		const currModules = [...listModules]
		const findIndex = currModules.findIndex(obj => obj.uuid === item.uuid)
		if (e.target.checked) {
			currModules[findIndex].isSelected = true
		} else {
			currModules[findIndex].isSelected = false
		}
		setListModules(currModules)
	}

	const handleSubmit = async () => {
		const choosedModules = listModules.filter(item => item.isSelected)
		if (choosedModules.length <= 0) {
			setNotifMessage('Failed! Assign minimum 1 module.')
            setShowNotif(true)
			return
		}

		const optionalFields = { location: true, idEmployee: true, division: true, categoryShip: true, password: true }
		if (!validateForm(formData, optionalFields)) {
			setNotifMessage('Failed! Fill all required fields.')
            setShowNotif(true)
			return
		}else{
			setIsLoading(true)
			try {
				const payload = {
					...formData,
                    categoryShip: "",
					status: parseInt(formData.status),
                    password: formData.password ? formData.password : "",
					id: detailsAccount.uuid
				}
				const resp = await sendRequestApi(`account/update`, 'POST', JSON.stringify(payload), headersReq)

                const payloadModules = {
                    idAccount: detailsAccount.uuid,
                    modules: choosedModules.map(mod => ({
                        id: mod.uuid
                    }))
                }
                await sendRequestApi(`menu`, 'POST', JSON.stringify(payloadModules), headersReq)

				setIsLoading(false)
				setNotifMessage(resp?.message ? `${resp?.message}. Redirect to Outbound page in 3 seconds.` : `Successfully created outbound.`)
            	setShowNotif(true)

				setTimeout(() => {
					router.back()
				}, 3000);
			} catch (error) {
				setIsLoading(false)
				setNotifMessage(`Failed! ${error?.message}.`)
            	setShowNotif(true)
			}
		}
	}

	if (isLoading) 
        return <OverlayLoading />

	return (
		<div className="mt-10">
			<Card extra={'w-full pb-10 p-4 h-full'}>
				<header className="relative flex items-center justify-between">
					<div className="py-5 text-xl font-bold text-navy-700 dark:text-white">
						Settings Account
					</div>
				</header>
				<div className="mt-2 border-b border-t border-gray-900/10 pb-12">
					<div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-1 sm:grid-cols-6">
						<div className="sm:col-span-2">
                            <label htmlFor="name" className="block text-base font-medium leading-6 text-gray-900">
                                Name <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
								<input
									id="name"
									name="name"
									placeholder="Name"
									type="text"
									value={formData?.name}
									onChange={(e) =>
										setFormData({
											...formData,
											name: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="email" className="block text-base font-medium leading-6 text-gray-900">
                                Email <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
								<input
									id="email"
									name="email"
									placeholder="Email"
									type="text"
									value={formData?.email}
									onChange={(e) =>
										setFormData({
											...formData,
											email: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="idEmployee" className="block text-base font-medium leading-6 text-gray-900">
                                Employee ID
                            </label>
                            <div className="mt-2">
								<input
									id="idEmployee"
									name="idEmployee"
									placeholder="Employee ID"
									type="text"
									value={formData?.idEmployee}
									onChange={(e) =>
										setFormData({
											...formData,
											idEmployee: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="username" className="block text-base font-medium leading-6 text-gray-900">
                                Username <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
								<input
									id="username"
                                    autoComplete="off"
									name="username"
									placeholder="Username"
									type="text"
									value={formData?.username}
									onChange={(e) =>
										setFormData({
											...formData,
											username: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="password" className="block text-base font-medium leading-6 text-gray-900">
                                Password <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
								<input
									id="password"
                                    autoComplete="off"
									name="password"
									placeholder="Password"
									type="password"
									value={formData?.password}
									onChange={(e) =>
										setFormData({
											...formData,
											password: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="division" className="block text-base font-medium leading-6 text-gray-900">
                                Division
                            </label>
                            <div className="mt-2">
								<input
									id="division"
									name="division"
									placeholder="Division"
									type="text"
									value={formData?.division}
									onChange={(e) =>
										setFormData({
											...formData,
											division: e.target.value,
										})
									}
									className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
								/>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="ship" className="block text-base font-medium leading-6 text-gray-900">
                                Level <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
                                <select
                                    className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                                    value={formData?.ltree}
                                    onChange={(e) => {
                                        setFormData({
                                            ...formData,
                                            ltree: e.target.value,
                                        });
                                    }}
                                >
                                    <option value="" disabled selected hidden>
                                        Select Level
                                    </option>
                                    <option value="1">Level 1</option>
                                    <option value="2">Level 2</option>
                                    <option value="3">Level 3</option>
                                </select>
                            </div>
                        </div>
						<div className="sm:col-span-2">
                            <label htmlFor="ship" className="block text-base font-medium leading-6 text-gray-900">
                                Status <span className='text-red-500'>*</span>
                            </label>
                            <div className="mt-2">
                                <select
                                    className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                                    value={formData?.status}
                                    onChange={(e) => {
                                        setFormData({
                                            ...formData,
                                            status: e.target.value,
                                        });
                                    }}
                                >
                                    <option value="" disabled selected hidden>
                                        Select Status
                                    </option>
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="location" className="block text-base font-medium leading-6 text-gray-900">
                                Location
                            </label>
                            <div className="mt-2">
                                <AutocompleteInput
                                    placeholder="Search Location"
                                    apiUrl="location/search"
                                    searchQuery='name'
									defaultValue={formData.location}
                                    keyLabel='name'
                                    keyValue='name'
                                    minTreshold={3}
                                    onSelect={async (suggestion: any) => {
                                        setFormData({
											...formData,
											location: suggestion?.name,
										})
                                        if (suggestion?.isNew) {
                                            await onAddAutocomplete('location', { 
                                                name: suggestion.name,
												email: '',
                                                address: '',
                                                telephone: '',
                                                fax: '',
                                                desc: ''
                                            }, headersReq)
                                        }
                                    }}
                                />
                                <p className="mt-1 text-sm leading-6 text-gray-600">Min 3 character for search.</p>
                            </div>
                        </div>
					</div>

					<div className="mt-4 border-t">
						<h3 className="font-bold mt-3 ml-1 rounded bg-gray-700 text-white p-2">List Modules Application</h3>
						<div className="my-3 grid grid-cols-4 gap-6">
                            {listModules?.map((item, idx) => (
                            <div className="col-span-1 flex items-center" key={idx}>
                                <label className="text-base p-2 font-semibold border rounded bg-gray-200 mx-1 flex-1">
                                    {item.moduleName}
                                </label>
                                <input
                                    type="checkbox"
                                    checked={item.isSelected}
                                    name={`${idx}-module-check`}
                                    onChange={(e) => handleOnChange(e, item)}
                                    className="ml-2 h-6 w-6 rounded border-gray-300 text-brand-600 focus:ring-brand-600" />
                            </div>
                            ))}
						</div>
					</div>
				</div>

				<div className="mt-6 flex items-center justify-end gap-x-6">
					<button
						type="button"
						onClick={() => router.back()}
						className="text-md rounded-md bg-gray-600 px-4 py-2 text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600" >
						{' '}
						Cancel
					</button>
					<button
						type="button"
						onClick={handleSubmit}
						className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600">
						{' '}
						Submit
					</button>
				</div>
			</Card>

			{showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
		</div>
	)
}

export default Page