'use client';
import ShipTable from 'components/admin/data-tables/ShipTable';
import OverlayLoading from 'components/loading/OverlayLoading';
import Default from 'components/modal/Default';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import React, { useContext, useEffect, useState } from 'react';

type Props = {};

const Page = (props: Props) => {
  const { sessionUser } = useContext(AuthContext)
  const [listData, setListData] = useState<any>();
  const [isLoading, setIsLoading] = useState(true);
  const [showModalForm, setShowModalForm] = useState(false);
  const [editedData, setEditedData] = useState<any>(null);
  const [notifSuccess, setNotifSuccess] = useState(false);
  const [notifFailed, setNotifFailed] = useState(false);

  const fetchData = async () => {
    const url = `${process.env.NEXT_PUBLIC_API}ship`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        setNotifFailed(true);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          setListData(data.data);
        }
      }else{
        setListData([])
      }
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setNotifFailed(true);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const saveData = async (payload: any) => {
    setIsLoading(true);

    const url = `${process.env.NEXT_PUBLIC_API}ship`;
    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "account-access": sessionUser?.uuid
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        setIsLoading(false);
        setNotifFailed(true);
        throw new Error('Network response was not ok');
      } else {
        await fetchData();
        setNotifSuccess(true);
      }

      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setNotifFailed(true);
      console.error('There was a problem with the save operation:', error);
    }
  };

  const updateData = async (payload: any) => {
    setIsLoading(true);

    const url = `${process.env.NEXT_PUBLIC_API}ship/update`;
    try {
      const response = await fetch(url, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          "account-access": sessionUser?.uuid
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        setIsLoading(false);
        setNotifFailed(true);
        throw new Error('Network response was not ok');
      } else {
        await fetchData();
        setNotifSuccess(true);
      }

      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setNotifFailed(true);
      console.error('There was a problem with the update operation:', error);
    }
  };

  const handleSubmit = async (savedData: any) => {
    if (editedData && editedData?.uuid != '' && editedData?.uuid != null) {
      savedData.uuid = editedData.uuid;
      await updateData(savedData);
    } else {
      await saveData(savedData);
    }
  };

  const onEdit = (ctx?: any) => {
    console.log(ctx?.row?.original)
    const { uuid, name, desc, shipCode, location, image, category } = ctx?.row?.original;
    setEditedData({ uuid, name, desc, shipCode, location, image, category });
    setShowModalForm(true);
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (isLoading) return <OverlayLoading />;

  return (
    <div className="mt-10">
      <ShipTable
        onHandleAdd={() => setShowModalForm(true)}
        onHandleEdit={onEdit}
        tableName="Data Ships"
        tableData={listData}
      />

      {showModalForm && (
        <ModalForm
          onClose={() => {
            setEditedData(null);
            setShowModalForm(false);
          }}
          defaultValue={editedData ? editedData : null}
          onSubmit={handleSubmit}
        />
      )}

      {notifSuccess && (
        <NotificationOverlay
          isShow={notifSuccess}
          handleClose={() => setNotifSuccess(false)}
          title="Success"
          message="Action completed successfully!"
          color="green"
        />
      )}
      {notifFailed && (
        <NotificationOverlay
          isShow={notifFailed}
          handleClose={() => setNotifFailed(false)}
          title="Failed"
          message="Something went wrong. Please try again!"
          color="red"
        />
      )}
    </div>
  );
};

const ModalForm = ({ onClose, onSubmit, defaultValue }: any) => {
  const [formData, setFormData] = useState<any>(
    defaultValue
      ? defaultValue
      : {
          name: '',
          category: '',
          desc: '',
          shipCode: '',
          location: '',
          image: '',
        },
  );

  return (
    <Default title="Form Ship" onClose={onClose}>
      <div className="mt-6 border-b border-t border-gray-900/10 pb-12">
        <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
          <div className="col-span-full">
            <label
              htmlFor="name"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Name
            </label>
            <div className="mt-2">
              <input
                id="name"
                name="name"
                type="text"
                value={formData?.name}
                onChange={(e) =>
                  setFormData({
                    ...formData,
                    name: e.target.value,
                  })
                }
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
          </div>
          <div className="col-span-full">
            <label
              htmlFor="name"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Category
            </label>
            <div className="mt-2">
              <select
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
                value={formData?.category}
                onChange={(e) => {
                  setFormData({
                    ...formData,
                    category: e.target.value,
                  });
                }}
              >
                <option value="" disabled selected hidden>
                  Select Category
                </option>
                <option value="MOTHER VESSEL">MOTHER VESSEL</option>
                <option value="TUGBOAT">TUGBOAT</option>
                <option value="BARGE">BARGE</option>
                <option value="HOPPER BARGE">HOPPER BARGE</option>
                <option value="CRANE BARGE">CRANE BARGE</option>
                <option value="PILOT BOAT">PILOT BOAT</option>
                <option value="HARBOUR TUG">HARBOUR TUG</option>
              </select>
            </div>
          </div>
          <div className="col-span-full">
            <label
              htmlFor="desc"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Description
            </label>
            <div className="mt-2">
              <textarea
                id="desc"
                name="desc"
                value={formData?.desc}
                rows={3}
                onChange={(e) =>
                  setFormData({
                    ...formData,
                    desc: e.target.value,
                  })
                }
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
            <p className="mt-3 text-sm leading-6 text-gray-600">
              Write a few sentences about the data.
            </p>
          </div>
          <div className="col-span-full">
            <label
              htmlFor="shipCode"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Ship Code
            </label>
            <div className="mt-2">
              <input
                id="shipCode"
                name="shipCode"
                type="text"
                value={formData?.shipCode}
                onChange={(e) =>
                  setFormData({
                    ...formData,
                    shipCode: e.target.value,
                  })
                }
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
          </div>
          <div className="col-span-full">
            <label
              htmlFor="location"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Location
            </label>
            <div className="mt-2">
              <input
                id="location"
                name="location"
                type="text"
                value={formData?.location}
                onChange={(e) =>
                  setFormData({
                    ...formData,
                    location: e.target.value,
                  })
                }
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
          </div>
          <div className="col-span-full">
            <label
              htmlFor="image"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Image
            </label>
            <div className="mt-2">
              <input
                id="image"
                name="image"
                type="text"
                value={formData?.image}
                onChange={(e) =>
                  setFormData({
                    ...formData,
                    image: e.target.value,
                  })
                }
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            setFormData({
              name: '',
              desc: '',
              shipCode: '',
              location: '',
              image: '',
            });
            onSubmit(formData);
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </Default>
  );
};

export default Page;
