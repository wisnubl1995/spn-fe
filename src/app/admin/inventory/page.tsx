'use client';

import Card from 'components/card';
import CardMenu from 'components/card/CardMenu';
import OverlayLoading from 'components/loading/OverlayLoading';
import AuthContext from 'contexts/AuthContext';

import React, { useContext, useEffect, useState } from 'react';
import { MdCreate, MdOutlineDeleteForever } from 'react-icons/md';
import exportToExcel from 'utils/exportExcel';

import { formatDate } from 'utils/formatDate';
import { formattedDate } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
  const { sessionUser } = useContext(AuthContext);
  const [activeTab, setActiveTab] = useState('Pending');
  const [inventoryData, setInventoryData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchLocation, setSearchLocation] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  let isMounted = true;
  let timeoutId: number | any;

  const handleSelectUUID = (uuid) => {
    // setSelectedUUID(uuid);
    // // Mengambil objek yang sesuai dengan UUID yang dipilih
    // const selectedData = purchaseData.find((data) => data.uuid === uuid);
    // console.log(selectedData);
    // setSelectedRow(selectedData);
    // // Tampilkan pop-up atau lakukan operasi lain sesuai kebutuhan
    // setShowDetail(true);
  };
  const fetchData = async () => {
    setIsLoading(true);
    const url = `${process.env.NEXT_PUBLIC_API}inventory?page=1&pageSize=10&location=${searchLocation}`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (isMounted) {
          setInventoryData(data.data);
        }else{
          setInventoryData([])
        }
      }else{
        setInventoryData([])
      }

      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      setIsLoading(false);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    const debouncedFetch = debounce(fetchData, 1000); // Debounce to avoid excessive requests
    debouncedFetch();

    // Cleanup function to clear timeout on unmount
    return () => clearTimeout(timeoutId);
  }, [searchLocation]);

  const debounce = (func, delay) => {
    return (...args) => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => func.apply(this, args), delay);
    };
  };

  const handleExport = () => {
    setIsLoading(true);
    const tmpData = [];
    for (let i = 0; i < inventoryData.length; i++) {
      const header = inventoryData[i];
      tmpData.push({
        PRODUCT_NAME: header.productName,
        SATUAN: header.unit,
        QUANTITY: header.count,
        NAMA_KAPAL: header.shipName,
        KATEGORI_KAPAL: header.categoryShip,
        LOKASI: header.location,
        CREATED_BY: header.createdBy ? header.createdBy : '-',
        CREATED_AT: header.createdAt
          ? formattedDate(new Date(header.createdAt), 'YYYY-MM-DD')
          : header.createdAt,
        STATUS_INVENTORY: header.status
      });
    }

    exportToExcel(tmpData, 'INVENTORY', 'DATA');
    setIsLoading(false);
  };

  useEffect(() => {
    isMounted = true
    fetchData()
    return () => { isMounted = false }
  }, [currentPage])

  const canGoPrev = currentPage < 1
  const canGoNext = inventoryData.length >= 10

  if (isLoading) {
    return <OverlayLoading />;
  }

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="text-xl font-bold text-navy-700 dark:text-white">
            Inventory
          </div>
          <div className="flex items-center mr-2">
            <div className="flex items-center mr-2">
              <label className="text-sm font-bold text-navy-700">
                Search Location :{' '}
              </label>
              <input
                id="location"
                name="location"
                placeholder="Search by Location"
                value={searchLocation}
                onChange={(e) => {
                  setSearchLocation(e.target.value);
                }}
                className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm outline-none ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
              />
            </div>
            <CardMenu
              hideAddBtn={true}
              showExportBtn={true}
              handleExport={handleExport}
            />
          </div>
        </header>

        <div className=" overflow-x-scroll ">
          <table className="w-full">
            <thead>
              <tr className="!border-px !border-gray-400">
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Product Name
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Quantity
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Unit
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Ship
                    </p>
                  </div>
                </th>

                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Location
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Status
                    </p>
                  </div>
                </th>

                {/* <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Action
                    </p>
                  </div>
                </th> */}
              </tr>
            </thead>
            <tbody>
              {inventoryData
                ? inventoryData.map((v: any, i: any) => (
                    <tr key={i}>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.productName}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.count}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.unit}
                        </p>
                      </td>

                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.shipName}
                        </p>
                      </td>

                      <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.location}
                        </p>
                      </td>
                      <td className="min-w-[150px] border-white/0 py-3 pr-4">
                        <p className="text-sm font-bold text-navy-700 dark:text-white">
                          {v.status === 'Available' ? (
                            <small className="p-2 rounded text-white bg-green-600">{v.status}</small>
                          ) : (
                            <small className="p-2 rounded text-white bg-red-600">{v.status}</small>
                          )}
                        </p>
                      </td>

                      {/* <td className="min-w-[150px] border-white/0 py-3  pr-4">
                        {'-'}
                      </td> */}
                    </tr>
                  ))
                : 'Loading'}
            </tbody>
          </table>
        </div>
        
        <div className="mt-6 flex items-center justify-between">
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage - 1
              setCurrentPage(page)
            }}
            disabled={!canGoPrev}
            className="rounded bg-gray-300 px-4 py-2">
            Previous
          </button>
          <span>
            Page <strong>{currentPage}</strong>
          </span>
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage + 1
              setCurrentPage(page)
            }}
            disabled={!canGoNext}
            className="rounded bg-gray-300 px-4 py-2"
          >
            Next
          </button>
        </div>

      </Card>
    </div>
  );
};

export default Page;
