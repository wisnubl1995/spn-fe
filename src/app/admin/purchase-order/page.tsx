'use client';

import DeliveryTable from 'components/admin/data-tables/DeliveryTable';
import PurchaseRequestTable from 'components/admin/data-tables/PurchaseRequestTable';
import Card from 'components/card';
import CardMenu from 'components/card/CardMenu';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import React, { useContext, useEffect, useState } from 'react';
import { MdCreate, MdOutlineDeleteForever, MdSearchOff } from 'react-icons/md';
import { formatDate } from 'utils/formatDate';
import { deliveryData } from 'variables/data-tables/deliveryData';
import logo from '/public/img/logo/spn.png';
import { FaFileExport, FaPrint, FaRightFromBracket } from 'react-icons/fa6';
import { buildQuery, formatNumber, formattedDate } from 'utils/helper';
import Papa from 'papaparse';
import OverlayLoading from 'components/loading/OverlayLoading';
import exportToExcel from 'utils/exportExcel';
import AuthContext from 'contexts/AuthContext';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AutocompleteInput from 'components/fields/AutoComplete';
import DefaultModal from 'components/modal/Default';
import { FaSearch } from 'react-icons/fa';
import CustomTooltip from 'components/tooltip/CustomTooltip';

type Props = {};

const Page = (props: Props) => {
  const router = useRouter();
  const [newPurchaseRequestData, setNewPurchaseRequestData] = useState<any>({
    purchaseRequestNo: '',
    shipName: '',
    department: '',
    category: '',
    requestedBy: 'wisnu',
    createdBy: 'wisnu',
    location: 'Jakarta',
    purchaseOrderItem: [],
  });
  const [items, setItems] = useState([{ id: 1 }]);
  const { sessionUser } = useContext(AuthContext);
  const [selectedUUID, setSelectedUUID] = useState('');
  const [selectedToko, setSelectedToko] = useState('');
  const [selectedInvoice, setSelectedInvoice] = useState('');
  const [addNew, setAddNew] = React.useState(false);
  const [activeTab, setActiveTab] = useState('Pending');
  const [showDetail, setShowDetail] = React.useState(false);
  const [showInvoice, setShowInvoice] = React.useState(false);
  const [selectedRow, setSelectedRow] = React.useState(null);
  const [selectedInvoiceRow, setSelectedInvoiceRow] = React.useState(null);
  const [selectedPoRow, setSelectedPoRow] = React.useState(null);
  const [currentPagePending, setCurrentPagePending] = useState(1);
  const [currentPageReady, setCurrentPageReady] = useState(1);
  const [showNotif, setShowNotif] = useState(false)
  const [notifMessage, setNotifMessage] = useState(null)
  const [showFilter, setShowFilter] = useState(false);
  const [filteredQuery, setFilteredQuery] = useState(null);

  const [po, setPo] = useState(false);

  const [bulk, setBulk] = useState(false);

  const [isChecked, setIsChecked] = useState(false); // State untuk menandai apakah ada item yang dicentang

  const handleCheckboxChange = (index) => {
    const updatedItems = [...selectedRow.purchaseOrderItem];
    updatedItems[index].isChecked = !updatedItems[index].isChecked;
    setSelectedRow({ ...selectedRow, purchaseOrderItem: updatedItems });

    setIsChecked(updatedItems.some((item) => item.isChecked));
  };

  const handleSelectUUID = (uuid) => {
    setSelectedUUID(uuid);
    // Mengambil objek yang sesuai dengan UUID yang dipilih
    const selectedData = purchaseOrderData.find((data) => data.uuid === uuid);
    console.log(selectedData);
    setSelectedRow(selectedData);
    // Tampilkan pop-up atau lakukan operasi lain sesuai kebutuhan
    setShowDetail(true);
  };
  const handleInvoiceUUID = (uuid) => {
    setSelectedInvoice(uuid);

    const selectedData = purchaseOrderReadyData.find(
      (data) => data.uuid === uuid,
    );
    setSelectedInvoiceRow(selectedData);
    setShowInvoice(true);
  };
  const handleDetailToko = (uuid) => {
    setSelectedToko(uuid);
    // Mengambil objek yang sesuai dengan UUID yang dipilih
    const selectedData = selectedRow.purchaseOrderItem.find(
      (data) => data.uuid === uuid,
    );
    console.log(selectedData);
    setSelectedPoRow(selectedData);
    // Tampilkan pop-up atau lakukan operasi lain sesuai kebutuhan
    setPo(true);
  };

  const [purchaseOrderData, setpurchaseOrderData] = useState<any>([]);
  const [purchaseOrderReadyData, setpurchaseOrderReadyData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(true);
  let isMounted = true;

  const fetchData = async () => {
    setIsLoading(true);
    const url = `${process.env.NEXT_PUBLIC_API}purchaseorder?page=${currentPagePending}&pageSize=10`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (isMounted) {
          setpurchaseOrderData(data.data);
        }else{
          setpurchaseOrderData([])
        }
      }else{
        setpurchaseOrderData([])
      }
      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchDataReady = async () => {
    const url = `${process.env.NEXT_PUBLIC_API}approvedpo?page=${currentPageReady}&pageSize=20`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (isMounted) {
          setpurchaseOrderReadyData(data.data);
        }else{
          setpurchaseOrderReadyData([])
        }
      }else{
        setpurchaseOrderReadyData([])
      }

      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const updatePurchaseRequest = (updatedRow) => {
    const updatedData = purchaseOrderData.map((item) => {
      if (item.uuid === updatedRow.uuid) {
        return {
          ...item,
          ...updatedRow,
          purchaseOrderItem: item.purchaseOrderItem,
        };
      }
      return item;
    });

    setpurchaseOrderData(updatedData);
    console.log(purchaseOrderData);
  };

  const handleCheckAll = () => {
    // Check if all items are already checked
    const allChecked = selectedRow.purchaseOrderItem.every(
      (item) => item.isChecked,
    );

    // Toggle check status based on current status
    const updatedItems = selectedRow.purchaseOrderItem.map((item) => ({
      ...item,
      isChecked: !allChecked, // Toggle the isChecked value
    }));

    setSelectedRow((prevSelectedRow) => ({
      ...prevSelectedRow,
      purchaseOrderItem: updatedItems,
    }));
  };

  const handleApproval = async (item) => {
    const { uuid, quantityApporvedBy, approvedBy, quantityApproved } = item;

    const payload = {
      id: uuid,
      quantityApproved,
      quantityApporvedBy: 'Wisnu',
    };

    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API}purchaseOrderItem/update`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            "account-access": sessionUser?.uuid
          },
          body: JSON.stringify(payload),
        },
      );

      if (response.ok) {
        console.log('Purchase item approved successfully!');
        fetchData();
      } else {
        console.error('Failed to approve purchase item');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const addItem = () => {
    const newItem = { id: items.length + 1 }; // Create a new item object
    setItems([...items, newItem]); // Add the new item to the items array
  };

  const removeItem = (idToRemove) => {
    const updatedItems = items.filter((item) => item.id !== idToRemove);
    setItems(updatedItems);
  };

  const handleSaveNewPurchaseRequest = async () => {
    const newPurchaseRequest = {
      ...newPurchaseRequestData,
    };

    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API}purchaseRequest`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            "account-access": sessionUser?.uuid
          },
          body: JSON.stringify(newPurchaseRequest),
        },
      );

      if (response.ok) {
        setpurchaseOrderData([...purchaseOrderData, newPurchaseRequest]);

        setNewPurchaseRequestData({
          purchaseRequestNo: '',
          shipName: '',
          department: '',
          category: '',
          requestedBy: 'Wisnu',
          createdBy: 'Wisnu',
          location: 'Jakarta',
          purchaseOrderItem: [{}],
        });

        setAddNew(false);
        window.location.reload();
      } else {
        console.error('Failed to save new purchase request');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };
  const clearTempData = () => {
    localStorage.removeItem('tempData');
  };

  const handleSelectChange = (e: any, item: any) => {
    // Get the selected value
    const selectedShop = e.target.value;

    // Retrieve existing data from local storage or initialize as an empty array
    const tempData = JSON.parse(localStorage.getItem('tempData')) || [];

    console.log(item);

    // Add the selected shop data to the tempData array
    tempData.push({
      idPo: item.poId,
      billingTo: selectedShop,
      project: 'engine',
      detailPoItems: [
        {
          id: item.uuid,
          description: item.description,
          quantity: item.quantity,
          unit: item.unit,
          unitPrice:
            selectedShop == item.shopOneName
              ? item.shopOneUnitPrice
              : selectedShop == item.shopTwoName
              ? item.shopTwoUnitPrice
              : selectedShop == item.shopThreeName
              ? item.shopThreeUnitPrice
              : null,
          amount:
            selectedShop == item.shopOneName
              ? String(item.shopOneUnitPrice * item.quantity)
              : selectedShop == item.shopTwoName
              ? String(item.shopTwoUnitPrice * item.quantity)
              : selectedShop == item.shopThreeName
              ? String(item.shopThreeUnitPrice * item.quantity)
              : null,
        },
      ],
    });

    // Store the updated data back to local storage
    localStorage.setItem('tempData', JSON.stringify(tempData));
  };

  const groupDataByBillingTo = (tempData) => {
    if (tempData.length === 0) return []; // Return empty array if tempData is empty

    const currentDate = new Date().toISOString(); // Get current date and time
    const orderNumber = 'PO-0023'; // Generate orderNumber
    const quotationNumber = 'Q-29912'; // Generate quotationNumber

    // Collect all detailPoItems into a single array
    const detailPoItems = tempData.flatMap((item) => item.detailPoItems);

    return [
      {
        idPo: tempData[0].idPo,
        orderDate: currentDate,
        orderNumber: orderNumber,
        quotationNumber: quotationNumber,
        project: tempData[0].project,
        billingTo: tempData[0].billingTo,
        detailPoItems: detailPoItems,
      },
    ];
  };

  const sendDataToServer = (groupedData, setShowDetail, clearTempData) => {
    const currentDate = new Date().toISOString(); // Get current date and time
    const orderNumber = 'PO-0023'; // Generate orderNumber
    const quotationNumber = 'Q-29912'; // Generate quotationNumber

    console.log(groupedData);
    fetch(`${process.env.NEXT_PUBLIC_API}purchaseorder/movepotoaprpo`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "account-access": sessionUser?.uuid
      },
      body: JSON.stringify(groupedData),
    })
      .then((response) => {
        if (response.ok) {
          console.log('Data sent successfully');
          setShowDetail(false);
          clearTempData();
        } else {
          console.error('Failed to send data');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  const handleApprove = (idPo, detailAprPoItems) => {
    const payload = {
      idPo: idPo,
      detailAprPoItems: detailAprPoItems.map((item) => ({ id: item.uuid })),
      approvedBy: sessionUser?.name,
    };

    fetch(`${process.env.NEXT_PUBLIC_API}approvedpo`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "account-access": sessionUser?.uuid
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (response.ok) {
          console.log('Data sent successfully');
          fetchDataReady();
        } else {
          console.error('Failed to send data');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  function printPDF() {
    // Mengambil konten HTML yang ingin diubah menjadi PDF
    const pdfContent = document.getElementById('pdfContent').innerHTML;

    // Membuat jendela pop-up untuk mencetak konten HTML
    const printWindow = window.open('', '_blank');

    // Menambahkan konten HTML ke jendela pop-up
    printWindow.document.open();
    printWindow.document.write(`
      <html>
        <head>
          <title>PDF</title>
        </head>
        <body>
          ${pdfContent}
        </body>
      </html>
    `);
    printWindow.document.close();

    // Mencetak konten HTML menjadi PDF
    printWindow.print();
  }

  const handleExport = () => {
    setIsLoading(true);
    const poPending = purchaseOrderData.map((item) => ({
      ...item,
      po_status: 'PENDING',
      items: item.purchaseOrderItem,
    }));
    const poReady = purchaseOrderReadyData.map((item) => ({
      ...item,
      po_status: 'PO READY',
      items: item.approvedPoItems,
    }));

    const mergedData = [...poPending, ...poReady];
    const tmpData = [];
    for (let i = 0; i < mergedData.length; i++) {
      const header = mergedData[i];
      for (let j = 0; j < header.length; j++) {
        const item = header[j];
        tmpData.push({
          PO_STATUS: header.po_status,
          PURCHASE_REQUEST_NO: header.purchaseRequestNo,
          QUOTATION_NO: header.quotationNumber,
          BILLING_TO: header.billingTo,
          DEPARTEMEN: header.department,
          NAMA_KAPAL: header.shipName,
          LOKASI: header.location,
          KATEGORI: header.category,
          ORDER_NUMBER: header.orderNumber,
          ORDER_DATE: header.orderDate,
          PROJECT: header.project,
          REQUEST_BY: header.createdBy ? header.createdBy : '-',
          TANGGAL_REQUEST: header.createdAt
            ? formattedDate(new Date(header.createdAt), 'YYYY-MM-DD')
            : header.createdAt,
          STATUS_PO: header.status,
          NAMA_BARANG: item.description,
          SATUAN: item.unit,
          HARGA_SATUAN: `Rp${formatNumber(parseInt(item.unitPrice))}`,
          QTY: item.unit,
          TOTAL_HARGA: `Rp${formatNumber(parseInt(item.amount))}`,
          STATUS_BARANG: item.status,
        });
      }
    }

    exportToExcel(tmpData, 'PURCHASE_ORDER', 'DATA');
    setIsLoading(false);
  };

  useEffect(() => {
    isMounted = true
    fetchData();

    return () => { isMounted = false }
  }, [currentPagePending]);

  useEffect(() => {
    isMounted = true
    fetchDataReady();
    return () => { isMounted = false }
  }, [currentPageReady]);

  const onSubmitModal = async (formData: any) => {
    setFilteredQuery(formData)
    fetchFiltered(formData)
    fetchFilteredPOReady(formData)
  }

  const fetchFiltered = async (queries: any) => {
    const url = buildQuery(`${process.env.NEXT_PUBLIC_API}purchaseorder/filter`, queries);
    if (url.includes('?')) {
      setIsLoading(true);
      try {
        const response = await fetch(url, {
          headers: {
            "account-access": sessionUser?.uuid
          }
        });
  
        if (!response.ok) {
          setIsLoading(false);
          throw new Error('Network response was not ok');
        }
  
        if (response.status === 200) {
          const data = await response.json();
          if (isMounted) {
            setpurchaseOrderData(data.data);
          }else{
            setpurchaseOrderData([])
            setNotifMessage(`Failed! ${data?.message ? data?.message : 'Internal Server Error'}`)
            setShowNotif(true)
          }
        } else {
          setpurchaseOrderData([])
        }
  
        setIsLoading(false); // Update isLoading state to false
      } catch (error) {
        setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
        setShowNotif(true)
        setIsLoading(false);
        console.error('There was a problem with the fetch operation:', error);
      }
    }
  }

  const fetchFilteredPOReady = async (queries: any) => {
    const url = buildQuery(`${process.env.NEXT_PUBLIC_API}approvedpo/filter`, queries);
    if (url.includes('?')) {
      setIsLoading(true);
      try {
        const response = await fetch(url, {
          headers: {
            "account-access": sessionUser?.uuid
          }
        });
  
        if (!response.ok) {
          setIsLoading(false);
          throw new Error('Network response was not ok');
        }
  
        if (response.status === 200) {
          const data = await response.json();
          if (isMounted) {
            setpurchaseOrderReadyData(data.data);
          }else{
            setpurchaseOrderReadyData([])
            setNotifMessage(`Failed! ${data?.message ? data?.message : 'Internal Server Error'}`)
            setShowNotif(true)
          }
        } else {
          setpurchaseOrderReadyData([])
        }
  
        setIsLoading(false); // Update isLoading state to false
      } catch (error) {
        setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
        setShowNotif(true)
        setIsLoading(false);
        console.error('There was a problem with the fetch operation:', error);
      }
    }
  }

  if (isLoading) {
    return <OverlayLoading />;
  }

  const canGoPrevPending = currentPagePending < 1
  const canGoNextPending = purchaseOrderData.length >= 10
  const canGoPrevReady = currentPageReady < 1
  const canGoNextReady = purchaseOrderReadyData.length >= 10

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="text-xl font-bold text-navy-700 dark:text-white">
            Purchase Order
          </div>
          <CardMenu
            hideAddBtn={true}
            showExportBtn={true}
            handleExport={handleExport}
          />
        </header>

        <div className="flex justify-between items-center my-2">
          <div>
            <button
              className={`mr-2 rounded-md px-4 py-2 focus:outline-none ${
                activeTab === 'Pending' ? 'bg-blue-500 text-white' : 'bg-gray-200'
              }`}
              onClick={() => {
                setActiveTab('Pending'), fetchData();
                setFilteredQuery(null)
              }}
            >
              Pending
            </button>
            <button
              className={`rounded-md px-4 py-2 focus:outline-none ${
                activeTab === 'PO Ready'
                  ? 'bg-blue-500 text-white'
                  : 'bg-gray-200'
              }`}
              onClick={() => {
                setActiveTab('PO Ready'), fetchDataReady();
                setFilteredQuery(null)
              }}
            >
              PO Ready
            </button>
          </div>
          <div className="flex items-center gap-2">
            <div className="flex cursor-pointer">
              <button
                onClick={() => {
                  setShowFilter(true)
                }}
                className="rounded px-3 py-1 text-white focus:outline-none bg-brand-600 hover:bg-brand-300"
              >
                <FaSearch className="inline-block h-4 w-4" /> Filter
              </button>
            </div>
          </div>
        </div>

        {filteredQuery && <div className="flex items-center my-1">
          <div className="mr-8 font-bold p-2 bg-gray-700 rounded text-white">
            FILTERED BY :
          </div>
          <div className="flex-1 flex justify-between items-center">
            {Object.keys(filteredQuery)?.filter((key) => filteredQuery[key] !== '').map((key) => (
              <div key={key}>
                <p className="font-semibold">{key.toUpperCase()} : </p>
                <p>{filteredQuery[key]}</p>
              </div>
            ))}
            <div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={handleExport}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-cyan-600 hover:bg-cyan-300"
                >
                  <FaFileExport className="inline-block h-4 w-4" /> Export
                </button>
              </div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={() => {
                    setFilteredQuery(null)
                    fetchData()
                  }}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-red-600 hover:bg-red-300"
                >
                  <MdSearchOff className="inline-block h-6 w-6" /> Clear
                </button>
              </div>
            </div>
          </div>
        </div>}

        <div className=" overflow-x-scroll ">
          {activeTab === 'Pending' && (
            <table className="w-full">
              <thead>
                <tr className="!border-px !border-gray-400">
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        PR No
                      </p>
                    </div>
                  </th>
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Billing To
                      </p>
                    </div>
                  </th>
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Deparment
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Ship
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Location
                      </p>
                    </div>
                  </th>
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Date Requested
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Action
                      </p>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {purchaseOrderData
                  ? purchaseOrderData
                      .filter((order) => order.status === 'Open')
                      .map((v: any, i: any) => (
                        <tr key={i}>
                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <p className="text-sm font-bold text-navy-700 dark:text-white">
                              {v.purchaseRequestNo}
                            </p>
                          </td>
                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <p className="text-sm font-bold text-navy-700 dark:text-white">
                              {v.billingTo}
                            </p>
                          </td>
                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <p className="text-sm font-bold text-navy-700 dark:text-white">
                              {v.department}
                            </p>
                          </td>

                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <p className="text-sm font-bold text-navy-700 dark:text-white">
                              {v.shipName}
                            </p>
                          </td>

                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <p className="text-sm font-bold text-navy-700 dark:text-white">
                              {v.location}
                            </p>
                          </td>
                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <p className="text-sm font-bold text-navy-700 dark:text-white">
                              {formatDate(v.createdAt)}
                            </p>
                          </td>

                          <td className="min-w-[150px] border-white/0 py-3  pr-4">
                            <div className="flex cursor-pointer gap-3">
                              <CustomTooltip text="Update Billing Info">
                              <p
                                onClick={() => {
                                  router.push(
                                    `/admin/purchase-order/edit/${v.uuid}`,
                                  );
                                }}
                                className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                              >
                                <MdCreate />
                              </p>
                              </CustomTooltip>
                            </div>
                          </td>
                        </tr>
                      ))
                  : 'Loading'}
              </tbody>
            </table>
          )}

          {activeTab === 'PO Ready' && (
            <table className="w-full">
              <thead>
                <tr className="!border-px !border-gray-400">
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        PR No
                      </p>
                    </div>
                  </th>
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Deparment
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Ship
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Status
                      </p>
                    </div>
                  </th>
                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Date Requested
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Invoice
                      </p>
                    </div>
                  </th>

                  <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                    <div className="items-center justify-between text-xs text-gray-200">
                      <p className="text-sm font-bold text-gray-600 dark:text-white">
                        Move to Inbound
                      </p>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {purchaseOrderReadyData
                  ? purchaseOrderReadyData.map((v: any, i: any) => (
                      <tr key={i}>
                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <p className="text-sm font-bold text-navy-700 dark:text-white">
                            {v.purchaseRequestNo}
                          </p>
                        </td>
                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <p className="text-sm font-bold text-navy-700 dark:text-white">
                            {v.department}
                          </p>
                        </td>

                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <p className="text-sm font-bold text-navy-700 dark:text-white">
                            {v.shipName}
                          </p>
                        </td>

                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <p className="text-sm font-bold text-navy-700 dark:text-white">
                            {v.status == 'Open' ? 'On Progress' : 'Closed'}
                          </p>
                        </td>
                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <p className="text-sm font-bold text-navy-700 dark:text-white">
                            {formatDate(v.createdAt)}
                          </p>
                        </td>

                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <button
                            onClick={() => {
                              router.push(
                                `/admin/purchase-order/preview/${v.uuid}`,
                              );
                            }}
                            className={`mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none`}
                          >
                            <FaPrint className="mr-2 inline-block" />
                            Invoice
                          </button>
                        </td>

                        <td className="min-w-[150px] border-white/0 py-3  pr-4">
                          <button
                            onClick={() =>
                              handleApprove(v.uuid, v.approvedPoItems)
                            }
                            disabled={v.status == 'Close' ? true : false}
                            className={`mt-4  rounded bg-cyan-500 px-4 py-2 text-white focus:outline-none
                            ${
                              v.status == 'Close'
                                ? 'bg-gray-500 hover:bg-gray-600'
                                : 'hover:bg-cyan-600'
                            }
                            `}
                              >
                            <FaRightFromBracket className="mr-2 inline-block" />
                            Move
                          </button>
                        </td>
                      </tr>
                    ))
                  : 'Loading'}
              </tbody>
            </table>
          )}
        </div>

        {activeTab === 'Pending' ? (
          <div className="mt-6 flex items-center justify-between">
            <button
              onClick={() => {
                let page = currentPagePending <= 0 ? 1 : currentPagePending - 1
                setCurrentPagePending(page)
              }}
              disabled={!canGoPrevPending}
              className="rounded bg-gray-300 px-4 py-2">
              Previous
            </button>
            <span>
              Page <strong>{currentPagePending}</strong>
            </span>
            <button
              onClick={() => {
                let page = currentPagePending <= 0 ? 1 : currentPagePending + 1
                setCurrentPagePending(page)
              }}
              disabled={!canGoNextPending}
              className="rounded bg-gray-300 px-4 py-2"
            >
              Next
            </button>
          </div>
        ) : (
          <div className="mt-6 flex items-center justify-between">
            <button
              onClick={() => {
                let page = currentPageReady <= 0 ? 1 : currentPageReady - 1
                setCurrentPageReady(page)
              }}
              disabled={!canGoPrevReady}
              className="rounded bg-gray-300 px-4 py-2">
              Previous
            </button>
            <span>
              Page <strong>{currentPageReady}</strong>
            </span>
            <button
              onClick={() => {
                let page = currentPageReady <= 0 ? 1 : currentPageReady + 1
                setCurrentPageReady(page)
              }}
              disabled={!canGoNextReady}
              className="rounded bg-gray-300 px-4 py-2"
            >
              Next
            </button>
          </div>
        )}

        {showInvoice && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[700px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">Invoice</h2>
                <button onClick={printPDF}>Print PDF</button>
                <button
                  onClick={() => {
                    setShowInvoice(false);
                    setSelectedInvoiceRow({});
                    clearTempData();
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>

              <div className="flex w-full flex-col gap-3 px-3" id="pdfContent">
                <div className="flex w-full items-center justify-between border-b-2">
                  <div className="w-3/12">
                    <Image src={logo} alt="" width={1000} height={1000} />
                  </div>
                  <div className="flex w-3/12 flex-col gap-2">Alamat SPN</div>
                </div>

                <div className="flex w-full justify-between">
                  <div className="flex w-4/12 flex-col gap-3">
                    <div>
                      <div>
                        <h3>To:</h3>
                      </div>
                      <div>Nama PT</div>
                    </div>

                    <div>
                      <div>
                        <h3>Address</h3>
                      </div>
                      <div>
                        <textarea
                          className="border-[1px] px-1"
                          name=""
                          id=""
                          cols={30}
                          rows={3}
                        ></textarea>
                      </div>
                    </div>
                  </div>
                  <div className="flex w-4/12 flex-col gap-3">
                    <div className="flex items-center justify-end gap-10">
                      <div>
                        <div>
                          <div>
                            <h3>Deparment:</h3>
                          </div>
                          <div>
                            <h3>Deparment</h3>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div>
                          <div>
                            <h3>Project:</h3>
                          </div>
                          <div>
                            <h3>Project</h3>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="flex items-center justify-end gap-10">
                      <div>
                        <div>
                          <div>
                            <h3>Quotation Number:</h3>
                          </div>
                          <div>
                            <h3>-</h3>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div>
                          <div>
                            <h3>Order Number:</h3>
                          </div>
                          <div>
                            <h3>PO66666</h3>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div>
                          <div>
                            <h3>Order Date:</h3>
                          </div>
                          <div>
                            <h3>06 April 2024</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="w-full">
                  <table className="w-full">
                    <thead>
                      <tr className="!border-px !border-gray-400">
                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Product Code
                            </p>
                          </div>
                        </th>
                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Description
                            </p>
                          </div>
                        </th>
                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Quantity
                            </p>
                          </div>
                        </th>
                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Unit Price
                            </p>
                          </div>
                        </th>
                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Discount
                            </p>
                          </div>
                        </th>

                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Amount
                            </p>
                          </div>
                        </th>
                        <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                          <div className="items-center justify-between text-xs text-gray-200">
                            <p className="text-sm font-bold text-gray-600 dark:text-white">
                              Tax
                            </p>
                          </div>
                        </th>
                      </tr>
                    </thead>
                    <tbody>"Loading"</tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        )}

        {showDetail && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[700px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">Edit Data</h2>
                <button
                  onClick={() => {
                    setShowDetail(false);
                    setSelectedRow({});
                    clearTempData();
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>

              <div className="pt-10">
                <table className="w-full">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Unit</td>
                      <td>Quantity</td>
                      <td>Pilih Toko</td>

                      <td>Image</td>
                      <td>Action</td>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedRow.purchaseOrderItem.map(
                      (item, index) =>
                        // Conditionally render the row only if the status is "Open"
                        item.status === 'Open' && (
                          <tr className="border-b-2" key={index}>
                            <td>{item.description}</td>
                            <td>{item.unit}</td>
                            <td>{item.quantity}</td>
                            <td>
                              <select
                                name={`shopSelect-${index}`}
                                id={`shopSelect-${index}`}
                                onChange={(e) => handleSelectChange(e, item)} // Pass the item to the handler
                              >
                                <option value="">Pilih Toko</option>
                                <option value={item.shopOneName}>
                                  {item.shopOneName}
                                </option>
                                <option value={item.shopTwoName}>
                                  {item.shopTwoName}
                                </option>
                                {item.shopThreeName && (
                                  <option value={item.shopThreeName}>
                                    {item.shopThreeName}
                                  </option>
                                )}
                              </select>
                            </td>
                            <td>
                              <Image
                                className="h-[100px] w-[100px] object-cover"
                                src={
                                  'https://nimsteel.co.id/wp-content/uploads/2023/05/Cara-Memilih-Besi-Beton-Berkualitas-300x267.jpg'
                                }
                                alt="image"
                                width={100}
                                height={100}
                              />
                            </td>

                            <td>
                              <button
                                onClick={() => {
                                  handleDetailToko(item.uuid);
                                }}
                                className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                              >
                                Detail Toko
                              </button>
                            </td>
                          </tr>
                        ),
                    )}
                  </tbody>
                </table>
                <button
                  onClick={() => {
                    const tempData = JSON.parse(
                      localStorage.getItem('tempData'),
                    );
                    const groupedData = groupDataByBillingTo(tempData);
                    sendDataToServer(groupedData, setShowDetail, clearTempData);
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        )}

        {po && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[500px] w-[800px] overflow-auto rounded bg-white p-4 shadow-md">
              {/* Isi dari pop-up comparison */}
              <div className="flex items-center justify-between">
                <h2>Detail Toko</h2>
                <button
                  onClick={() => {
                    setPo(false);
                  }} // Tombol untuk menutup pop-up
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>

              <div className="flex items-center justify-center gap-4">
                <div className="flex flex-col items-center justify-center gap-2">
                  <div>
                    <h3>{selectedPoRow.shopOneName}</h3>
                  </div>
                  <div>Unit Price = Rp. {selectedPoRow.shopOneUnitPrice}</div>
                  <div>Total Price = Rp. {selectedPoRow.shopOneTotalPrice}</div>
                  <div>Remarks = {selectedPoRow.shopOneRemarks}</div>
                  <div>PPN = 11%</div>

                  <div>
                    GRAND TOTAL = Rp.{' '}
                    {selectedPoRow.shopOneTotalPrice * (11 / 100) +
                      Number(selectedPoRow.shopOneTotalPrice)}
                  </div>
                </div>
                <div className="flex flex-col items-center justify-center gap-2">
                  <div>
                    <h3>{selectedPoRow.shopTwoName}</h3>
                  </div>
                  <div>Unit Price = Rp. {selectedPoRow.shopTwoUnitPrice}</div>
                  <div>Total Price = Rp. {selectedPoRow.shopTwoTotalPrice}</div>
                  <div>Remarks = info</div>
                  <div>PPN = 11%</div>

                  <div>
                    GRAND TOTAL = Rp.{' '}
                    {selectedPoRow.shopTwoTotalPrice * (11 / 100) +
                      Number(selectedPoRow.shopTwoTotalPrice)}
                  </div>
                </div>
                {selectedPoRow.shopThreeName ? (
                  <div className="flex flex-col items-center justify-center gap-2">
                    <div>
                      <h3>{selectedPoRow.shopThreeName}</h3>
                    </div>
                    <div>
                      Unit Price = Rp. {selectedPoRow.shopThreeUnitPrice}
                    </div>
                    <div>
                      Total Price = Rp. {selectedPoRow.shopThreeTotalPrice}
                    </div>
                    <div>Remarks = info</div>
                    <div>PPN = 11%</div>

                    <div>
                      GRAND TOTAL = Rp.{' '}
                      {selectedPoRow.shopThreeTotalPrice * (11 / 100) +
                        Number(selectedPoRow.shopThreeTotalPrice)}
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        )}

        {addNew && (
          <div className="bg-black fixed inset-0 flex items-center justify-center bg-opacity-50">
            <div className="h-[600px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">New Purchase Request</h2>
                <button
                  onClick={() => {
                    setAddNew(false);
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>

              <div className="flex w-full flex-col gap-2 p-3">
                <div className="flex items-center justify-start gap-3">
                  <div className="flex flex-col">
                    <label className="ml-2">PR No</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          purchaseRequestNo: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Ship</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      value={newPurchaseRequestData.ship_name}
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          shipName: e.target.value,
                        })
                      }
                    />
                  </div>
                </div>
                <div className="flex items-center justify-start gap-3">
                  <div className="flex flex-col">
                    <label className="ml-2">Department</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      value={newPurchaseRequestData.department}
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          department: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Category</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      value={newPurchaseRequestData.category}
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          category: e.target.value,
                        })
                      }
                    />
                  </div>
                </div>
              </div>

              <div className="flex w-full flex-col gap-2 overflow-x-auto p-3">
                <div className=" flex">
                  <button
                    className={`mr-2 rounded-md px-4 py-2 focus:outline-none ${
                      bulk === false ? 'bg-blue-500 text-white' : 'bg-gray-200'
                    }`}
                    onClick={() => setBulk(false)}
                  >
                    Manual
                  </button>
                  <button
                    className={`rounded-md px-4 py-2 focus:outline-none ${
                      bulk === true ? 'bg-blue-500 text-white' : 'bg-gray-200'
                    }`}
                    onClick={() => setBulk(true)}
                  >
                    Bulk
                  </button>
                </div>
                <h3 className="text-[20px] font-bold">Items</h3>
                {items.map((item, index) => (
                  <div className="flex items-center gap-1" key={item.id}>
                    <div className="flex flex-col">
                      <label className="ml-2">Item Name</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseOrderItem,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            description: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseOrderItem: newItems, // Update the purchaseOrderItem array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Unit</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseOrderItem,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            unit: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseOrderItem: newItems, // Update the purchaseOrderItem array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Quantity</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseOrderItem,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            quantityAsked: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseOrderItem: newItems, // Update the purchaseOrderItem array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">SAP Code</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseOrderItem,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            sapCode: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseOrderItem: newItems, // Update the purchaseOrderItem array with the modified item
                          });
                        }}
                      />
                    </div>

                    <div className="flex flex-col">
                      <label className="ml-2">Additional Info</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseOrderItem,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            additionalInfo: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseOrderItem: newItems, // Update the purchaseOrderItem array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Image</label>

                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="file"
                        placeholder="Input Your Text Here"
                      />
                    </div>
                    <button
                      className="mt-4 rounded bg-blue-500 px-4 py-1 text-white hover:bg-blue-600 focus:outline-none"
                      onClick={() => removeItem(item.id)}
                    >
                      -
                    </button>
                  </div>
                ))}
                <div className="flex gap-2">
                  <button
                    className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                    onClick={addItem}
                  >
                    +
                  </button>
                </div>
              </div>
              <button
                onClick={handleSaveNewPurchaseRequest}
                className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
              >
                Add New
              </button>
            </div>
          </div>
        )}

        {showNotif && 
          <NotificationOverlay 
              isShow={showNotif}
              title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
              message={notifMessage}
              handleClose={() => setShowNotif(false)}
              color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}

        {showFilter && (
          <ModalFilter
            title={`Filter Data Purchase Request`}
            onClose={() => setShowFilter(false)}
            onSubmit={onSubmitModal}/>)}
      </Card>
    </div>
  );
};

const ModalFilter = ({ onClose, onSubmit, title }: any) => {
  const [formData, setFormData] = useState({
    department: '',
    ship: '',
    startDate: '',
    endDate: '',
  })
  return (
    <DefaultModal title={title} onClose={onClose}>
      <div className="mt-6 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 py-2">
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Start Date</p>
          <input
              type="date"
              name="startDate"
              id="startDate"
              placeholder="Input Start Date"
              value={formData.startDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  startDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">End Date</p>
          <input
              type="date"
              name="endDate"
              id="endDate"
              placeholder="Input End Date"
              value={formData.endDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  endDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Department</p>
          <AutocompleteInput
              placeholder="Search Department"
              apiUrl="department/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  department: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Ship</p>
          <AutocompleteInput
              placeholder="Search Ship"
              apiUrl="ship/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  ship: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit(formData);
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </DefaultModal>
  );
};

export default Page;
