'use client'
import Card from 'components/card'
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation'
import React, { useContext, useEffect, useState } from 'react'
import { FaBox } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import { formatNumber, formattedDate, validateForm } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
    const router = useRouter()
    const params = useParams()
    const uuid = params?.id

    const { sessionUser } = useContext(AuthContext);
    const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const [isLoading, setIsLoading] = useState(false)
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [totalAmount, setTotalAmount] = useState(0)
    const [currentAddress, setCurrentAddress] = useState('')
    const [formData, setFormData] = useState({
        billingAddress: '',
        quotationNumber: '',
        project: '',
        orderNumber: '',
    })
    const [details, setDetails] = useState(null)

    const handleSubmit = async () => {
        const optionalFields = { quotationNumber: true }
        if (validateForm(formData, optionalFields)) {
            const payload: any = {
                idPo: uuid,
                address: formData.billingAddress,
                quotationNumber: formData.quotationNumber,
                project: formData.project,
                orderNumber: formData?.orderNumber,
                orderDate: formattedDate(new Date(), "dd/MM/yyy"),
                createdBy: sessionUser?.name,
                detailPoItems: details?.purchaseOrderItem?.map((item: any) => (
                    {
                        id: item.uuid,
                        description: item.description,
                        quantity: item.quantity,
                        unit: item.unit,
                        unitPrice: item.unitPrice,
                        amount: item.amount
                    }
                ))
            }
            // Form is valid, submit data
            setIsLoading(true)
            try {
                const resp = await sendRequestApi(`purchaseorder/movepotoaprpo`, 'POST', JSON.stringify(payload), headersReq)
                
                await sendRequestApi(`store/update`, 'PATCH', JSON.stringify({ name: details?.billingTo, address: payload.address }), headersReq)
                
                setNotifMessage(`${resp?.message}. Will be redirect to Purchase Order page in 3 seconds`)
                setShowNotif(true)
                setIsLoading(false)

                setTimeout(() => {
                    router.back()
                }, 3000);
            } catch (error) {
                setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
                setShowNotif(true)
                setIsLoading(false)
                console.log(`Failed submit purchase order!`, error?.message)
            }
        } else {
            setNotifMessage('Failed! Please fill out all required fields (Billing Address, Order Number and Project).')
            setShowNotif(true)
        }
    }

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`purchaseorder/${uuid}`, 'GET', null, headersReq)
            if (resp.data) {
                setDetails(resp.data)
                if (resp.data?.purchaseOrderItem) {
                    let totalAmount = 0
                    for (let i = 0; i < resp.data.purchaseOrderItem.length; i++) {
                        const element = resp.data.purchaseOrderItem[i];
                        totalAmount += parseInt(element.amount)
                    }
                    setTotalAmount(totalAmount)
                }
                fetchDetailsStore(resp.data)
            }else{
                setNotifMessage(`Failed! ${resp?.message}`)
                setShowNotif(true)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.log(`failed fetching details. error: ${error?.message}`)
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [uuid])

    const fetchDetailsStore = async (data: any) => {
        try {
            const resp = await sendRequestApi(`store/search?name=${data.billingTo}`, 'GET', null, headersReq)
            if (resp?.data?.length > 0) {
                setFormData({
                    billingAddress: resp.data[0].address,
                    quotationNumber: data?.quotationNumber ? data?.quotationNumber : '',
                    project: data?.shipName ? data?.shipName : '',
                    orderNumber: data?.orderNumber ? data?.orderNumber : '',
                })
                setCurrentAddress(resp.data[0].address)
            }
        } catch (error) {
            console.log(`Failed fetch details stores`)
        }
    }

    if (isLoading) 
        return <OverlayLoading />

    return (
        <div className="mt-10">
            <Card extra={'w-full pb-10 p-4 h-full'}>
                <header className="relative flex items-center justify-between">
                    <div className="py-2 text-xl font-bold text-navy-700 dark:text-white">
                        Form Purchase Order
                    </div>
                </header>
                <div className="mt-6 border-b border-t border-gray-900/10 pb-12">
                    <div className="my-2">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-lg font-semibold leading-7 text-gray-900">Purchase Order Information</h3>
                            <p className="mt-1 max-w-2xl text-base leading-6 text-gray-500">Purchase Order details and list items.</p>
                        </div>
                        <div className="mt-6 border-t border-gray-100">
                            <dl className="divide-y divide-gray-100 grid grid-cols-3 gap-x-6">
                                <div className="col-span-1">
                                    <div className="px-4 py-6 grid grid-cols-1 gap-x-4 sm:px-0">
                                        <dt className="text-lg font-medium leading-6 text-gray-900">Billing To</dt>
                                        <dd className="mt-1 text-md leading-6 text-gray-700">{details?.billingTo}</dd>
                                    </div>
                                    <div className="px-4 grid grid-cols-1 gap-x-4 sm:px-0">
                                        <dt className="text-lg font-medium leading-6 text-gray-900">
                                            Billing Address <span className="text-red-500">*</span>
                                        </dt>
                                        <dd className="mt-1 text-md leading-6 text-gray-700">
                                            {currentAddress && currentAddress != '' ? 
                                                formData?.billingAddress : 
                                                <textarea
                                                    name="billingAddress"
                                                    id="billingAddress"
                                                    placeholder="Billing Address"
                                                    rows={3}
                                                    value={formData?.billingAddress}
                                                    onChange={(evt) => {
                                                        setFormData({
                                                            ...formData,
                                                            billingAddress: evt.target.value
                                                        })
                                                    }}
                                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                            >{formData?.billingAddress}</textarea>}   
                                        </dd>
                                    </div>
                                </div>
                                <div className="col-span-1 flex flex-col justify-center">
                                    <dt className="text-lg font-medium leading-6 text-gray-900">
                                        Quotation Number
                                    </dt>
                                    <dd className="mt-1 text-md leading-6 text-gray-700">
                                        <input
                                            type="text"
                                            name="quotationNumber"
                                            value={formData?.quotationNumber}
                                            id="quotationNumber"
                                            onChange={(evt) => {
                                                setFormData({
                                                    ...formData,
                                                    quotationNumber: evt.target.value
                                                })
                                            }}
                                            placeholder="Quotation Number"
                                            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                        />
                                    </dd>
                                </div>
                                <div className="col-span-1">
                                    <div className="grid grid-cols-2 gap-x-4">
                                        <div className="grid grid-cols-2 col-span-2 gap-x-2">
                                            <div className="px-4 py-6 grid grid-cols-1 gap-x-4 sm:px-0">
                                                <dt className="text-lg font-medium leading-6 text-gray-900">
                                                    Department
                                                </dt>
                                                <dd className="mt-1 text-md leading-6 text-gray-700">{details?.department}</dd>
                                            </div>
                                            <div className="px-4 py-6 grid grid-cols-1 gap-x-4 sm:px-0">
                                                <dt className="text-lg font-medium leading-6 text-gray-900">
                                                    Project <span className="text-red-500">*</span>
                                                </dt>
                                                <dd className="mt-1 text-md leading-6 text-gray-700">
                                                    <input
                                                        type="text"
                                                        name="project"
                                                        value={formData?.project}
                                                        id="project"
                                                        onChange={(evt) => {
                                                            setFormData({
                                                                ...formData,
                                                                project: evt.target.value
                                                            })
                                                        }}
                                                        placeholder="Project"
                                                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                                    />
                                                </dd>
                                            </div>
                                        </div>
                                        <div className="grid grid-cols-2 col-span-2 gap-x-2">
                                            <div className="px-4 py-6 grid grid-cols-1 gap-x-4 sm:px-0">
                                                <dt className="text-lg font-medium leading-6 text-gray-900">
                                                    Order Number <span className="text-red-500">*</span>
                                                </dt>
                                                <dd className="mt-1 text-md leading-6 text-gray-700">
                                                    <input
                                                        type="text"
                                                        name="orderNumber"
                                                        value={formData.orderNumber}
                                                        id="orderNumber"
                                                        onChange={(evt) => {
                                                            setFormData({
                                                                ...formData,
                                                                orderNumber: evt.target.value
                                                            })
                                                        }}
                                                        placeholder="Order Number"
                                                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                                    />
                                                </dd>
                                            </div>
                                            <div className="px-4 py-6 grid grid-cols-1 gap-x-4 sm:px-0">
                                                <dt className="text-lg font-medium leading-6 text-gray-900">
                                                    Order Date
                                                </dt>
                                                <dd className="mt-1 text-md leading-6 text-gray-700">
                                                    {formattedDate(new Date(), "dd/MM/yyy")}
                                                </dd>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </dl>
                        </div>
                    </div>
                    <hr className='my-4' />
                    <div className="border-b-2 py-4 flex flex-row justify-between items-center">
                        <h2 className="text-2xl font-semibold leading-7 text-gray-900">List Items</h2>
                    </div>
                    <div className='mt-6'>
                        <ul role="list" className="grid grid-cols-3 gap-3">
                        {details?.purchaseOrderItem?.map((item:any, index: number) => (
                            <li key={`item-${index}`} className="flex justify-between gap-4 py-5 px-6 col-span-1 border-2 rounded-md">
                                <div className="gap-x-4">
                                    <div className="flex items-center">
                                        <FaBox className="h-12 w-12 flex-none text-[#A07558]" />
                                        <p className="flex-1 ml-2 text-lg text-gray-900 font-semibold leading-6">{item.description}</p>
                                    </div>
                                    <div className="flex justify-between mt-2">
                                        <div>
                                            <p className="mt-4 text-base text-gray-700 leading-5">
                                                Unit Price : <strong className="text-gray-900">Rp{formatNumber(parseInt(item.unitPrice)) || 0}</strong>
                                            </p>
                                            <p className="mt-1 text-gray-700 truncate text-lg leading-5">
                                                Quantity : <strong className="text-gray-900">{item.quantity}</strong> {item.unit}
                                            </p>
                                            
                                        </div>
                                        <p className="mt-1 text-base text-gray-700 leading-5 self-end">
                                            Total Price : <strong className="text-gray-900">Rp{formatNumber(parseInt(item.amount)) || 0}</strong>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        ))}
                        </ul>
                    </div>
                    <div className="border-t-2 mt-6 py-4 flex flex-row justify-between items-center">
                        <h2 className="text-xl font-semibold leading-7 text-gray-900">Total Amount</h2>
                        <p className="text-xl font-semibold leading-7 text-gray-900">Rp{formatNumber(totalAmount)}</p>
                    </div>
                </div>
                <div className="mt-6 flex items-center justify-end gap-x-6">
                    <button
                        type="button"
                        onClick={() => router.back()}
                        className="rounded-md bg-gray-600 px-4 py-2 text-md text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"> Cancel
                    </button>
                    <button
                        type="button"
                        onClick={handleSubmit}
                        className="rounded-md bg-green-600 px-4 py-2 text-md text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"> Submit
                    </button>
                </div>
            </Card>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

export default Page