'use client'
import Card from 'components/card'
import Image from 'next/image';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation'
import React, { useContext, useEffect, useState } from 'react'
import { FaBox, FaPrint } from 'react-icons/fa6';
import { sendRequestApi } from 'utils/api/fetchData';
import { formatNumber, formattedDate, validateForm } from 'utils/helper';
import logo from '/public/img/logo/spn.png';
import { IoIosArrowRoundBack } from 'react-icons/io';

type Props = {};

const Page = (props: Props) => {
    const router = useRouter()
    const params = useParams()
    const uuid = params?.id

    const { sessionUser } = useContext(AuthContext);
    const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const printRef = React.createRef<HTMLDivElement>();

    const [isLoading, setIsLoading] = useState(false)
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [details, setDetails] = useState(null)
    const [subTotal, setSubtotal] = useState(0)
    const [totalTax, setTotalTax] = useState<number>(0)
    const [totalAmount, setTotalAmount] = useState<number>(0)
    const [purchasedBy, setPurchasedBy] = useState('-')

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`approvedpo/${uuid}`, 'GET', null, headersReq)
            if (resp.data) {
                setDetails(resp.data)
                setPurchasedBy(resp.data.createdBy)
                let sub = 0
                let tax = 0
                for (let i = 0; i < resp.data.approvedPoItems.length; i++) {
                    const item = resp.data.approvedPoItems[i];
                    sub += parseInt(item.amount)
                    tax += item.tax ? parseInt(item.tax) : 0
                }

                setSubtotal(sub)
                setTotalTax(tax)
                setTotalAmount(sub + tax)
            }else{
                setNotifMessage(`Failed! ${resp?.message}`)
                setShowNotif(true)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.log(`failed fetching details. error: ${error?.message}`)
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [uuid])

    const handlePrintClick = () => {
        if (!printRef.current) {
          console.error('Printing failed: Ref is not attached to a valid element.');
          return;
        }

        // var css = '@page { size: landscape; }',
        //     head = document.head || document.getElementsByTagName('head')[0],
        //     style:any = document.createElement('style');

        // style.type = 'text/css';
        // style.media = 'print';

        // if (style.styleSheet){
        //     style.styleSheet.cssText = css;
        // } else {
        //     style.appendChild(document.createTextNode(css));
        // }

        // head.appendChild(style);
    
        const printableContent = printRef.current.cloneNode(true) as HTMLElement;
        const originalContent = document.body.innerHTML;
    
        const otherElements = document.body.querySelectorAll('*:not(.print-container)');
        otherElements.forEach((element) => element.classList.add('hidden'));
    
        document.body.innerHTML = printableContent.outerHTML;
        window.print();
    
        document.body.innerHTML = originalContent;
        otherElements.forEach((element) => element.classList.remove('hidden'));
        window.location.reload()
    };

    if (isLoading) 
        return <OverlayLoading />

    return (
        <div className="mt-10">
            <Card extra={'w-full pb-10 p-4 h-full'}>
                <header className="relative flex items-center justify-between">
                    <div className="py-2 text-xl font-bold text-navy-700 dark:text-white">
                        Print Purchase Order
                    </div>
                    <div>
                        <button
                            onClick={() => {
                                router.push('/admin/purchase-order')
                            }}
                            className="mt-4 rounded bg-gray-500 px-4 py-2 text-white hover:bg-gray-600 focus:outline-none mr-2">
                            <IoIosArrowRoundBack className="w-6 h-6 inline-block" /> Cancel
                        </button>
                        <button
                            onClick={handlePrintClick}
                            className="mt-4 rounded bg-brand-500 px-4 py-2 text-white hover:bg-brand-600 focus:outline-none ">
                            <FaPrint className="inline-block" /> Print
                        </button>
                    </div>
                </header>
                <div className="my-6 border-b border-t border-gray-900/10 py-6">
                    <div className="print-container h-min-[700px] w-min-[1200px] overflow-auto rounded bg-white p-4" ref={printRef}>
                        <div className="flex w-full flex-col gap-3 px-3" id="pdfContent">
                            <div className="flex w-full items-center justify-between border-b-2 pb-6 mb-4">
                                <div className="w-3/12">
                                    <img src="/img/logo/spn.png" alt="Logo SPN" width={1000} height={1000}/>
                                </div>
                                <div className="flex w-6/12 flex-col">
                                    <p className="text-[#57B5E2] font-bold">
                                        PT. SOLUSI PELAYARAN NUSANTARA
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        Jl. Kembang Kerep No.4, RT.4/RW.2, Meruya Utara, Kec. Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11620
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        021-38777005
                                    </p>
                                    <p className="text-[#57B5E2] text-sm">
                                        solusipelayarannusantara@gmail.com
                                    </p>
                                </div>
                            </div>

                            <div className="flex w-full justify-between">
                                <div className="flex w-4/12 flex-col gap-3">
                                    <div>
                                        <div>
                                            <h3 className="text-[#57B5E2]">To:</h3>
                                        </div>
                                        <p className="font-bold text-base">{details?.billingTo}</p>
                                    </div>
                                    <div>
                                        <div>
                                            <h3 className="text-[#57B5E2]">Address</h3>
                                        </div>
                                        <div>
                                            <p className="font-bold text-base">{details?.address}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex w-6/12 flex-col gap-3">
                                    <div className="flex items-center justify-end gap-10">
                                        <div>
                                            <div>
                                                <h3 className="text-[#57B5E2]">Deparment:</h3>
                                            </div>
                                            <div>
                                                <p className="font-bold text-base">{details?.department}</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <h3 className="text-[#57B5E2]">Project:</h3>
                                            </div>
                                            <div>
                                                <p className="font-bold text-base">{details?.project}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex items-center justify-end gap-10">
                                        <div>
                                            <div>
                                                <h3 className="text-[#57B5E2]">Quotation Number:</h3>
                                            </div>
                                            <div>
                                                <p className="font-bold text-base">{details?.quotationNumber ? details?.quotationNumber : '-'}</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <h3 className="text-[#57B5E2]">Order Number:</h3>
                                            </div>
                                            <div>
                                                <p className="font-bold text-base">{details?.orderNumber}</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <h3 className="text-[#57B5E2]">Order Date:</h3>
                                            </div>
                                            <div>
                                                <p className="font-bold text-base">{details?.orderDate}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="w-full mt-4">
                                <table className="w-full">
                                    <thead>
                                        <tr className="!border-px !border-gray-400">
                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Product Code
                                                    </p>
                                                </div>
                                            </th>
                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Description
                                                    </p>
                                                </div>
                                            </th>
                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Quantity
                                                    </p>
                                                </div>
                                            </th>
                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Unit Price
                                                    </p>
                                                </div>
                                            </th>
                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Discount
                                                    </p>
                                                </div>
                                            </th>

                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Amount
                                                    </p>
                                                </div>
                                            </th>
                                            <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                                                <div className="items-center justify-between text-xs text-gray-200">
                                                    <p className="text-base font-bold text-gray-600 dark:text-white">
                                                    Tax
                                                    </p>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {details?.approvedPoItems?.map((item: any, idx: number) => (
                                            <tr key={idx} className="border-b border-gray-200">
                                                <td className="py-6 text-base font-semibold">PRD-{item.uuid}</td>
                                                <td className="py-6 text-base font-semibold">{item.description}</td>
                                                <td className="py-6 text-base font-semibold">{item.quantity}</td>
                                                <td className="py-6 text-base font-semibold">Rp{formatNumber(item.unitPrice)}</td>
                                                <td className="py-6 text-base font-semibold">{item?.discount ? item?.discount : '-'}</td>
                                                <td className="py-6 text-base font-semibold">Rp{formatNumber(parseInt(item.quantity) * parseInt(item.unitPrice))}</td>
                                                <td className="py-6 text-base font-semibold">{item?.tax ? item?.tax : '-'}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>

                            <div className="flex justify-between">
                                <div></div>
                                <div>
                                    <div className="grid grid-cols-2 mb-2 border-b py-3">
                                        <div className="mr-4 col-span-1 text-base font-bold text-gray-600 dark:text-white">
                                            <p>Subtotal</p>
                                        </div>
                                        <div className="col-span-1 text-base font-semibold">Rp{formatNumber(subTotal)}</div>
                                    </div>
                                    <div className="grid grid-cols-2 mb-2 border-b py-3">
                                        <div className="mr-4 col-span-1 text-base font-bold text-gray-600 dark:text-white">
                                            <p>Final Discount</p>
                                        </div>
                                        <div className="col-span-1 text-base font-semibold">-</div>
                                    </div>
                                    <div className="grid grid-cols-2 mb-2 border-b py-3">
                                        <div className="mr-4 col-span-1 text-base font-bold text-gray-600 dark:text-white">
                                            <p>Total Tax</p>
                                        </div>
                                        <div className="col-span-1 text-base font-semibold">Rp{formatNumber(totalTax)}</div>
                                    </div>
                                    <div className="grid grid-cols-2 mb-2 border-b py-3">
                                        <div className="mr-4 col-span-1 text-base font-bold text-gray-600 dark:text-white">
                                            <p>Others</p>
                                        </div>
                                        <div className="col-span-1 text-base font-semibold">-</div>
                                    </div>
                                    <div className="grid grid-cols-2 mb-2 border-b py-3">
                                        <div className="mr-4 col-span-1 text-base font-bold text-gray-600 dark:text-white">
                                            <p>Total Amount</p>
                                        </div>
                                        <div className="col-span-1 text-base font-semibold">Rp{formatNumber(totalAmount)}</div>
                                    </div>
                                </div>
                            </div>

                            <div className="my-2 ml-6">
                                <p className="text-base font-bold text-gray-600">Purchased By:</p>
                                <p className="mt-32 text-base font-semibold">{purchasedBy}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Card>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

export default Page