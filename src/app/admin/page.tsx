'use client';

import AuthContext from 'contexts/AuthContext';
import { redirect } from 'next/navigation';
import { useContext } from 'react';
export default function Home({}) {
  const { sessionUser } = useContext(AuthContext);

  if (sessionUser?.uuid) {
    redirect('/admin/home');
  } else {
    redirect('/auth/sign-in');
  }
}
