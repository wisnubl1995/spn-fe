'use client';
import DeliveryCard from 'components/card/DeliveryCard';
import { useContext, useEffect, useState } from 'react';
import AuthContext from 'contexts/AuthContext';
import { redirect } from 'next/navigation';
import OverlayLoading from 'components/loading/OverlayLoading';

const Dashboard = () => {
  const { sessionUser } = useContext(AuthContext);
  const [realData, setRealData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  let isMounted = true;

  const fetchData = async () => {
    setIsLoading(true)
    const url = `${process.env.NEXT_PUBLIC_API}outbound/getdelivery?page=${currentPage}&pageSize=10`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (isMounted) {
          setRealData(data.data);
        }
      }
      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      setIsLoading(false);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  // const filteredData = deliveryData
  //   .filter((data) => data.status === selectedStatus)
  //   .sort(
  //     (a, b) =>
  //       new Date(b.dateCreated).getTime() - new Date(a.dateCreated).getTime(),
  //   );

  const canGoPrev = currentPage < 1
  const canGoNext = realData.length >= 10

	useEffect(() => {
		fetchData();
		if (!sessionUser)
		  redirect('/auth/sign-in');
	}, [sessionUser, currentPage]);

	if (!sessionUser)
		redirect('/auth/sign-in');

	if (isLoading)
		return <OverlayLoading />

  return (
    <div>
      {/* Card widget 

      <div className="mt-3 grid grid-cols-1 gap-5 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-3 3xl:grid-cols-6">
        <Widget
          icon={<MdBarChart className="h-7 w-7" />}
          title={'Earnings'}
          subtitle={'$340.5'}
        />
        <Widget
          icon={<IoDocuments className="h-6 w-6" />}
          title={'Spend this month'}
          subtitle={'$642.39'}
        />
        <Widget
          icon={<MdBarChart className="h-7 w-7" />}
          title={'Sales'}
          subtitle={'$574.34'}
        />
        <Widget
          icon={<MdDashboard className="h-6 w-6" />}
          title={'Your Balance'}
          subtitle={'$1,000'}
        />
        <Widget
          icon={<MdBarChart className="h-7 w-7" />}
          title={'New Tasks'}
          subtitle={'145'}
        />
        <Widget
          icon={<IoMdHome className="h-6 w-6" />}
          title={'Total Projects'}
          subtitle={'$2433'}
        />
      </div>

      <div className="mt-5 grid grid-cols-1 gap-5 md:grid-cols-2">
        <TotalSpent />
        <WeeklyRevenue />
      </div>
      */}
      <div className="mt-6 flex justify-between items-center">
        <h1 className="font-bold text-navy-700">Delivery</h1>
        <div className="flex items-center justify-between gap-2">
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage - 1
              setCurrentPage(page)
            }}
            disabled={!canGoPrev}
            className={`rounded bg-gray-300 px-4 py-2 ${canGoPrev ? `hover:bg-gray-500` : ''}`}>
            Previous
          </button>
          <span>
            Page <strong>{currentPage}</strong>
          </span>
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage + 1
              setCurrentPage(page)
            }}
            disabled={!canGoNext}
            className={`rounded bg-gray-300 px-4 py-2 ${canGoNext ? `hover:bg-gray-500` : ''}`}
          >
            Next
          </button>
        </div>
      </div>
      <div className="mb-4 flex flex-col gap-3">
        {realData
          && realData.map((e, i: number) => (
              <div key={i}>
                <DeliveryCard
                  uuid={e.uuid}
                  status={e.status || 'On-Going'}
                  ship={e.shipName}
                  source={e.location}
                  destination={e.deliverTo}
                  date={e.dateCreated || '06 April 2024'}
                  arrived={e.dateArrived || null}
                  via={e.via}
                  packages={e.outboundItems}
                  noResi={e.noResi}
                />
              </div>
            ))}
      </div>

      {/* Charts 
        <PieChartCard />
      */}

      {/* Tables & Charts
       */}

      {/* Check Table */}

      {/* Traffic chart & Pie Chart

<div className="grid grid-cols-1 gap-5 rounded-[20px] md:grid-cols-2">
<DailyTraffic />
</div>
*/}

      {/* Complex Table , Task & Calendar 

<ComplexTable tableData={tableDataComplex} />
*/}

      {/* Task chart & Calendar 

<div className="grid grid-cols-1 gap-5 rounded-[20px] md:grid-cols-2">
<div className="grid grid-cols-2 rounded-[20px] lg:grid-cols-1">
<MiniCalendar />
</div>
</div>
      <div className="mt-5 grid grid-cols-1 gap-5 xl:grid-cols-2"></div>
    */}
    </div>
  );
};

export default Dashboard;
