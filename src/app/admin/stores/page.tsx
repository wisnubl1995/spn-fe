'use client';
import StoresTable from 'components/admin/data-tables/StoresTable';
import OverlayLoading from 'components/loading/OverlayLoading';
import Default from 'components/modal/Default';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import React, { useContext, useEffect, useState } from 'react';

type Props = {};

const Page = (props: Props) => {
	const { sessionUser } = useContext(AuthContext)
	const [listData, setListData] = useState<any>();
	const [isLoading, setIsLoading] = useState(true);
	const [showModalForm, setShowModalForm] = useState(false);
    const [editedData, setEditedData] = useState<any>(null)
	const [notifSuccess, setNotifSuccess] = useState(false);
	const [notifFailed, setNotifFailed] = useState(false);

	const fetchData = async () => {
		const url = `${process.env.NEXT_PUBLIC_API}store`;

		try {
			const response = await fetch(url, {
				headers: {
					"account-access": sessionUser?.uuid
				}
			});
		
			if (!response.ok) {
				setIsLoading(false);
				setNotifFailed(true)
				throw new Error('Network response was not ok');
			}

			if (response.status === 200) {
				const data = await response.json();
				if (data) {
					setListData(data.data);
				}	
			}else{
				setListData([])
			}
			setIsLoading(false);
		} catch (error) {
			setIsLoading(false);
			setNotifFailed(true)
		  	console.error('There was a problem with the fetch operation:', error);
		}
	}

	const saveData = async (payload: any) => {
		setIsLoading(true)
		
		const url = `${process.env.NEXT_PUBLIC_API}store`;
		try {
			const response = await fetch(url, {
				method: 'POST',
				headers: {
					"Content-Type": "application/json",
					"account-access": sessionUser?.uuid
				},
				body: JSON.stringify(payload)
			});
		
			if (!response.ok) {
				setIsLoading(false);
				setNotifFailed(true)
				throw new Error('Network response was not ok');
			} else {
				await fetchData()
				setNotifSuccess(true)
			}
			
			setIsLoading(false);
		} catch (error) {
			setIsLoading(false);
			setNotifFailed(true)
		  	console.error('There was a problem with the save operation:', error);
		}
	}

    const updateData = async (payload: any) => {
		setIsLoading(true)
		
		const url = `${process.env.NEXT_PUBLIC_API}store/update`;
		try {
			const response = await fetch(url, {
				method: 'POST',
				headers: {
					"Content-Type": "application/json",
					"account-access": sessionUser?.uuid
				},
				body: JSON.stringify(payload)
			});
		
			if (!response.ok) {
				setIsLoading(false);
				setNotifFailed(true)
				throw new Error('Network response was not ok');
			} else {
				await fetchData()
				setNotifSuccess(true)
			}

			setIsLoading(false);
		} catch (error) {
			setIsLoading(false);
			setNotifFailed(true)
		  	console.error('There was a problem with the update operation:', error);
		}
	}

    const handleSubmit = async (savedData: any) => {
        if (editedData && editedData.uuid !== '') {
            savedData.uuid = editedData.uuid
            await updateData(savedData)
        } else {
            await saveData(savedData)
        }
    }

    const onEdit = (ctx?:any) => {
        const { uuid, name, desc, email, address, telephone, fax } = ctx?.row?.original
        setEditedData({ uuid, name, desc, email, address, telephone, fax })
        setShowModalForm(true)
    }

	useEffect(() => {
		fetchData()
	}, [])

	if (isLoading)
		return <OverlayLoading />
    
	return (
		<div className="mt-10">
			<StoresTable 
				onHandleAdd={() => setShowModalForm(true)}
				onHandleEdit={onEdit}
				tableName="Data Stores"
				tableData={listData} />

			{showModalForm && <ModalForm 
				onClose={() => {
                    setEditedData(null)
                    setShowModalForm(false)
                }}
                defaultValue={editedData ? editedData : null }
				onSubmit={handleSubmit} />}

			{notifSuccess && <NotificationOverlay isShow={notifSuccess} handleClose={() => setNotifSuccess(false)} title='Success' message='Action completed successfully!' color='green' />}
			{notifFailed && <NotificationOverlay isShow={notifFailed} handleClose={() => setNotifFailed(false)} title='Failed' message='Something went wrong. Please try again!' color='red'  />}
		</div>
	);
};

const ModalForm = ({ onClose, onSubmit, defaultValue }: any) => {
	const [formData, setFormData] = useState<any>(defaultValue ? defaultValue : {
		name: '',
		desc: '',
		email: '',
		address: '',
		telephone: '',
		fax: ''
	})

	return (
		<Default title="Form Store" onClose={onClose}>
			<div className="mt-6 border-b border-t border-gray-900/10 pb-12">
				<div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
					<div className="col-span-full">
						<label htmlFor="name" className="block text-sm font-medium leading-6 text-gray-900">
							Name
						</label>
						<div className="mt-2">
							<input
								id="name"
								name="name"
								type="text"
								value={formData?.name}
								onChange={(e) =>
									setFormData({
										...formData,
										name: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
					<div className="col-span-full">
						<label htmlFor="desc" className="block text-sm font-medium leading-6 text-gray-900">
							Description
						</label>
						<div className="mt-2">
							<textarea
								id="desc"
								name="desc"
								value={formData?.desc}
								onChange={(e) =>
									setFormData({
										...formData,
										desc: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
							/>
						</div>
						<p className="mt-3 text-sm leading-6 text-gray-600">Write a few sentences about the data.</p>
					</div>
                    <div className="col-span-full">
						<label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
							Email
						</label>
						<div className="mt-2">
							<input
								id="email"
								name="email"
								type="email"
								value={formData?.email}
								onChange={(e) =>
									setFormData({
										...formData,
										email: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
                    <div className="col-span-full">
						<label htmlFor="address" className="block text-sm font-medium leading-6 text-gray-900">
							Address
						</label>
						<div className="mt-2">
							<textarea
								id="address"
								name="address"
								value={formData?.address}
								rows={3}
								onChange={(e) =>
									setFormData({
										...formData,
										address: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6"
							/>
						</div>
						<p className="mt-3 text-sm leading-6 text-gray-600">Write a few sentences about the data.</p>
					</div>
                    <div className="col-span-full">
						<label htmlFor="telephone" className="block text-sm font-medium leading-6 text-gray-900">
							Telephone
						</label>
						<div className="mt-2">
							<input
								id="telephone"
								name="telephone"
								type="text"
								value={formData?.telephone}
								onChange={(e) =>
									setFormData({
										...formData,
										telephone: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
                    <div className="col-span-full">
						<label htmlFor="fax" className="block text-sm font-medium leading-6 text-gray-900">
							Fax
						</label>
						<div className="mt-2">
							<input
								id="fax"
								name="fax"
								type="text"
								value={formData?.fax}
								onChange={(e) =>
									setFormData({
										...formData,
										fax: e.target.value,
									})
								}
								className="block w-full rounded-md border-0 px-2 py-1 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-sm sm:leading-6 outline-none"
							/>
						</div>
					</div>
				</div>
			</div>
			<div className="mt-6 flex items-center justify-end gap-x-6">
				<button
					type="button"
					onClick={() => {
						setFormData({
							name: '',
							desc: ''
						})
						onSubmit(formData)
						onClose()
					}}
					className="rounded-md bg-green-600 px-4 py-2 text-md text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"> Submit
				</button>
			</div>
		</Default>
	)
}

export default Page;
