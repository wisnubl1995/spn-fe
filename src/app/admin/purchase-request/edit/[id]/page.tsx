'use client'
import Card from 'components/card'
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useParams, useRouter } from 'next/navigation'
import React, { useContext, useEffect, useState } from 'react'
import { IoCheckmarkCircle } from 'react-icons/io5';
import { sendRequestApi } from 'utils/api/fetchData';
import { validateForm } from 'utils/helper';

type Props = {};

const Page = (props: Props) => {
    const router = useRouter()
    const params = useParams()
    const uuid = params?.id

    const { sessionUser } = useContext(AuthContext);
    const headersReq = {
		"account-access": sessionUser?.uuid
	}
    const [isLoading, setIsLoading] = useState(false)
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [details, setDetails] = useState(null)
    const [items, setItems] = useState([])

    const handleSubmit = async () => {
        const approvedItems = items.filter(item => item.quantityApproved !== "" && item.status === "Open")
        if (approvedItems.length > 0) {
            const payload: any = {
                idPr: uuid,
                prItemDetails: approvedItems.map(item => ({
                    id: item.itemId,
                    quantityApproved: `${item.quantityApproved}`,
                    additionalInfo: item.additionalInfo,
                    quantityApprovedBy: sessionUser?.name
                }))
            }

            setIsLoading(true)
            try {
                const resp = await sendRequestApi(`purchaseitems/update`, 'PATCH', JSON.stringify(payload), headersReq)
                setNotifMessage(`${resp?.message}. Will be redirect to Purchase Request page in 3 seconds`)
                setShowNotif(true)
                setIsLoading(false)

                setTimeout(() => {
                    router.back()
                }, 3000);
            } catch (error) {
                setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
                setShowNotif(true)
                setIsLoading(false)
                console.log(`Failed submit purchase request!`, error?.message)
            }
        } else {
            setNotifMessage('Failed! Please fill out required fields (in List Items, at least 1 item).')
            setShowNotif(true)
        }
    }
    
    const handleSubmitItem = (index: number, newItemValue: any) => {
        const newItems = [...items];
        newItems[index] = newItemValue; // Update specific item
        setItems(newItems);
    };

    const fetchDetails = async () => {
        setIsLoading(true)
        try {
            const resp = await sendRequestApi(`purchaserequest/${uuid}`, 'GET', null, headersReq)
            if (resp.data) {
                setDetails(resp.data)
                if (resp.data?.purchaseItems) {
                    setItems(resp.data?.purchaseItems.map((item: any, idx:number) => (
                        {
                            index: idx,
                            itemId: item.uuid,
                            itemName: item.description,
                            unit: item.unit,
                            unit_uuid: '',
                            quantity: parseInt(item.quantityAsked),
                            quantityApproved: item?.quantityApproved ? parseInt(item.quantityApproved) : '',
                            sapCode: item.sapCode,
                            sapCode_uuid: '',
                            additionalInfo: item.additionalInfo,
                            status: item.status,
                            image: item.image,
                        }
                    )))
                }
            }else{
                setNotifMessage(`Failed! ${resp?.message}`)
                setShowNotif(true)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
        }
    }

    useEffect(() => {
        fetchDetails()
    }, [uuid])

    if (isLoading) 
        return <OverlayLoading />

    return (
        <div className="mt-10">
            <Card extra={'w-full pb-10 p-4 h-full'}>
                <header className="relative flex items-center justify-between">
                    <div className="py-2 text-xl font-bold text-navy-700 dark:text-white">
                        Items Quantity Approved - Purchase Request
                    </div>
                </header>
                <div className="mt-6 border-b border-t border-gray-900/10 pb-12">
                    <div className="mt-5 grid grid-cols-1 gap-x-6 gap-y-1 sm:grid-cols-6">
                        <div className="sm:col-span-2">
                            <label htmlFor="purchaseNumber" className="block text-base font-medium leading-6 text-gray-900">
                                PR Number
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    disabled
                                    name="purchaseNumber"
                                    id="purchaseNumber"
                                    value={details?.purchaseRequestNo}
                                    placeholder="Purchase Request Number"
                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="ship" className="block text-base font-medium leading-6 text-gray-900">
                                Ship
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    disabled
                                    name="ship"
                                    id="ship"
                                    value={details?.shipName}
                                    placeholder="Ship Name"
                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="location" className="block text-base font-medium leading-6 text-gray-900">
                                Location
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    disabled
                                    name="location"
                                    id="location"
                                    value={details?.location}
                                    placeholder="Location"
                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="department" className="block text-base font-medium leading-6 text-gray-900">
                                Department
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    disabled
                                    name="department"
                                    id="department"
                                    value={details?.department}
                                    placeholder="Department"
                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="category" className="block text-base font-medium leading-6 text-gray-900">
                                Category
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    disabled
                                    name="category"
                                    id="category"
                                    value={details?.category}
                                    placeholder="Category"
                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <label htmlFor="requestBy" className="block text-base font-medium leading-6 text-gray-900">
                                Request By
                            </label>
                            <div className="mt-2">
                                <input
                                    disabled
                                    type="text"
                                    name="requestBy"
                                    value={details?.requestedBy}
                                    id="requestBy"
                                    placeholder="Request By"
                                    className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                                />
                            </div>
                        </div>
                    </div>
                    <hr className='my-4' />
                    <div className="border-b-2 py-4 flex flex-row justify-between items-center">
                        <h2 className="text-2xl font-semibold leading-7 text-gray-900">List Items</h2>
                    </div>
                    {items.map((item:any) => (
                        <FormItem 
                            key={item.index} 
                            index={item.index}
                            item={item}
                            onSubmit={handleSubmitItem} />
                    ))}
                </div>
                <div className="mt-6 flex items-center justify-end gap-x-6">
                    <button
                        type="button"
                        onClick={() => router.back()}
                        className="rounded-md bg-gray-600 px-4 py-2 text-md text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"> Cancel
                    </button>
                    <button
                        type="button"
                        onClick={handleSubmit}
                        className="rounded-md bg-green-600 px-4 py-2 text-md text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"> Submit
                    </button>
                </div>
            </Card>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

const FormItem = ({ index, item, onSubmit }) => {
    const [showNotif, setShowNotif] = useState(false)
    const [notifMessage, setNotifMessage] = useState(null)
    const [statusItem, setStatusItem] = useState(item.status ? item.status : 'pending')
    const [newItem, setNewItem] = useState({
        index: index,
        itemId: item?.itemId ? item?.itemId : '',
        itemName: item?.itemName ? item?.itemName : '',
        unit: item?.unit ? item?.unit : '',
        unit_uuid: '',
        quantity: item?.quantity ? item?.quantity : '',
        quantityApproved: '',
        sapCode: item?.sapCode ? item?.sapCode : '',
        sapCode_uuid: '',
        additionalInfo: item?.additionalInfo ? item?.additionalInfo : '',
        status: item.status,
        image: item?.image ? item?.image : '',
    });
    const [isSaved, setIsSaved] = useState(item.status === "Close" ? true : false)


    const handleChange = (event:any) => {
        if (event.target.name === 'quantityApproved') {
            if (parseInt(event.target.value) > parseInt(item.quantity) || parseInt(event.target.value) < 0) {
                event.target.value = parseInt(item.quantity)
            }
        }
        
        let tmpItem: any = {
            ...newItem,
            [event.target.name]: event.target.value
        }

        if (event.target.uuid) {
            tmpItem[`${event.target.name}_uuid`] = event.target.uuid
        }

        setNewItem(tmpItem);
    };

    const handleSubmit = (event:any) => {
        event.preventDefault();
        console.log(newItem)
        const optionalFields = { 
            additionalInfo: true, image: true, unit_uuid: true, sapCode: true,
            sapCode_uuid: true, index: true, itemId: true, status: true
        };

        if (validateForm(newItem, optionalFields)) {
            // Form is valid, submit data
            onSubmit(index, newItem); // Pass index and new item value
            setNewItem({
                index: index,
                itemId: item?.uuid ? item?.uuid : '',
                itemName: '',
                unit: '',
                unit_uuid: '',
                quantity: '',
                quantityApproved: '',
                sapCode: '',
                sapCode_uuid: '',
                additionalInfo: '',
                status: '',
                image: '',
            }); // Clear the input field after submission
            setIsSaved(true)
            setStatusItem('approved')
        } else {
            // Form is invalid, show error message
            setNotifMessage('Failed! Please fill out all required fields (Quantity Approved).')
            setShowNotif(true)
        }
    };

    return (
        <div className="border rounded-md p-4 mt-5 grid grid-cols-1 gap-x-2 gap-y-1 sm:grid-cols-4">
            <div className="sm:col-span-1">
                <label htmlFor="itemName" className="block text-base font-medium leading-6 text-gray-900">
                    Item Name
                </label>
                <div className="mt-2">
                    <input
                        type="text"
                        disabled
                        name="itemName"
                        id="itemName"
                        placeholder="Input Item Name"
                        value={item?.itemName ? item?.itemName : newItem.itemName}
                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                    />
                </div>
            </div>
            <div className="sm:col-span-1">
                <label htmlFor="unit" className="block text-base font-medium leading-6 text-gray-900">
                    Unit
                </label>
                <div className="mt-2">
                    <input
                        type="text"
                        disabled
                        name="unit"
                        id="unit"
                        value={item?.unit ? item?.unit : newItem.unit}
                        placeholder="Unit"
                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                    />
                </div>
            </div>
            <div className="sm:col-span-1">
                <label htmlFor="sapCode" className="block text-base font-medium leading-6 text-gray-900">
                    SAP Code 
                </label>
                <div className="mt-2">
                    <input
                        type="text"
                        disabled
                        name="sapCode"
                        id="sapCode"
                        value={item?.sapCode ? item?.sapCode : newItem.sapCode}
                        placeholder="SAP Code"
                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                    />
                </div>
            </div>
            <div className="sm:col-span-1 row-span-2 relative flex flex-1 flex-col items-center justify-center">
                <div className="absolute top-0 right-0">
                    <span className="px-4 py-2 rounded-full bg-yellow-600 text-white font-bold">
                        {index+1}
                    </span>
                </div>
                {!isSaved && (
                    <button type="button" className='w-3/6 mt-2 px-3 py-2 text-base font-medium rounded-md bg-green-500 text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 shadow-sm' onClick={handleSubmit}>
                        <IoCheckmarkCircle className="inline-block h-6 w-6" /> Save Item
                    </button>
                )}
                {!isSaved && (
                <button type="button" className='w-3/6 mt-2 px-3 py-2 text-base font-medium rounded-md bg-red-500 text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 shadow-sm' onClick={() => {
                    setIsSaved(true)
                    setStatusItem('declined')
                }}>
                    <IoCheckmarkCircle className="inline-block h-6 w-6" /> Decline Item
                </button>)}
                <p className="my-2 py-2 text-base">Status Item : <span className={`font-bold ${statusItem === 'declined' ? 'text-red-400' : (statusItem === 'approved' ? 'text-green-400' : 'text-gray-400')}`}>{statusItem?.toLocaleUpperCase()}</span></p>
            </div>
            <div className="sm:col-span-1">
                <label htmlFor="quantity" className="block text-base font-medium leading-6 text-gray-900">
                    Quantity Asked?
                </label>
                <div className="mt-2">
                    <input
                        type="number"
                        disabled
                        name="quantity"
                        id="quantity"
                        placeholder="Input Quantity"
                        value={item?.quantity ? item?.quantity : newItem.quantity}
                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                    />
                </div>
            </div>
            <div className="sm:col-span-1">
                <label htmlFor="quantityApproved" className="block text-base font-medium leading-6 text-gray-900">
                    Quantity Approved <span className='text-red-500'>*</span>
                </label>
                <div className="mt-2">
                    <input
                        type="number"
                        disabled={isSaved}
                        name="quantityApproved"
                        value={item?.quantityApproved ? item?.quantityApproved : newItem.quantityApproved}
                        id="quantityApproved"
                        placeholder="Input quantityApproved"
                        onChange={handleChange}
                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                    />
                </div>
            </div>
            <div className="sm:col-span-1">
                <label htmlFor="additionalInfo" className="block text-base font-medium leading-6 text-gray-900">
                    Additional Info
                </label>
                <div className="mt-2">
                    <input
                        type="text"
                        name="additionalInfo"
                        id="additionalInfo"
                        disabled={isSaved}
                        placeholder="Input Additional Info"
                        value={item?.additionalInfo ? item?.additionalInfo : newItem.additionalInfo}
                        onChange={handleChange}
                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                    />
                </div>
            </div>
            <div className="sm:col-span-1">
                <label htmlFor="image" className="block text-base font-medium leading-6 text-gray-900">
                    Image
                </label>
                <div className="mt-2">
                    {item?.image ?
                    <img src={item?.image} className="w-25 h-25" />
                    : '-'}
                </div>
            </div>

            {showNotif && 
                <NotificationOverlay 
                    isShow={showNotif}
                    title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
                    message={notifMessage}
                    handleClose={() => setShowNotif(false)}
                    color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
        </div>
    )
}

export default Page