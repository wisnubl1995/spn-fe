'use client';
import Card from 'components/card';
import AutocompleteInput from 'components/fields/AutoComplete';
import FileUpload from 'components/fields/FileUpload';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AuthContext from 'contexts/AuthContext';
import { useRouter } from 'next/navigation';
import React, { useContext, useEffect, useState } from 'react';
import { BsPlusCircle } from 'react-icons/bs';
import { IoCheckmarkCircle, IoCloseCircle } from 'react-icons/io5';
import { sendRequestApi } from 'utils/api/fetchData';
import {
  formatSequenceNumber,
  getBase64,
  slugify,
  validateForm,
} from 'utils/helper';
import PapaAceng from 'papaparse';
import onAddAutocomplete from 'utils/api/handleAddAutocomplete';
import * as XLSX from 'xlsx';

type Props = {};

const Page = (props: Props) => {
  const router = useRouter();
  const { sessionUser } = useContext(AuthContext);
  const headersReq = {
    'account-access': sessionUser?.uuid,
  };
  const [isLoading, setIsLoading] = useState(false);
  const [showNotif, setShowNotif] = useState(false);
  const [notifMessage, setNotifMessage] = useState(null);
  const [selectedShip, setSelectedShip] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [selectedDepartment, setSelectedDepartment] = useState(null);
  const [location, setLocation] = useState(null);
  const [PRNumber, setPRNumber] = useState('');
  const [sequenceNumPR, setSequenceNumPR] = useState<any>(1);
  const [items, setItems] = useState([]);

  const handleSubmit = async () => {
    const payload: any = {
      purchaseRequestNo: PRNumber,
      shipName: selectedShip?.name,
      category: selectedCategory?.name,
      department: selectedDepartment?.name,
      requestedBy: sessionUser?.name,
      location: location?.name,
      createdBy: sessionUser?.name,
    };

    if (validateForm(payload) && items.length > 0) {
      // Form is valid, submit data
      payload.purchaseItems = items.map((item) => ({
        sapCode: item.sapCode,
        quantityAsked: item.quantity,
        quantityLeft: 'nill',
        unit: item.unit,
        description: item.itemName,
        additionalInfo: item.additionalInfo,
        image: item.image,
      }));

      setIsLoading(true);
      try {
        const resp = await sendRequestApi(
          `purchaserequest`,
          'POST',
          JSON.stringify(payload),
          headersReq,
        );
        setNotifMessage(
          `${resp?.message}. Will be redirect to Purchase Request page in 3 seconds`,
        );
        setShowNotif(true);
        setIsLoading(false);

        setTimeout(() => {
          router.back();
        }, 3000);
      } catch (error) {
        setNotifMessage(
          `Failed! ${
            error?.message ? error?.message : 'Internal Server Error'
          }`,
        );
        setShowNotif(true);
        setIsLoading(false);
        console.log(`Failed submit purchase request!`, error?.message);
      }
    } else {
      setNotifMessage(
        'Failed! Please fill out all required fields (PR Number, Ship, Category, Department and Items).',
      );
      setShowNotif(true);
    }
  };

  const handleRemoveItem = (index: number) => {
    let newItems = [...items];
    newItems = newItems.filter((obj) => obj.index !== index);
    setItems(newItems);
  };

  const handleSubmitItem = (index: number, newItemValue: any) => {
    const newItems = [...items];
    newItems[index] = newItemValue; // Update specific item
    setItems(newItems);
  };

  const handleUpload = (file: File) => {
    if (
      file.type.includes('application/vnd.ms-excel') ||
      file.type.includes(
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      )
    ) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const binaryStr = e.target.result;
        const workbook = XLSX.read(binaryStr, { type: 'binary' });

        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const csvData = XLSX.utils.sheet_to_csv(worksheet);

        // Sekarang proses CSV menggunakan PapaParse (alias PapaAceng)
        PapaAceng.parse(csvData, {
          header: true,
          complete: function (results) {
            console.log('Completed Read File:', results.data);
            if (results.data.length > 0) {
              const data = [];
              for (let r = 0; r < results.data.length; r++) {
                const row: any = results.data[r];
                data.push({
                  index: items.length ? items.length : r,
                  itemName: row.itemName || '',
                  unit: row.unit || '',
                  unit_uuid: '',
                  quantity: parseInt(row.quantity) || 0,
                  sapCode: row.sap_code || '',
                  sapCode_uuid: '',
                  additionalInfo: row.info || '',
                  image: '',
                });
              }
              setItems([...items, ...data]);
            }
          },
        });
      };

      reader.readAsBinaryString(file);
    } else if (file.type.includes('text/csv')) {
      PapaAceng.parse(file, {
        header: true,
        complete: function (results) {
          console.log('Completed Read File:', results.data);
          if (results.data.length > 0) {
            const data = [];
            for (let r = 0; r < results.data.length; r++) {
              const row: any = results.data[r];
              data.push({
                index: items.length ? items.length : r,
                itemName: row.itemName || '',
                unit: row.unit || '',
                unit_uuid: '',
                quantity: parseInt(row.quantity) || 0,
                sapCode: row.sap_code || '',
                sapCode_uuid: '',
                additionalInfo: row.info || '',
                image: '',
              });
            }
            setItems([...items, ...data]);
          }
        },
      });
    } else {
      setNotifMessage(
        'Failed! Invalid format file (.csv, .xls, or .xlsx only).',
      );
      setShowNotif(true);
    }
  };

  const fetchSeqPR = async () => {
    try {
      const resp = await sendRequestApi(
        `purchaserequest/count`,
        'GET',
        null,
        headersReq,
      );
      if (resp?.data) {
        setSequenceNumPR(parseInt(resp.data));
      }
    } catch (error) {
      console.log('Failed fetch sequence number!');
    }
  };

  useEffect(() => {
    fetchSeqPR();
  }, []);

  const onChangePR = () => {
    const dateObj = new Date();
    const prNo = `${
      selectedShip
        ? slugify(
            selectedShip.shipCode !== ''
              ? selectedShip.shipCode
              : selectedShip.name,
          )?.toUpperCase()
        : '-'
    }/SPN/${
      selectedDepartment ? slugify(selectedDepartment.name)?.toUpperCase() : '-'
    }/${('0' + (dateObj.getMonth() + 1)).slice(
      -2,
    )}/${dateObj.getFullYear()}/${formatSequenceNumber(
      parseInt(sequenceNumPR),
      3,
    )}`;
    setPRNumber(prNo);
  };

  useEffect(() => {
    onChangePR();
  }, [selectedShip, selectedDepartment, sequenceNumPR]);

  if (isLoading) return <OverlayLoading />;

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="py-2 text-xl font-bold text-navy-700 dark:text-white">
            Form Purchase Request
          </div>
        </header>
        <div className="mt-6 border-b border-t border-gray-900/10 pb-12">
          <div className="mt-5 grid grid-cols-1 gap-x-6 gap-y-1 sm:grid-cols-6">
            <div className="sm:col-span-2">
              <label
                htmlFor="purchaseNumber"
                className="block text-base font-medium leading-6 text-gray-900"
              >
                PR Number <span className="text-red-500">*</span>
              </label>
              <div className="mt-2">
                <input
                  type="text"
                  disabled
                  name="purchaseNumber"
                  value={PRNumber}
                  id="purchaseNumber"
                  placeholder="Purchase Request Number"
                  className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                />
              </div>
            </div>
            <div className="sm:col-span-2">
              <label
                htmlFor="ship"
                className="block text-base font-medium leading-6 text-gray-900"
              >
                Ship <span className="text-red-500">*</span>
              </label>
              <div className="mt-2">
                <AutocompleteInput
                  placeholder="Search Ship Name"
                  apiUrl="ship/search"
                  searchQuery="name"
                  keyLabel="name"
                  keyValue="name"
                  minTreshold={3}
                  onSelect={async (suggestion: any) => {
                    setSelectedShip(suggestion);
                    if (suggestion?.isNew) {
                      await onAddAutocomplete(
                        'ship',
                        {
                          name: suggestion.name,
                          desc: '',
                          shipCode: '',
                          location: '',
                          image: '',
                        },
                        headersReq,
                      );
                    }
                  }}
                />
                <p className="mt-3 text-sm leading-6 text-gray-600">
                  Min 3 character for search.
                </p>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label
                htmlFor="location"
                className="block text-base font-medium leading-6 text-gray-900"
              >
                Location
              </label>
              <div className="mt-2">
                <AutocompleteInput
                  placeholder="Search Location"
                  apiUrl="location/search"
                  searchQuery="name"
                  keyLabel="name"
                  keyValue="name"
                  minTreshold={3}
                  onSelect={async (suggestion: any) => {
                    setLocation(suggestion);
                    if (suggestion?.isNew) {
                      await onAddAutocomplete(
                        'location',
                        {
                          name: suggestion.name,
                          email: '',
                          address: '',
                          telephone: '',
                          fax: '',
                          desc: '',
                        },
                        headersReq,
                      );
                    }
                  }}
                />
                <p className="mt-3 text-sm leading-6 text-gray-600">
                  Min 3 character for search.
                </p>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label
                htmlFor="department"
                className="block text-base font-medium leading-6 text-gray-900"
              >
                Department <span className="text-red-500">*</span>
              </label>
              <div className="mt-2">
                <AutocompleteInput
                  placeholder="Search Department Name"
                  apiUrl="department/search"
                  searchQuery="name"
                  keyLabel="name"
                  keyValue="name"
                  minTreshold={3}
                  onSelect={async (suggestion: any) => {
                    setSelectedDepartment(suggestion);
                    if (suggestion?.isNew) {
                      await onAddAutocomplete(
                        'department',
                        {
                          name: suggestion.name,
                          desc: '',
                        },
                        headersReq,
                      );
                    }
                  }}
                />
                <p className="mt-3 text-sm leading-6 text-gray-600">
                  Min 3 character for search.
                </p>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label
                htmlFor="category"
                className="block text-base font-medium leading-6 text-gray-900"
              >
                Category <span className="text-red-500">*</span>
              </label>
              <div className="mt-2">
                <AutocompleteInput
                  placeholder="Search Category Name"
                  apiUrl="category/search"
                  searchQuery="name"
                  keyLabel="name"
                  keyValue="name"
                  minTreshold={3}
                  onSelect={async (suggestion: any) => {
                    setSelectedCategory(suggestion);
                    if (suggestion?.isNew) {
                      await onAddAutocomplete(
                        'category',
                        {
                          name: suggestion.name,
                          desc: '',
                        },
                        headersReq,
                      );
                    }
                  }}
                />
                <p className="mt-3 text-sm leading-6 text-gray-600">
                  Min 3 character for search.
                </p>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label
                htmlFor="requestBy"
                className="block text-base font-medium leading-6 text-gray-900"
              >
                Request By
              </label>
              <div className="mt-2">
                <input
                  disabled
                  type="text"
                  name="requestBy"
                  value={sessionUser?.name}
                  id="requestBy"
                  placeholder="Request By"
                  className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
                />
              </div>
            </div>
          </div>
          <hr className="my-4" />
          <div className="flex flex-row items-center justify-between border-b-2 py-4">
            <h2 className="text-2xl font-semibold leading-7 text-gray-900">
              List Items
            </h2>
            <FileUpload
              onUpload={handleUpload}
              accept=".csv, .txt .xls .xlsx"
              title="Bulk Upload"
            />
          </div>
          {items.map((item: any) => (
            <FormItem
              key={item.index}
              index={item.index}
              item={item}
              onRemove={handleRemoveItem}
              onSubmit={handleSubmitItem}
            />
          ))}
          <div className="my-6">
            <button
              type="button"
              onClick={() => {
                setItems([
                  ...items,
                  {
                    index: items.length === 0 ? 0 : items.length,
                    itemName: '',
                    unit: '',
                    unit_uuid: '',
                    quantity: '',
                    sapCode: '',
                    sapCode_uuid: '',
                    additionalInfo: '',
                    image: '',
                  },
                ]);
              }}
              className="text-md w-1/3 rounded-md bg-brand-600 px-4 py-2 text-white shadow-sm hover:bg-brand-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-brand-600"
            >
              <BsPlusCircle className="inline-block h-5 w-6" /> Add New
            </button>
          </div>
        </div>
        <div className="mt-6 flex items-center justify-end gap-x-6">
          <button
            type="button"
            onClick={() => router.back()}
            className="text-md rounded-md bg-gray-600 px-4 py-2 text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
          >
            {' '}
            Cancel
          </button>
          <button
            type="button"
            onClick={handleSubmit}
            className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
          >
            {' '}
            Submit
          </button>
        </div>
      </Card>

      {showNotif && (
        <NotificationOverlay
          isShow={showNotif}
          title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
          message={notifMessage}
          handleClose={() => setShowNotif(false)}
          color={notifMessage?.includes('Failed') ? 'red' : 'green'}
        />
      )}
    </div>
  );
};

const FormItem = ({ index, item, onRemove, onSubmit }) => {
  const [showNotif, setShowNotif] = useState(false);
  const [notifMessage, setNotifMessage] = useState(null);
  const [newItem, setNewItem] = useState({
    index: index,
    itemName: item?.itemName ? item?.itemName : '',
    unit: item?.unit ? item?.unit : '',
    unit_uuid: '',
    quantity: item?.quantity ? item?.quantity : '',
    sapCode: item?.sapCode ? item?.sapCode : '',
    sapCode_uuid: '',
    additionalInfo: item?.additionalInfo ? item?.additionalInfo : '',
    image: item?.image ? item?.image : '',
  });
  const [isSaved, setIsSaved] = useState(false);

  const handleChange = (event: any) => {
    let tmpItem: any = {
      ...newItem,
      [event.target.name]: event.target.value,
    };

    if (event.target.uuid) {
      tmpItem[`${event.target.name}_uuid`] = event.target.uuid;
    }

    setNewItem(tmpItem);
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    const optionalFields = {
      additionalInfo: true,
      image: true,
      unit_uuid: true,
      sapCode: true,
      sapCode_uuid: true,
      index: true,
    };
    if (validateForm(newItem, optionalFields)) {
      // Form is valid, submit data
      onSubmit(index, newItem); // Pass index and new item value
      // setNewItem({
      //     index: index,
      //     itemName: '',
      //     unit: '',
      //     unit_uuid: '',
      //     quantity: '',
      //     sapCode: '',
      //     sapCode_uuid: '',
      //     additionalInfo: '',
      //     image: '',
      // });
      // Clear the input field after submission
      setIsSaved(true);
    } else {
      // Form is invalid, show error message
      setNotifMessage(
        'Failed! Please fill out all required fields (Item Name, Unit and Quantity).',
      );
      setShowNotif(true);
    }
  };

  return (
    <div className="mt-5 grid grid-cols-1 gap-x-2 rounded-md border p-4 sm:grid-cols-4">
      <div className="sm:col-span-1">
        <label
          htmlFor="itemName"
          className="block text-base font-medium leading-6 text-gray-900"
        >
          Item Name <span className="text-red-500">*</span>
        </label>
        <div className="mt-2">
          <input
            type="text"
            disabled={isSaved}
            name="itemName"
            id="itemName"
            placeholder="Input Item Name"
            value={newItem.itemName}
            onChange={handleChange}
            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
      </div>
      <div className="sm:col-span-1">
        <label
          htmlFor="unit"
          className="block text-base font-medium leading-6 text-gray-900"
        >
          Unit <span className="text-red-500">*</span>
        </label>
        <div className="mt-2">
          <AutocompleteInput
            placeholder="Search Unit"
            disabled={isSaved}
            apiUrl="unit/search"
            searchQuery="name"
            defaultValue={item?.unit ? item.unit : ''}
            keyLabel="name"
            keyValue="name"
            minTreshold={2}
            onSelect={(suggestion: any) => {
              console.log(suggestion);
              // setSelectedUnit(suggestion)
              handleChange({
                target: {
                  name: 'unit',
                  value: suggestion.name,
                  uuid: suggestion.uuid,
                },
              });
            }}
          />
          <p className="text-sm leading-6 text-gray-600">
            Min 2 character for search.
          </p>
        </div>
      </div>
      <div className="sm:col-span-1">
        <label
          htmlFor="quantity"
          className="block text-base font-medium leading-6 text-gray-900"
        >
          Quantity <span className="text-red-500">*</span>
        </label>
        <div className="mt-2">
          <input
            type="number"
            disabled={isSaved}
            name="quantity"
            id="quantity"
            placeholder="Input Quantity"
            value={newItem.quantity}
            onChange={handleChange}
            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
      </div>
      <div className="relative row-span-2 flex flex-1 flex-col items-center justify-center sm:col-span-1">
        <div className="absolute right-0 top-0">
          <span className="rounded-full bg-yellow-600 px-4 py-2 font-bold text-white">
            {index + 1}
          </span>
        </div>
        {!isSaved && (
          <button
            type="button"
            className="mt-2 w-3/6 rounded-md bg-green-500 px-3 py-2 text-base font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
            onClick={handleSubmit}
          >
            <IoCheckmarkCircle className="inline-block h-6 w-6" /> Save Item
          </button>
        )}
        <button
          type="button"
          className=" mt-2 w-3/6 rounded-md bg-red-500 px-3 py-2 text-base font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2"
          onClick={() => onRemove(index)}
        >
          <IoCloseCircle className="inline-block h-6 w-6" /> Remove Item
        </button>
      </div>
      <div className="sm:col-span-1">
        <label
          htmlFor="sapCode"
          className="block text-base font-medium leading-6 text-gray-900"
        >
          SAP Code
        </label>
        <div className="mt-2">
          <AutocompleteInput
            placeholder="Search SAP Code"
            disabled={isSaved}
            apiUrl="sapcode/search"
            defaultValue={item?.sapCode ? item.sapCode : ''}
            searchQuery="name"
            keyLabel="name"
            keyValue="name"
            minTreshold={3}
            onSelect={(suggestion: any) => {
              console.log(suggestion);
              // setSelectedSapCode(suggestion)
              handleChange({
                target: {
                  name: 'sapCode',
                  value: suggestion.name,
                  uuid: suggestion.uuid,
                },
              });
            }}
          />
          <p className="text-sm leading-6 text-gray-600">
            Min 3 character for search.
          </p>
        </div>
      </div>
      <div className="sm:col-span-1">
        <label
          htmlFor="additionalInfo"
          className="block text-base font-medium leading-6 text-gray-900"
        >
          Additional Info
        </label>
        <div className="mt-2">
          <input
            type="text"
            name="additionalInfo"
            disabled={isSaved}
            id="additionalInfo"
            placeholder="Input Additional Info"
            value={newItem.additionalInfo}
            onChange={handleChange}
            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
      </div>
      <div className="sm:col-span-1">
        <label
          htmlFor="image"
          className="block text-base font-medium leading-6 text-gray-900"
        >
          Image
        </label>
        <div className="mt-2">
          <input
            accept="image/*"
            type="file"
            disabled={isSaved}
            name="image"
            id="image"
            onChange={async (ev) => {
              handleChange({
                target: {
                  name: 'image',
                  value: await getBase64(ev.target.files[0]),
                },
              });
            }}
            placeholder="Input Additional Info"
            className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
      </div>

      {showNotif && (
        <NotificationOverlay
          isShow={showNotif}
          title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
          message={notifMessage}
          handleClose={() => setShowNotif(false)}
          color={notifMessage?.includes('Failed') ? 'red' : 'green'}
        />
      )}
    </div>
  );
};

export default Page;
