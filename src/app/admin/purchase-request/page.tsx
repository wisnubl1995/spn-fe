'use client';

import Card from 'components/card';
import CardMenu from 'components/card/CardMenu';
import OverlayLoading from 'components/loading/OverlayLoading';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import React, { useContext, useEffect, useState } from 'react';
import { MdCheck, MdCreate, MdSearchOff } from 'react-icons/md';
import { formatDate } from 'utils/formatDate';
import { buildQuery, formattedDate } from 'utils/helper';
import { FaSearch, FaTrashAlt } from 'react-icons/fa';
import exportToExcel from 'utils/exportExcel';
import AuthContext from 'contexts/AuthContext';
import { sendRequestApi } from 'utils/api/fetchData';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import AutocompleteInput from 'components/fields/AutoComplete';
import DefaultModal from 'components/modal/Default';
import { FaFileExport } from 'react-icons/fa6';
import CustomTooltip from 'components/tooltip/CustomTooltip';

type Props = {};

const Page = (props: Props) => {
  const router = useRouter()
  const { sessionUser } = useContext(AuthContext)
  const headersReq = {
		"account-access": sessionUser?.uuid
	}
  const [newPurchaseRequestData, setNewPurchaseRequestData] = useState<any>({
    purchaseRequestNo: '',
    shipName: '',
    department: '',
    category: '',
    requestedBy: 'wisnu',
    createdBy: 'wisnu',
    location: 'Jakarta',
    purchaseItems: [],
  });
  const [items, setItems] = useState([{ id: 1 }]);
  const [selectedUUID, setSelectedUUID] = useState('');
  const [addNew, setAddNew] = useState(false);
  const [activeTab, setActiveTab] = useState('Pending');
  const [showDetail, setShowDetail] = useState(false);
  const [selectedRow, setSelectedRow] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [showNotif, setShowNotif] = useState(false)
  const [notifMessage, setNotifMessage] = useState(null)
  const [showFilter, setShowFilter] = useState(false);
  const [filteredQuery, setFilteredQuery] = useState(null);
  const [showDirectApprove, setShowDirectApprove] = useState(false);
  const [details, setDetails] = useState(null);
  const [itemsApproved, setItemsApproved] = useState([])

  const [bulk, setBulk] = useState(false);

  const [isChecked, setIsChecked] = useState(false); // State untuk menandai apakah ada item yang dicentang

  const handleCheckboxChange = (index) => {
    const updatedItems = [...selectedRow.purchaseItems];
    updatedItems[index].isChecked = !updatedItems[index].isChecked;
    setSelectedRow({ ...selectedRow, purchaseItems: updatedItems });

    setIsChecked(updatedItems.some((item) => item.isChecked));
  };

  const handleSelectUUID = (uuid) => {
    setSelectedUUID(uuid);
    // Mengambil objek yang sesuai dengan UUID yang dipilih
    const selectedData = purchaseData.find((data) => data.uuid === uuid);
    console.log(selectedData);
    setSelectedRow(selectedData);
    // Tampilkan pop-up atau lakukan operasi lain sesuai kebutuhan
    setShowDetail(true);
  };

  const [purchaseData, setPurchaseData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(true);
  let isMounted = true;

  const fetchData = async () => {
    setIsLoading(true);
    const url = `${process.env.NEXT_PUBLIC_API}purchaserequest?page=${currentPage}&pageSize=10`;

    try {
      const response = await fetch(url, {
        headers: {
          "account-access": sessionUser?.uuid
        }
      });

      if (!response.ok) {
        setIsLoading(false);
        throw new Error('Network response was not ok');
      }

      if (response.status === 200) {
        const data = await response.json();
        if (isMounted) {
          setPurchaseData(data.data);
        }else{
          setPurchaseData([])
          setNotifMessage(`Failed! ${data?.message ? data?.message : 'Internal Server Error'}`)
          setShowNotif(true)
        }
      } else {
        setPurchaseData([])
      }

      setIsLoading(false); // Update isLoading state to false
    } catch (error) {
      setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
      setShowNotif(true)
      setIsLoading(false);
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    isMounted = true
    fetchData();
    return () => { isMounted = false }
  }, [currentPage]);

  if (isLoading) {
    return <OverlayLoading />
  }

  const pendingData = purchaseData
    ? purchaseData.filter((entry) =>
        entry.purchaseItems.some((item) => item.status === 'Open'),
      )
    : 'Loading';
  const completeData = purchaseData
    ? purchaseData.filter((entry) =>
        entry.purchaseItems.every((item) => item.status === 'Close'),
      )
    : 'Loading';

  const updatePurchaseRequest = (updatedRow) => {
    const updatedData = purchaseData.map((item) => {
      if (item.uuid === updatedRow.uuid) {
        return { ...item, ...updatedRow, purchaseItems: item.purchaseItems };
      }
      return item;
    });

    setPurchaseData(updatedData);
    console.log(purchaseData);
  };

  const handleSaveChanges = async () => {
    const checkedItems = selectedRow.purchaseItems.filter(
      (item) => item.isChecked && item.status === 'Open',
    );

    console.log(checkedItems);

    // Membuat payload dengan idPr dan prItemDetails yang sesuai
    const payload = {
      idPr: selectedRow.uuid, // Gunakan purchaseRequestNo sebagai idPr
      prItemDetails: checkedItems.map((item) => ({ id: item.uuid })),
    };

    // Kirim payload ke API
    fetch(`${process.env.NEXT_PUBLIC_API}purchaseitems/movetomoc`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (response.ok) {
          // Handle jika request berhasil
          console.log('Success: Items moved to MOC');
          window.location.reload();
        } else {
          // Handle jika request gagal
          console.error('Failed to move items to MOC');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  const handleCheckAll = () => {
    // Check if all items are already checked
    const allChecked = selectedRow.purchaseItems.every(
      (item) => item.isChecked,
    );

    // Toggle check status based on current status
    const updatedItems = selectedRow.purchaseItems.map((item) => ({
      ...item,
      isChecked: !allChecked, // Toggle the isChecked value
    }));

    setSelectedRow((prevSelectedRow) => ({
      ...prevSelectedRow,
      purchaseItems: updatedItems,
    }));
  };

  const handleApproval = async (item) => {
    const { uuid, quantityApporvedBy, approvedBy, quantityApproved } = item;

    const payload = {
      id: uuid,
      quantityApproved,
      quantityApporvedBy: 'Wisnu',
    };

    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API}purchaseitems/update`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            "account-access": sessionUser?.uuid
          },
          body: JSON.stringify(payload),
        },
      );

      if (response.ok) {
        console.log('Purchase item approved successfully!');
        fetchData();
      } else {
        console.error('Failed to approve purchase item');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const addItem = () => {
    const newItem = { id: items.length + 1 }; // Create a new item object
    setItems([...items, newItem]); // Add the new item to the items array
  };

  const removeItem = (idToRemove) => {
    const updatedItems = items.filter((item) => item.id !== idToRemove);
    setItems(updatedItems);
  };

  const handleSaveNewPurchaseRequest = async () => {
    const newPurchaseRequest = {
      ...newPurchaseRequestData,
    };

    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API}purchaseRequest`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            "account-access": sessionUser?.uuid
          },
          body: JSON.stringify(newPurchaseRequest),
        },
      );

      if (response.ok) {
        setPurchaseData([...purchaseData, newPurchaseRequest]);

        setNewPurchaseRequestData({
          purchaseRequestNo: '',
          shipName: '',
          department: '',
          category: '',
          requestedBy: 'Wisnu',
          createdBy: 'Wisnu',
          location: 'Jakarta',
          purchaseItems: [{}],
        });

        setAddNew(false);
        window.location.reload();
      } else {
        console.error('Failed to save new purchase request');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleExport = () => {
    if (purchaseData.length > 0) {
      setIsLoading(true)
      const tmpData = []
      for (let i = 0; i < purchaseData.length; i++) {
        const header = purchaseData[i];
        for (let j = 0; j < header.purchaseItems.length; j++) {
          const item = header.purchaseItems[j];
          tmpData.push({
            PURCHASE_REQUEST_NO: header.purchaseRequestNo,
            DEPARTEMEN: header.department,
            KATEGORI: header.category,
            NAMA_KAPAL: header.shipName,
            REQUEST_BY: header.requestedBy,
            LOKASI: header.location,
            TANGGAL_REQUEST: formattedDate(new Date(header.createdAt), "YYYY-MM-DD"),
            STATUS_PR: header.status,
            NAMA_BARANG: item.description,
            SATUAN: item.unit,
            QTY: item.quantity,
            SAP_CODE: item.sapCode,
            REMARKS: item.additionalInfo,
            STATUS_BARANG: item.status,
          })
        }
      }
  
      exportToExcel(tmpData, "PURCHASE_REQUEST", "DATA")
      setIsLoading(false)
    }else{
      setNotifMessage(`Failed! No data to be exported!`)
      setShowNotif(true)
    }
  }

  const handleDelete = async (uuid) => {
    setIsLoading(true)
    try {
        const resp = await sendRequestApi(`purchaserequest/${uuid}`, 'DELETE', null, headersReq)
        setNotifMessage(resp?.message)
        setShowNotif(true)
        fetchData()
        setIsLoading(false)
    } catch (error) {
        setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
        setShowNotif(true)
        setIsLoading(false)
        console.log(`Failed submit purchase request!`, error?.message)
    }
  }

  const onSubmitModal = async (formData: any) => {
    setFilteredQuery(formData)
    fetchFiltered(formData)
  }

  const fetchFiltered = async (queries: any) => {
    const url = buildQuery(`${process.env.NEXT_PUBLIC_API}purchaserequest/filter`, queries);
    if (url.includes('?')) {
      setIsLoading(true);
      try {
        const response = await fetch(url, {
          headers: {
            "account-access": sessionUser?.uuid
          }
        });
  
        if (!response.ok) {
          setIsLoading(false);
          throw new Error('Network response was not ok');
        }
  
        if (response.status === 200) {
          const data = await response.json();
          if (isMounted) {
            setPurchaseData(data.data);
          }else{
            setPurchaseData([])
            setNotifMessage(`Failed! ${data?.message ? data?.message : 'Internal Server Error'}`)
            setShowNotif(true)
          }
        } else {
          setPurchaseData([])
        }
  
        setIsLoading(false); // Update isLoading state to false
      } catch (error) {
        setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
        setShowNotif(true)
        setIsLoading(false);
        console.error('There was a problem with the fetch operation:', error);
      }
    }
  }

  const fetchDetailsPR = async (uuid: any) => {
    setIsLoading(true)
    try {
        const resp = await sendRequestApi(`purchaserequest/${uuid}`, 'GET', null, headersReq)
        if (resp.data) {
            setDetails(resp.data)
            if (resp.data?.purchaseItems) {
            	setItemsApproved(resp.data?.purchaseItems.map((item: any, idx:number) => (
                    {
                        index: idx,
                        itemId: item.uuid,
                        itemName: item.description,
                        unit: item.unit,
                        unit_uuid: '',
                        quantity: parseInt(item.quantityAsked),
                        quantityApproved: parseInt(item.quantityAsked),
                        sapCode: item.sapCode,
                        sapCode_uuid: '',
                        additionalInfo: item.additionalInfo,
                        status: item.status,
                        image: item.image,
                    }
                )))
            }
        }else{
            setNotifMessage(`Failed! ${resp?.message}`)
            setShowNotif(true)
        }
        setIsLoading(false)
    } catch (error) {
        setIsLoading(false)
    }
  }

  const onSubmitApproved = async () => {
	const uuid = details?.uuid
	const approvedItems = itemsApproved.filter(item => item.quantityApproved !== "" && item.status === "Open")
	if (approvedItems.length > 0) {
		const payload: any = {
			idPr: uuid,
			prItemDetails: approvedItems.map(item => ({
				id: item.itemId,
				quantityApproved: `${item.quantityApproved}`,
				additionalInfo: item.additionalInfo,
				quantityApprovedBy: sessionUser?.name
			}))
		}

		setIsLoading(true)
		try {
			const resp = await sendRequestApi(`purchaseitems/update`, 'PATCH', JSON.stringify(payload), headersReq)
			setNotifMessage(`${resp?.message}`)
			setShowNotif(true)
			setIsLoading(false)

			fetchData();
		} catch (error) {
			setNotifMessage(`Failed! ${error?.message ? error?.message : 'Internal Server Error'}`)
			setShowNotif(true)
			setIsLoading(false)
			console.log(`Failed submit purchase request!`, error?.message)
		}
	} else {
		setNotifMessage('Failed! Please fill out required fields (in List Items, at least 1 item).')
		setShowNotif(true)
	}
  }
  
  const canGoPrev = currentPage < 1
  const canGoNext = purchaseData.length >= 10

  return (
    <div className="mt-10">
      <Card extra={'w-full pb-10 p-4 h-full'}>
        <header className="relative flex items-center justify-between">
          <div className="text-xl font-bold text-navy-700 dark:text-white">
            Purchase Request
          </div>
          <CardMenu
            handlePopUp={() => {
              router.push('/admin/purchase-request/create');
            }}
            showExportBtn={true}
            handleExport={handleExport}
          />
        </header>

        <div className="flex justify-between items-center my-2">
          <div>
            <button
              className={`mr-2 rounded-md px-4 py-2 focus:outline-none ${
                activeTab === 'Pending' ? 'bg-blue-500 text-white' : 'bg-gray-200'
              }`}
              onClick={() => setActiveTab('Pending')}
            >
              Pending
            </button>
            <button
              className={`rounded-md px-4 py-2 focus:outline-none ${
                activeTab === 'Complete'
                  ? 'bg-blue-500 text-white'
                  : 'bg-gray-200'
              }`}
              onClick={() => setActiveTab('Complete')}
            >
              Complete
            </button>
          </div>
          <div className="flex items-center gap-2">
            <div className="flex cursor-pointer">
              <button
                onClick={() => {
                  setShowFilter(true)
                }}
                className="rounded px-3 py-1 text-white focus:outline-none bg-brand-600 hover:bg-brand-300"
              >
                <FaSearch className="inline-block h-4 w-4" /> Filter
              </button>
            </div>
          </div>
        </div>
        
        {filteredQuery && <div className="flex items-center my-1">
          <div className="mr-8 font-bold p-2 bg-gray-700 rounded text-white">
            FILTERED BY :
          </div>
          <div className="flex-1 flex justify-between items-center">
            {Object.keys(filteredQuery)?.filter((key) => filteredQuery[key] !== '').map((key) => (
              <div key={key}>
                <p className="font-semibold">{key.toUpperCase()} : </p>
                <p>{filteredQuery[key]}</p>
              </div>
            ))}
            <div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={handleExport}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-cyan-600 hover:bg-cyan-300"
                >
                  <FaFileExport className="inline-block h-4 w-4" /> Export
                </button>
              </div>
              <div className="my-1 cursor-pointer">
                <button
                  onClick={() => {
                    setFilteredQuery(null)
                    fetchData()
                  }}
                  className="w-full rounded px-3 py-1 text-white focus:outline-none bg-red-600 hover:bg-red-300"
                >
                  <MdSearchOff className="inline-block h-6 w-6" /> Clear
                </button>
              </div>
            </div>
          </div>
        </div>}

        <div className=" overflow-x-scroll ">
          <table className="w-full">
            <thead>
              <tr className="!border-px !border-gray-400">
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      PR No
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Deparment
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Category
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Ship
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      RequestedBy
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Location
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Date Requested
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Total Items
                    </p>
                  </div>
                </th>
                <th className="cursor-pointer border-b border-gray-200 pb-2 pr-4 pt-4 text-start dark:border-white/30">
                  <div className="items-center justify-between text-xs text-gray-200">
                    <p className="text-sm font-bold text-gray-600 dark:text-white">
                      Action
                    </p>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {activeTab === 'Pending' &&
                pendingData.map((v: any, i: any) => (
                  <tr key={i}>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.purchaseRequestNo}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.department}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.category}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.shipName}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.requestedBy}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.location}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {formatDate(v.createdAt)}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.purchaseItems.length}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3 pr-4">
                      <div className="flex cursor-pointer gap-3 mb-2">
                        <CustomTooltip text="Directly Approve">
                          <p
                            onClick={() => {
                              fetchDetailsPR(v.uuid)
                              setShowDirectApprove(true)
                            }}
                            className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                          >
                            <MdCheck className="h-5 w-5" />
                          </p>
                        </CustomTooltip>
                      </div>
                      <div className="flex cursor-pointer gap-3 mb-2">
                        <CustomTooltip text="Quantity Approve">
                          <p
                            onClick={() => {
                              router.push(`/admin/purchase-request/edit/${v.uuid}`);
                            }}
                            className="text-sm font-bold text-navy-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                          >
                            <MdCreate className="h-5 w-5" />
                          </p>
                        </CustomTooltip>
                      </div>
                      <div className="flex cursor-pointer gap-3 mt-2">
                        <CustomTooltip text="Delete Data">
                        <p
                          onClick={() => handleDelete(v.uuid)}
                          className="text-sm font-bold text-red-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                        >
                          <FaTrashAlt className="h-5 w-5" />
                        </p>
                        </CustomTooltip>
                      </div>
                    </td>
                  </tr>
                ))}
              {activeTab === 'Complete' &&
                completeData.map((v: any, i: any) => (
                  <tr key={i}>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.purchaseRequestNo}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.department}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.category}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.shipName}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.requestedBy}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.location}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {formatDate(v.createdAt)}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {v.purchaseItems.length}
                      </p>
                    </td>
                    <td className="min-w-[150px] border-white/0 py-3  pr-4">
                      <div className="flex cursor-pointer gap-3 mt-2">
                        <CustomTooltip text="Delete Data">
                        <p
                          onClick={() => handleDelete(v.uuid)}
                          className="text-sm font-bold text-red-700 transition-all duration-200 ease-out hover:scale-150 dark:text-white"
                        >
                          <FaTrashAlt className="h-5 w-5" />
                        </p>
                        </CustomTooltip>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>

        <div className="mt-6 flex items-center justify-between">
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage - 1
              setCurrentPage(page)
            }}
            disabled={!canGoPrev}
            className="rounded bg-gray-300 px-4 py-2">
            Previous
          </button>
          <span>
            Page <strong>{currentPage}</strong>
          </span>
          <button
            onClick={() => {
              let page = currentPage <= 0 ? 1 : currentPage + 1
              setCurrentPage(page)
            }}
            disabled={!canGoNext}
            className="rounded bg-gray-300 px-4 py-2"
          >
            Next
          </button>
        </div>

        {showDetail && (
          <div className="bg-black border-black fixed inset-0 flex items-center justify-center border-2 bg-opacity-50 shadow-2xl">
            <div className="h-[700px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">Edit Data</h2>
                <button
                  onClick={() => {
                    setShowDetail(false);
                    setSelectedRow({});
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>
              <div className="w-full pt-5">
                <div className="flex w-full flex-wrap items-center gap-3">
                  <div className="flex flex-col">
                    <label className="ml-2">PR No</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.purchaseRequestNo}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          purchase_request_no: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Department</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.department}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          department: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Category</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.category}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          category: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Ship</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.shipName}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          ship_name: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Requested By</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1 placeholder:font-bold placeholder:text-gray-900"
                      type="text"
                      disabled
                      placeholder={selectedRow.requestedBy}
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Location</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder={selectedRow.location}
                      onChange={(e) =>
                        setSelectedRow({
                          ...selectedRow,
                          location: e.target.value,
                        })
                      }
                    />
                  </div>
                  <button
                    type="submit"
                    onClick={() => {
                      window.location.reload;
                      setShowDetail(false);
                      updatePurchaseRequest(selectedRow);
                    }}
                    className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                  >
                    Save Info
                  </button>
                </div>
              </div>
              <div className="pt-10">
                <table className="w-full">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Unit</td>
                      <td>Quantity</td>
                      <td>Sap Code</td>

                      <td>Additional Info</td>
                      <td>Image</td>
                      <td>Quantity Approved</td>
                      <td>
                        <button
                          className="rounded bg-blue-500 px-1 py-2 text-white hover:bg-blue-600 focus:outline-none"
                          onClick={handleCheckAll}
                        >
                          Check All
                        </button>
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedRow.purchaseItems.map(
                      (item, index) =>
                        // Conditionally render the row only if the status is "Open"
                        item.status === 'Open' && (
                          <tr className="border-b-2" key={index}>
                            <td>{item.description}</td>
                            <td>{item.unit}</td>
                            <td>{item.quantityAsked}</td>
                            <td>{item.sapCode}</td>
                            <td>{item.additionalInfo}</td>
                            <td>
                              <Image
                                className="h-[100px] w-[100px] object-cover"
                                src={
                                  'https://nimsteel.co.id/wp-content/uploads/2023/05/Cara-Memilih-Besi-Beton-Berkualitas-300x267.jpg'
                                }
                                alt="image"
                                width={100}
                                height={100}
                              />
                            </td>
                            <td>
                              <div className="flex items-center">
                                <input
                                  className="w-[120px] rounded-xl border-2 px-1 py-1"
                                  type="text"
                                  placeholder={item.quantityApproved}
                                  value={item.quantityApproved || ''}
                                  onChange={(e) => {
                                    const newValue = e.target.value;
                                    const updatedItems = [
                                      ...selectedRow.purchaseItems,
                                    ];
                                    updatedItems[index].quantityApproved =
                                      newValue; // Update quantityApproved for the specific item
                                    setSelectedRow((prevSelectedRow) => ({
                                      ...prevSelectedRow,
                                      purchaseItems: updatedItems, // Update purchaseItems with the modified item
                                    }));
                                  }}
                                />
                                <button
                                  className={`ml-2 rounded bg-blue-500 px-1 py-2 text-white hover:bg-blue-600 focus:outline-none ${
                                    !item.quantityApproved
                                      ? 'cursor-not-allowed opacity-50'
                                      : ''
                                  }`}
                                  disabled={!item.quantityApproved}
                                  onClick={() => {
                                    handleApproval(item);

                                    setShowDetail(true);
                                  }}
                                >
                                  Change
                                </button>
                              </div>
                            </td>

                            <td>
                              <input
                                type="checkbox"
                                checked={item.isChecked || false}
                                onChange={() => handleCheckboxChange(index)} // Memanggil fungsi handleCheckboxChange dengan indeks item
                              />
                            </td>
                          </tr>
                        ),
                    )}
                  </tbody>
                </table>

                <button
                  onClick={() => {
                    handleSaveChanges();
                    setShowDetail(false);
                  }}
                  disabled={!isChecked}
                  className={`mt-4 rounded bg-blue-500 px-4 py-2 text-white transition-all duration-200 ease-out hover:bg-blue-600 focus:outline-none ${
                    !isChecked ? 'bg-gray-500 hover:bg-gray-500' : ''
                  }`}
                >
                  Add To MOC
                </button>
              </div>
            </div>
          </div>
        )}

        {addNew && (
          <div className="bg-black fixed inset-0 flex items-center justify-center bg-opacity-50">
            <div className="h-[600px] w-[1200px] overflow-auto rounded bg-white p-4 shadow-md">
              <div className="flex items-center justify-between">
                <h2 className="mb-4 text-lg font-bold">New Purchase Request</h2>
                <button
                  onClick={() => {
                    setAddNew(false);
                  }}
                  className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                >
                  Close
                </button>
              </div>

              <div className="flex w-full flex-col gap-2 p-3">
                <div className="flex items-center justify-start gap-3">
                  <div className="flex flex-col">
                    <label className="ml-2">PR No</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          purchaseRequestNo: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Ship</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      value={newPurchaseRequestData.ship_name}
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          shipName: e.target.value,
                        })
                      }
                    />
                  </div>
                </div>
                <div className="flex items-center justify-start gap-3">
                  <div className="flex flex-col">
                    <label className="ml-2">Department</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      value={newPurchaseRequestData.department}
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          department: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="flex flex-col">
                    <label className="ml-2">Category</label>
                    <input
                      className="rounded-xl border-2 px-2 py-1"
                      type="text"
                      placeholder="Input Your Text Here"
                      value={newPurchaseRequestData.category}
                      onChange={(e) =>
                        setNewPurchaseRequestData({
                          ...newPurchaseRequestData,
                          category: e.target.value,
                        })
                      }
                    />
                  </div>
                </div>
              </div>

              <div className="flex w-full flex-col gap-2 overflow-x-auto p-3">
                <div className=" flex">
                  <button
                    className={`mr-2 rounded-md px-4 py-2 focus:outline-none ${
                      bulk === false ? 'bg-blue-500 text-white' : 'bg-gray-200'
                    }`}
                    onClick={() => setBulk(false)}
                  >
                    Manual
                  </button>
                  <button
                    className={`rounded-md px-4 py-2 focus:outline-none ${
                      bulk === true ? 'bg-blue-500 text-white' : 'bg-gray-200'
                    }`}
                    onClick={() => setBulk(true)}
                  >
                    Bulk
                  </button>
                </div>
                <h3 className="text-[20px] font-bold">Items</h3>
                {items.map((item, index) => (
                  <div className="flex items-center gap-1" key={item.id}>
                    <div className="flex flex-col">
                      <label className="ml-2">Item Name</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseItems,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            description: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseItems: newItems, // Update the purchaseItems array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Unit</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseItems,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            unit: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseItems: newItems, // Update the purchaseItems array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Quantity</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseItems,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            quantityAsked: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseItems: newItems, // Update the purchaseItems array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">SAP Code</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseItems,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            sapCode: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseItems: newItems, // Update the purchaseItems array with the modified item
                          });
                        }}
                      />
                    </div>

                    <div className="flex flex-col">
                      <label className="ml-2">Additional Info</label>
                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="text"
                        placeholder="Input Your Text Here"
                        onChange={(e) => {
                          const newName = e.target.value;
                          const newItems = [
                            ...newPurchaseRequestData.purchaseItems,
                          ]; // Make a copy of existing items array
                          newItems[index] = {
                            ...newItems[index],
                            additionalInfo: newName,
                          }; // Update the name of the item at the current index
                          setNewPurchaseRequestData({
                            ...newPurchaseRequestData,
                            purchaseItems: newItems, // Update the purchaseItems array with the modified item
                          });
                        }}
                      />
                    </div>
                    <div className="flex flex-col">
                      <label className="ml-2">Image</label>

                      <input
                        className="rounded-xl border-2 px-2 py-1"
                        type="file"
                        placeholder="Input Your Text Here"
                      />
                    </div>
                    <button
                      className="mt-4 rounded bg-blue-500 px-4 py-1 text-white hover:bg-blue-600 focus:outline-none"
                      onClick={() => removeItem(item.id)}
                    >
                      -
                    </button>
                  </div>
                ))}
                <div className="flex gap-2">
                  <button
                    className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
                    onClick={addItem}
                  >
                    +
                  </button>
                </div>
              </div>
              <button
                onClick={handleSaveNewPurchaseRequest}
                className="mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none"
              >
                Add New
              </button>
            </div>
          </div>
        )}
      </Card>

      {showNotif && 
        <NotificationOverlay 
            isShow={showNotif}
            title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
            message={notifMessage}
            handleClose={() => setShowNotif(false)}
            color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
      
      {showFilter && (
        <ModalFilter
          title={`Filter Data Purchase Request`}
          onClose={() => setShowFilter(false)}
          onSubmit={onSubmitModal}
        />
      )}

      {showDirectApprove && (
        <ModalDirectlyApprove
          title={`Directly Approve Data Purchase Request?`}
          onClose={() => setShowDirectApprove(false)}
		  details={details}
		  listItemsApproved={itemsApproved}
          onSubmit={onSubmitApproved}
        />
      )}
    </div>
  );
};

const ModalFilter = ({ onClose, onSubmit, title }: any) => {
  const [formData, setFormData] = useState({
    category: '',
    department: '',
    ship: '',
    startDate: '',
    endDate: '',
  })
  return (
    <DefaultModal title={title} onClose={onClose}>
      <div className="mt-6 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 py-2">
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Start Date</p>
          <input
              type="date"
              name="startDate"
              id="startDate"
              placeholder="Input Start Date"
              value={formData.startDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  startDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">End Date</p>
          <input
              type="date"
              name="endDate"
              id="endDate"
              placeholder="Input End Date"
              value={formData.endDate}
              onChange={(e) => {
                setFormData({
                  ...formData,
                  endDate: e.target.value
                })
              }}
              className="block w-full rounded-md border-0 px-2 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-brand-600 sm:text-base sm:leading-6"
          />
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Category</p>
          <AutocompleteInput
              placeholder="Search Category"
              apiUrl="category/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  category: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Department</p>
          <AutocompleteInput
              placeholder="Search Department"
              apiUrl="department/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  department: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
        <div className="m-2">
          <p className="text-lg font-medium leading-6 my-1">Ship</p>
          <AutocompleteInput
              placeholder="Search Ship"
              apiUrl="ship/search"
              searchQuery='name'
              keyLabel='name'
              keyValue='name'
              minTreshold={3}
              onSelect={async (suggestion: any) => {
                setFormData({
                  ...formData,
                  ship: suggestion?.name
                })
              }}
          />
          <p className="text-sm leading-6 text-gray-600">Min 3 character for search.</p>
        </div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit(formData);
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Submit
        </button>
      </div>
    </DefaultModal>
  );
};

const ModalDirectlyApprove = ({ onClose, onSubmit, title, details, listItemsApproved }: any) => {
  return (
    <DefaultModal title={title} onClose={onClose}>
      <div className="mt-3 max-h-[400px] overflow-auto border-b border-t border-gray-900/10 py-2">
	  	<div className="my-2 border-b pb-2">
			<h3 className="font-bold">Details Info Purchase Request</h3>
		</div>
        <div className="grid grid-cols-3 gap-2">
          <div className="col-span-1 bg-gray-100 p-2 rounded">
            <label className="font-semibold">PR Number</label>
            <p>{details?.purchaseRequestNo}</p>
          </div>
          <div className="col-span-1 bg-gray-100 p-2 rounded">
            <label className="font-semibold">Ship</label>
            <p>{details?.shipName}</p>
          </div>
          <div className="col-span-1 bg-gray-100 p-2 rounded">
            <label className="font-semibold">Location</label>
            <p>{details?.location}</p>
          </div>
          <div className="col-span-1 bg-gray-100 p-2 rounded">
            <label className="font-semibold">Department</label>
            <p>{details?.department}</p>
          </div>
          <div className="col-span-1 bg-gray-100 p-2 rounded">
            <label className="font-semibold">Category</label>
            <p>{details?.category}</p>
          </div>
          <div className="col-span-1 bg-gray-100 p-2 rounded">
            <label className="font-semibold">Request By</label>
            <p>{details?.requestedBy}</p>
          </div>
        </div>
		<div className="my-2 border-t pt-2">
			<h3 className="font-bold">List Items ({listItemsApproved.length} Item)</h3>
		</div>
		<div className="grid grid-cols-2 gap-2">
			{listItemsApproved?.map((item:any, idx:number) => (
				<div className="col-span-1 bg-[#F3EBC6] p-2 rounded" key={'item-'+idx}>
					<div className="flex justify-between gap-2">
						<div className="flex-1 flex items-center">
							<label className="font-semibold mr-1">Item: </label>
							<p>{item.itemName}</p>
						</div>
						<div className="p-2 rounded bg-white font-bold">{idx+1}</div>
					</div>
					<div className="flex items-center">
						<label className="font-semibold mr-1">Unit: </label>
						<p>{item.unit}</p>
					</div>
					<div className="flex items-center">
						<label className="font-semibold mr-1">Quantity: </label>
						<p>{item.quantity}</p>
					</div>
					
				</div>
			))}
		</div>
      </div>
      <div className="mt-6 flex items-center justify-end gap-x-6">
        <button
          type="button"
          onClick={() => {
            onSubmit();
            onClose();
          }}
          className="text-md rounded-md bg-green-600 px-4 py-2 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          {' '}
          Approve
        </button>
      </div>
    </DefaultModal>
  );
};

export default Page;
