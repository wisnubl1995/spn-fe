'use client';

import DeliveryTable from 'components/admin/data-tables/DeliveryTable';
import OverlayLoading from 'components/loading/OverlayLoading';
import NotificationOverlay from 'components/notification/NotificationOverlay';
import React, { useContext, useEffect, useState } from 'react';
import { sendRequestApi } from 'utils/api/fetchData';
import { formattedDate } from 'utils/helper';
import Papa from 'papaparse'
import exportToExcel from 'utils/exportExcel';
import AuthContext from 'contexts/AuthContext';

type Props = {};

const Page = (props: Props) => {
  const { sessionUser } = useContext(AuthContext)
	const headersReq = {
		"account-access": sessionUser?.uuid
	}
  const [isLoading, setIsLoading] = useState(true);
  const [listData, setListData] = useState<any>([]);
  const [showNotif, setShowNotif] = useState(false)
  const [notifMessage, setNotifMessage] = useState(null)
  const [searchLocation, setSearchLocation] = useState('')
  const [currentPage, setCurrentPage] = useState(1);
  let timeoutId: number | any;

  const fetchData = async () => {
    setIsLoading(true)
    try {
        const resp = await sendRequestApi(`outbound/getdelivery?page=${currentPage}&pageSize=10&location=${searchLocation}`, 'GET', null, headersReq)
        if (resp.data) {
          setListData(resp.data)
        }else{
          setListData([])
        }
        setIsLoading(false)
    } catch (error) {
        setIsLoading(false)
        setNotifMessage(`Failed fetching details data!`)
        setShowNotif(true)
        console.log(`failed fetching details`)
    }
  }

  useEffect(() => {
    const debouncedFetch = debounce(fetchData, 1000); // Debounce to avoid excessive requests
    debouncedFetch();

    // Cleanup function to clear timeout on unmount
    return () => clearTimeout(timeoutId);;
  }, [searchLocation])

  const debounce = (func, delay) => {
    return (...args) => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => func.apply(this, args), delay);
    };
  };

  const handleExport = () => {
    setIsLoading(true);
    const tmpData = [];
    for (let i = 0; i < listData.length; i++) {
      const header = listData[i];
      for (let j = 0; j < header.outboundItems.length; j++) {
        const item = header.outboundItems[j];
        tmpData.push({
          PURCHASE_REQUEST_NO: header.noPr,
          NAMA_KAPAL: header.shipName,
          KATEGORI_KAPAL: header.categoryShip,
          DEPARTEMEN: header.department,
          LOKASI: header.location,
          DELIVER_TO: header.deliverTo,
          VIA: header.via,
          NO_RESI: header.noResi,
          PENERIMA: header.recepient,
          TANGGAL_TERIMA: header.receiptDate
            ? formattedDate(new Date(header.receiptDate), 'YYYY-MM-DD')
            : header.receiptDate,
          PRODUCT_NAME: item.productName,
          SATUAN: item.unit,
          QUANTITY: item.count,
          CREATED_BY: header.createdBy ? header.createdBy : '-',
          CREATED_AT: header.createdAt
            ? formattedDate(new Date(header.createdAt), 'YYYY-MM-DD')
            : header.createdAt,
          STATUS_DELIVERY: header.status === "Close" ? "DELIVERED" : "ON THE WAY"
        });
      }
    }

    exportToExcel(tmpData, 'DELIVERY', 'DATA');
    setIsLoading(false);
  };

  useEffect(() => {
    fetchData()
  }, [currentPage])

  const canGoPrev = currentPage < 1
  const canGoNext = listData.length >= 10

  if (isLoading) 
    return <OverlayLoading />

  return (
    <div className="mt-10">
      <DeliveryTable 
        tableName="Data Delivery"
        tableData={listData}
        onHandleExport={handleExport}
        onSearchLocation={(e) => {
          setSearchLocation(e.target.value)
        }}
        valueSearch={searchLocation}
        onHandleAdd={() => console.log('on develop')}
        onHandleEdit={() => console.log('on develop')}
        canGoNext={canGoNext}
        canGoPrev={canGoPrev}
        currentPage={currentPage}
        nextHandler={() => {
          let page = currentPage <= 0 ? 1 : currentPage + 1
          setCurrentPage(page)
        }}
        prevHandler={() => {
          let page = currentPage <= 0 ? 1 : currentPage - 1
          setCurrentPage(page)
        }} />

      {showNotif && 
        <NotificationOverlay 
            isShow={showNotif}
            title={notifMessage?.includes('Failed') ? 'Failed' : 'Info'}
            message={notifMessage}
            handleClose={() => setShowNotif(false)}
            color={notifMessage?.includes('Failed') ? 'red' : 'green'}  />}
    </div>
  );
};

export default Page;
