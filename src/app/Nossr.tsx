import React, { ReactNode } from 'react';

const NoSSR: React.FC<{ children: ReactNode }> = ({ children }) => (
  <React.Fragment>{children}</React.Fragment>
);

export default NoSSR;
