import React from 'react';

// Admin Imports

// Icon Imports
import {
  MdHome,
  MdArrowForward,
  MdArrowBack,
  MdOutlineWarehouse,
  MdFmdGood,
  MdLock,
} from 'react-icons/md';
import { FaShip, FaDatabase, FaWpforms, FaUser } from 'react-icons/fa6';

const routes = [
  {
    name: 'Main Dashboard',
    layout: '/admin',
    path: 'home',
    icon: <MdHome className="h-6 w-6" />,
  },
  {
    name: 'Purchase Request',
    layout: '/admin',
    path: 'purchase-request',
    icon: <MdHome className="h-6 w-6" />,
  },
  {
    name: 'MOC',
    layout: '/admin',
    path: 'moc',
    icon: <MdHome className="h-6 w-6" />,
  },
  {
    name: 'Purchase Order',
    layout: '/admin',
    path: 'purchase-order',
    icon: <MdHome className="h-6 w-6" />,
  },
  {
    name: 'Inbound',
    layout: '/admin',
    path: 'inbound',
    icon: <MdArrowForward className="h-6 w-6" />,
  },
  {
    name: 'Inventory',
    layout: '/admin',
    path: 'inventory',
    icon: <MdOutlineWarehouse className="h-6 w-6" />,
  },
  {
    name: 'Outbound',
    layout: '/admin',
    path: 'outbound',
    icon: <MdArrowBack className="h-6 w-6" />,
  },
  {
    name: 'Delivery',
    layout: '/admin',
    path: 'delivery',
    icon: <MdFmdGood className="h-6 w-6" />,
  },
  {
    name: 'Ships',
    layout: '/admin',
    path: 'ships',
    icon: <FaShip className="h-6 w-6" />,
  },
  {
    name: 'Data Accounts',
    layout: '/admin',
    path: 'accounts',
    icon: <FaUser className="h-6 w-6" />,
  },
  {
    name: 'Departments',
    layout: '/admin',
    path: 'departments',
    icon: <FaDatabase className="h-6 w-6" />,
  },
  {
    name: 'Categories',
    layout: '/admin',
    path: 'categories',
    icon: <FaDatabase className="h-6 w-6" />,
  },
  {
    name: 'Units',
    layout: '/admin',
    path: 'units',
    icon: <FaDatabase className="h-6 w-6" />,
  },
  {
    name: 'Stores',
    layout: '/admin',
    path: 'stores',
    icon: <FaDatabase className="h-6 w-6" />,
  },
  {
    name: 'Locations',
    layout: '/admin',
    path: 'location',
    icon: <FaDatabase className="h-6 w-6" />,
  },
  // {
  //   name: 'Roles',
  //   layout: '/admin',
  //   path: 'roles',
  //   icon: <FaDatabase className="h-6 w-6" />,
  // },
  // {
  //   name: 'Report',
  //   layout: '/admin',
  //   path: 'report',
  //   icon: <MdInsertChartOutlined className="h-6 w-6" />,
  // },
  // {
  //   name: 'NFT Marketplace',
  //   layout: '/admin',
  //   path: 'nft-marketplace',
  //   icon: <MdOutlineShoppingCart className="h-6 w-6" />,

  //   secondary: true,
  // },
  // {
  //   name: 'Data Tables',
  //   layout: '/admin',
  //   icon: <MdBarChart className="h-6 w-6" />,
  //   path: 'data-tables',
  // },

  // {
  //   name: 'Profile',
  //   layout: '/admin',
  //   path: 'profile',
  //   icon: <MdPerson className="h-6 w-6" />,
  // },
  {
    name: 'Log Out',
    layout: '/auth',
    path: 'sign-in?logout=true',
    icon: <MdLock className="h-6 w-6" />,
  },
];
export default routes;
